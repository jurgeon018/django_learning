from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from testt.models import * 


def index(request):
    
    return HttpResponse('Hello')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
]
