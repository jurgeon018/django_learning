from django.contrib import admin
from django.urls import path, include
from django.shortcuts import render
from django.utils.safestring import mark_safe
import json

from asgiref.sync import async_to_sync, sync_to_async
from channels.generic.websocket import WebsocketConsumer, AsyncWebsocketConsumer
from channels.auth import AuthMiddlewareStack
from channels.routing import (
    ProtocolTypeRouter, URLRouter,
    ChannelNameRouter
)


# loads - строка  -> словарь (JSON.parse)
# dumps - словарь -> строкa  (JSON.stringify)
# load  - файл    -> словарь
# dump  - словарь -> файл


class ChatConsumer(WebsocketConsumer):

    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room']
        self.room_group_name = f'group_{self.room_name}'
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name, 
            self.channel_name
        )
        self.accept()

    def disconnect(self):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name, 
            self.channel_name
        )

    def receive(self, text_data):
        print(self.channel_name)

        # self.send(text_data=json.dumps({
        #     'message':json.loads(text_data)['message']
        # }))

        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type':'send_chat_message',
                'message': json.loads(text_data)['message'],
                'channel_name':self.channel_name,
            }
        )
    def send_chat_message(self, event):
        self.send(text_data=json.dumps({
            'message':event['message'],
            'channel_name': event['channel_name'],
        }))


def room(request, room):
    return render(request, 'room.html', locals())


urlpatterns = [
    path('chat/<room>/', room, name='room'),
]

websocket_urlpatterns = [
    path('ws/chat/<room>/', ChatConsumer)
]


application = ProtocolTypeRouter({
    'websocket':AuthMiddlewareStack(URLRouter(websocket_urlpatterns))
})

