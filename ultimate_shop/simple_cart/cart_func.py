import datetime
from . import models

CART_ID = 'CART-ID'


def __init__(request):
  cart_id = request.session.get(CART_ID)
  if cart_id:
    try:
      cart = models.Cart.objects.get(id=cart_id, checked_out=False)
    except models.Cart.DoesNotExist:
      cart = self.new(request)
  else:
    cart = self.new(request)
  self.cart = cart

def __iter__(cart):
  for item in cart.item_set.all():
    yield item

def new(request, cart):
  cart = models.Cart(creation_date=datetime.datetime.now())
  cart.save()
  request.session[CART_ID] = cart.id
  return cart

def add(product, unit_price, quantity=1):
  try:
    item = models.Item.objects.get(
        cart=self.cart,
        product=product,
    )
  except models.Item.DoesNotExist:
    item = models.Item()
    item.cart = self.cart
    item.product = product
    item.unit_price = unit_price
    item.quantity = quantity
    item.save()
  else: #ItemAlreadyExists
    item.unit_price = unit_price
    item.quantity += int(quantity)
    item.save()

def remove(cart, product):
  try:
    item = models.Item.objects.get(
      cart=cart,
      product=product,
    )
  except models.Item.DoesNotExist:
    raise ItemDoesNotExist
  else:
    item.delete()

def update(cart, product, quantity, unit_price=None):
  try:
    item = models.Item.objects.get(
      cart=cart,
      product=product,
    )
  except models.Item.DoesNotExist:
    raise ItemDoesNotExist
  else: #ItemAlreadyExists
    if quantity == 0:
      item.delete()
    else:
      item.unit_price = unit_price
      item.quantity = int(quantity)
      item.save()

def count(cart):
  result = 0
  for item in cart.item_set.all():
    result += 1 * item.quantity
  return result

def summary(cart):
  result = 0
  for item in cart.item_set.all():
    result += item.total_price
  return result

def clear(cart):
  for item in cart.item_set.all():
    item.delete()

def is_empty(cart):
  return cart.count() == 0

def cart_serializable(cart):
  representation = {}
  for item in cart.item_set.all():
      itemID = str(item.object_id)
      itemToDict = {
          'total_price': item.total_price,
          'quantity': item.quantity
      }
      representation[itemID] = itemToDict
  return representation
