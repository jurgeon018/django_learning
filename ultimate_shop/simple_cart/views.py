from .models import *
from django.shortcuts import render
from django.urls import  path
# from .cart import *
from .cart_func import *


def add_to_cart(request, pk, quantity):
    item = Item.objects.get(pk=pk)
    cart = Cart(request)
    cart.add(item, item.unit_price, quantity)

def remove_from_cart(request, pk):
    item = Item.objects.get(pk=pk)
    cart = Cart(request)
    cart.remove(item)

def get_cart(request):
  cart = Cart(request)
  cart_id = request.session.get('CART-ID')
  if cart_id:
    try:
      cart = models.Cart.objects.get(id=cart_id, checked_out=False)
    except models.Cart.DoesNotExist:
      cart = Cart().new(request)
  else:
    cart = Cart().new(request)
  cart = cart.item_set.all()
  return render(request, 'cart.html', {'cart':cart})


urlpatterns = [
  path('add/<pk>/', add_to_cart, name='add_to_cart'),
  path('remove/<pk>/', remove_from_cart, name='remove_from_cart'),
  path('', get_cart, name='get_cart'),
]