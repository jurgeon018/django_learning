1. add 'cart' to your INSTALLED_APPS directive and
2. If you have South migrations type: `./manage.py migrate cart`
3. or if you don't: `./manage.py makemigrations cart`

## Known Problems
Right now the main problem is that it adds a database record for each cart it creates. I'm in the process of studying this and will soon implement something to handle it.

