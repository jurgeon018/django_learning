from django.contrib import admin
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save

# products
class Product(models.Model):
    name = models.CharField(max_length=120)
    price = models.IntegerField()
    def __str__(self):
        return self.name

# accounts
class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    ebooks = models.ManyToManyField(Product, blank=True)
    # stripe_id = models.CharField(max_length=200, null=True, blank=True)
    def __str__(self):
        return self.user.username

def post_save_profile_create(sender, instance, created, *args, **kwargs):
    if created:
        Profile.objects.get_or_create(user=instance)
#     user_profile, created = Profile.objects.get_or_create(user=instance)
#     if user_profile.stripe_id is None or user_profile.stripe_id == '':
#         new_stripe_id = stripe.Customer.create(email=instance.email)
#         user_profile.stripe_id = new_stripe_id['id']
#         user_profile.save()
post_save.connect(post_save_profile_create, sender=settings.AUTH_USER_MODEL)
admin.site.register(Profile)

# shopping_cart
class OrderItem(models.Model):
    product = models.OneToOneField(Product, on_delete=models.SET_NULL, null=True)
    is_ordered = models.BooleanField(default=False)
    date_added = models.DateTimeField(auto_now=True)
    date_ordered = models.DateTimeField(null=True)
    def __str__(self):
        return self.product.name

class Order(models.Model):
    ref_code = models.CharField(max_length=15)
    owner = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
    is_ordered = models.BooleanField(default=False)
    items = models.ManyToManyField(OrderItem)
    date_ordered = models.DateTimeField(auto_now=True)
    def get_cart_items(self):
        return self.items.all()
    def get_cart_total(self):
        return sum([item.product.price for item in self.items.all()])
    def __str__(self):
        return '{0} - {1}'.format(self.owner, self.ref_code)

class Transaction(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    token = models.CharField(max_length=120)
    order_id = models.CharField(max_length=120)
    amount = models.DecimalField(max_digits=100, decimal_places=2)
    success = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    def __str__(self):
        return self.order_id
    class Meta:
        ordering = ['-timestamp']




admin.site.register(Product)
admin.site.register(OrderItem)
admin.site.register(Order)
admin.site.register(Transaction)



        