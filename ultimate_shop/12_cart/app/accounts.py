from django.urls import path
from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.db import models
from .models import *
# import stripe
# stripe.api_key = settings.STRIPE_SECRET_KEY


def my_profile(request):
	my_user_profile = Profile.objects.filter(user=request.user).first()
	my_orders = Order.objects.filter(is_ordered=True, owner=my_user_profile)
	context = {
		'my_orders': my_orders
	}

	return render(request, "profile.html", context)


app_name = 'accounts'
urlpatterns = [
	path('profile/', my_profile, name='my_profile')
]

