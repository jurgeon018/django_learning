from django.contrib import admin
from .models import *
from django.utils.safestring import mark_safe

def make_payed(modeladmin, request, queryset):
    queryset.update(status='Оплачен')
make_payed.short_description = "Пометить как оплаченные"

def make_available(modeladmin, request, queryset):
    queryset.update(available=True)
make_available.short_description = "Есть в наличии"

def make_unavailable(modeladmin, request, queryset):
    queryset.update(available=False)
make_unavailable.short_description = "Нет в наличии"


class OrderAdmin(admin.ModelAdmin):
    list_filter = ['status']
    list_display= ['id', 'items_in_order']
    actions = [make_payed]
    def items_in_order(self, obj):
        items_in_order = '<hr>'.join(['Товар - {} | Кол-во - {} | '.format(item.product.title, item.qty) for item in obj.items.items.all()])
        return mark_safe(items_in_order)
class ProductAdmin(admin.ModelAdmin):
    class Meta:
        model = Product
    list_display = [field.name for field in Product._meta.fields]
    #fields = ['']
    exclude = ['']
    #list_filter = ['']
    seatch_fields = ['']
    actions = [make_available, make_unavailable]

admin.site.register(Category)
admin.site.register(Brand)
admin.site.register(Product, ProductAdmin)
admin.site.register(CartItem)
admin.site.register(Cart)
admin.site.register(Order, OrderAdmin)
admin.site.register(MiddlewareNotification)
