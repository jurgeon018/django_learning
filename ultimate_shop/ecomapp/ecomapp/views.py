from decimal import Decimal
from django.shortcuts import render, reverse, redirect
from django.http import JsonResponse
from django.contrib.auth import login, authenticate
from .forms import *
from .models import *
from django.contrib.auth.models import User
from notifications.models import Notification
from .mixins import *


def get_cart(request):
	try:
		cart = Cart.objects.get(id=request.session['cart_id'])
		request.session['total'] = cart.items.count()
	except:
		cart = Cart()
		cart.save()
		request.session['cart_id'] = cart.id
		cart = Cart.objects.get(id=cart.id)
	return cart


def base(request):
	cart = get_cart(request)
	categories = Category.objects.all()
	products = Product.objects.all()
	context = {
		'categories': categories,
		'products': products,
		'cart': cart
	}
	return render(request, 'ecomapp/base.html', context)


def product(request, product_slug):
	cart = get_cart(request)
	product = Product.objects.get(slug=product_slug)
	categories = Category.objects.all()
	context = {
		'categories': categories,
		'product': product,
		'cart': cart,
	}
	if str(request.user) != 'AnonymousUser':
		check_for_subscribe = [notification.product for notification in MiddlewareNotification.objects.filter(user_name=request.user, product=product)]
		context.update({'check_for_subscribe': check_for_subscribe,})
	return render(request, 'ecomapp/product.html', context)


def category(request, category_slug):
	cart = get_cart(request)
	category = Category.objects.get(slug=category_slug)
	categories = Category.objects.all()
	price_filter_type = request.GET.get('price_filter_type')
	products_of_category = Product.objects.filter(category=category)
	context = {
		'category': category,
		'categories': categories,
		'products_of_category': products_of_category,
		'cart': cart
	}
	return render(request, 'ecomapp/category.html', context)


def cart(request):
	cart = get_cart(request)
	categories = Category.objects.all()
	context = {
		'cart': cart,
		'categories': categories
	}
	return render(request, 'ecomapp/cart.html', context)


def add_to_cart(request):
	cart = get_cart(request)
	product_slug = request.GET.get('product_slug')
	product = Product.objects.get(slug=product_slug)
	cart.add_to_cart(product.slug)
	new_cart_total = 0.00
	for item in cart.items.all():
		new_cart_total += float(item.item_total)
	cart.cart_total = new_cart_total
	cart.save()
	return JsonResponse({'cart_total': cart.items.count(), 'cart_total_price': cart.cart_total})

def remove_from_cart(request):
	cart = get_cart(request)
	product_slug = request.GET.get('product_slug')
	product = Product.objects.get(slug=product_slug)
	cart.remove_from_cart(product.slug)
	new_cart_total = 0.00
	for item in cart.items.all():
		new_cart_total += float(item.item_total)
	cart.cart_total = new_cart_total
	cart.save()
	return JsonResponse({'cart_total': cart.items.count(), 'cart_total_price': cart.cart_total})


def change_item_qty(request):
	cart = get_cart(request)
	qty = request.GET.get('qty')
	item_id = request.GET.get('item_id')
	cart.change_qty(qty, item_id)
	cart_item = CartItem.objects.get(id=int(item_id))
	return JsonResponse(
		{'cart_total': cart.items.count(),
		'item_total': cart_item.item_total,
		'cart_total_price': cart.cart_total})


def checkout(request):
	cart = get_cart(request)
	categories = Category.objects.all()
	context = {
		'cart': cart,
		'categories': categories
	}
	return render(request, 'ecomapp/checkout.html', context)


def order_create(request):
	cart = get_cart(request)
	form = OrderForm(request.POST or None)
	categories = Category.objects.all()
	context = {
		'form': form,
		'cart': cart,
		'categories': categories
	}
	return render(request, 'ecomapp/order.html', context)


def make_order(request):
	cart = get_cart(request)
	form = OrderForm(request.POST or None)
	categories = Category.objects.all()
	if form.is_valid():
		name = form.cleaned_data['name']
		last_name = form.cleaned_data['last_name']
		phone = form.cleaned_data['phone']
		buying_type = form.cleaned_data['buying_type']
		address = form.cleaned_data['address']
		comments = form.cleaned_data['comments']
		new_order = Order.objects.create(
			user=request.user,
			items=cart,
			total=cart.cart_total,
			first_name=name,
			last_name=last_name,
			phone=phone,
			address=address,
			buying_type=buying_type,
			comments=comments
			)
		del request.session['cart_id']
		del request.session['total']
		return redirect(reverse('thank_you'))
	return render(request, 'ecomapp/order.html', {'categories': categories})












def account(request):
	order = Order.objects.filter(user=request.user).order_by('-id')
	categories = Category.objects.all()
	for item in order:
		for new_item in item.items.items.all():
			print(new_item.item_total)
	context = {
		'order': order,
		'categories': categories
	}
	return render(request, 'ecomapp/account.html', context)

def registration(request):
	form = RegistrationForm(request.POST or None)
	categories = Category.objects.all()
	if form.is_valid():
		new_user = form.save(commit=False)
		username = form.cleaned_data['username']
		password = form.cleaned_data['password']
		email = form.cleaned_data['email']
		first_name = form.cleaned_data['first_name']
		last_name = form.cleaned_data['last_name']
		new_user.username = username
		new_user.set_password(password)
		new_user.first_name = first_name
		new_user.last_name = last_name
		new_user.email = email
		new_user.save()
		login_user = authenticate(username=username, password=password)
		if login_user:
			login(request, login_user)
			return redirect(reverse('base'))
	context = {
		'form': form,
		'categories': categories
	}
	return render(request, 'ecomapp/registration.html', context)


def loginn(request):
	form = LoginForm(request.POST or None)
	categories = Category.objects.all()
	if form.is_valid():
		username = form.cleaned_data['username']
		password = form.cleaned_data['password']
		login_user = authenticate(username=username, password=password)
		if login_user:
			login(request, login_user)
			return redirect(reverse('base'))
	context = {
		'form': form,
		'categories': categories
	}
	return render(request, 'ecomapp/loginn.html', context)

def notify_create(request):
	product_slug = request.GET.get('product_slug')
	new_notification = MiddlewareNotification.objects.create(
		user_name = request.user,
		product = Product.objects.get(slug=product_slug)
	)
	return JsonResponse({'created':"Вы подписались на уведомления о поступлении товара."})

def notify_delete(request):
	slug = request.GET.get('slug')
	notification_on_delete = Notification.objects.get(recipient=request.user, description=slug)
	notification_on_delete.delete()
	return JsonResponse({'ok':'ok'})
