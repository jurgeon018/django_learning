from django.views import View



class UniversalMixin(object):
    def get_cart(self, *args,**kwargs):
        try:
            cart_id = self.request.session['cart_id']
            cart = Cart.objects.get(id=cart_id)
            request.session['total'] = cart.items.count()
        except:
            cart = Cart()
            cart.save()
            cart_id = cart.id
            self.request.session['cart_id'] = cart_id
            cart = Cart.objects.get(id=cart_id)
        return cart
    def get_products(self):
        return Product.objects.all()
    def get_categories(self):
        return Category.objects.all()
    def get_all_data(self):
        context = {
            'cart': self.get_cart(),
            'products': self.get_products(),
            'categories': self.get_categories()
        }

class Base(UniversalMixin, View):
	def get(self, request, *args, **kwargs):
		mixin_data = super(Base, self)
		# categories = mixin_data.get_categories()
		# products = mixin_data.get_products()
		# cart = mixin_data.get_cart()
		# context = {'categories': categories, 'products': products, 'cart': cart}
		context = mixin_data.get_all_data()
		template = 'ecomapp/base.html'
		return render(request, template, context)
