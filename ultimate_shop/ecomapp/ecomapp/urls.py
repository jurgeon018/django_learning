from django.urls import path, reverse_lazy
from django.contrib.auth.views import LogoutView
from django.views.generic import TemplateView
from .views import *

urlpatterns = [
	path('category/<category_slug>', category, name='category_detail'),
	path('product/<product_slug>', product, name='product_detail'),
	path('add_to_cart/', add_to_cart, name='add_to_cart'),
	path('remove_from_cart/', remove_from_cart, name='remove_from_cart'),
	path('change_item_qty/', change_item_qty, name='change_item_qty'),
	path('cart/', cart, name='cart'),
	path('checkout/', checkout, name='checkout'),
	path('order/', order_create, name='create_order'),
	path('make_order/', make_order, name='make_order'),
	path('thank_you/', TemplateView.as_view(template_name='ecomapp/thank_you.html'), name='thank_you'),
	
	path('account/', account, name='account'),
	path('registration/', registration, name='registration'),
	path('loginn/', loginn, name='loginn'),
	path('logout/', LogoutView.as_view(next_page=reverse_lazy('base')), name='logout'),
	path('notify_create/', notify_create, name='notify_create'),
	path('notify_delete/', notify_delete, name='notify_delete'),
	path('', base, name='base'),
]
