from .models import ProductInBasket

def getting_basket_info(request):

    session_key = request.session.session_key
    if not session_key:
        # request.session["session_key"] = 123
        request.session.cycle_key()

    products_in_basket = ProductInBasket.objects.filter(
        session_key=session_key, 
        is_active=True, 
        order__isnull=True
    )
    products_total_nmb = products_in_basket.count()

    total_amount = 0
    for i in products_in_basket:
        total_amount += int(i.total_price)
    request.session['total_amount'] = total_amount

    return locals()