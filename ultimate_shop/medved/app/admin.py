from django.contrib import admin
from .models import *

# products
class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 0
class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ProductCategory._meta.fields]
    class Meta:
        model = ProductCategory
class ProductAdmin (admin.ModelAdmin):
    list_display = [field.name for field in Product._meta.fields]
    inlines = [ProductImageInline]
    class Meta:
        model = Product
class ProductImageAdmin (admin.ModelAdmin):
    list_display = [field.name for field in ProductImage._meta.fields]
    class Meta:
        model = ProductImage
admin.site.register(ProductCategory, ProductCategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductImage, ProductImageAdmin)


# order
class StatusAdmin (admin.ModelAdmin):
    list_display = [field.name for field in Status._meta.fields]
    class Meta:
        model = Status




# class ProductInOrderInline(admin.TabularInline):
#     model = ProductInOrder
#     extra = 0
class ProductInBasketInline(admin.TabularInline):
    model = ProductInBasket
    extra = 0
    exclude = ['session_key']
class OrderAdmin (admin.ModelAdmin):
    list_display = [field.name for field in Order._meta.fields]
    inlines = [
        # ProductInOrderInline,
        ProductInBasketInline
    ]
    class Meta:
        model = Order
class ProductInBasketAdmin (admin.ModelAdmin):
    list_display = [field.name for field in ProductInBasket._meta.fields]
    class Meta:
        model = ProductInBasket


# class ProductInOrderAdmin (admin.ModelAdmin):
#     list_display = [field.name for field in ProductInOrder._meta.fields]
#     class Meta:
#         model = ProductInOrder
# admin.site.register(ProductInOrder, ProductInOrderAdmin)


admin.site.register(Status, StatusAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(ProductInBasket, ProductInBasketAdmin)