// let form = document.querySelector('#form_buying_product')
// let submit_btn = document.querySelector('#submit_btn')
// let delete_btn = 


// function basketUpdating(product_id, nmb, is_delete) {
//   var data = {};
//   data.product_id = product_id;
//   data.nmb = nmb;
//   data["csrfmiddlewaretoken"] = document.querySelector('#form_buying_product [name="csrfmiddlewaretoken"]').value;
//   data["is_delete"] = is_delete
//   //  fetch or XMLHttpRequest
//   $.ajax({
//     url: form.getAttribute("action"),
//     type: 'POST',
//     data: data,
//     cache: true,
//     success: function (data) {
//       if (data.products_total_nmb || data.products_total_nmb == 0) {
//         res =`(${data.products_total_nmb})`
//         document.querySelector('#basket_total_nmb').innerHTML = res;
//         document.querySelector('.basket-items ul').innerHTML = "";
//         $.each(data.products, function (k, v) {
//           $('.basket-items ul').append('<li>' + v.name + ', ' + v.nmb + 'шт. ' + 'по ' + v.price_per_item + 'грн  ' +
//             '<a class="delete-item" href="" data-product_id="' + v.id + '">x</a>' +
//             '</li>');
//         });
//       }
//     },
//     error: function () {
//       console.log("error")
//     }
//   })
  
// }
function calculatingBasketAmount() {
  var total_order_amount = 0;
  document.querySelectorAll('.total-product-in-basket-amount').forEach(function(currentValue, currentIndex, listObj){
    total_order_amount = total_order_amount + parseFloat(currentValue.innerHTML);
  })
  try{
    document.querySelector('#total_order_amount').innerHTML = total_order_amount.toFixed(2)
  }catch{
    console.log('total_order_amount is null')
  }
};

document.addEventListener('DOMContentLoaded', function(){
  calculatingBasketAmount();
  ////// navbar.html
  document.querySelector('.basket-container').addEventListener('mouseover', function () {
    document.querySelector('.basket-items').classList.remove('hidden');
  });
    document.querySelector('.basket-container').addEventListener('mouseout', function () {
      document.querySelector('.basket-items').classList.add('hidden');
  });
  // $(document).on('click', '.delete-item', function (e) {
  // // document.querySelector('.delete-item').addEventListener('click', function (e) {
  //   e.preventDefault();
  //   var product_id = $(this).data("product_id")
  //   // var product_id = document.querySelector('.delete-item').dataset.product_id
  //   console.log(product_id)
  //   var nmb = 0;
  //   var is_delete = true;
  //   basketUpdating(product_id, nmb, is_delete)
  // });
  ////// product.html
  // form.addEventListener('submit', function (e) {
  //   e.preventDefault();
  //   var nmb = document.querySelector('#nmb').value;
  //   var product_id = submit_btn.dataset.product_id;
  //   var is_delete = false;
  //   basketUpdating(product_id, nmb, is_delete)
  // });
  ////// checkout.html
  // document.querySelector('.product-in-basket-nmb').addEventListener('change', function(){
  $(document).on('change', ".product-in-basket-nmb", function () {
    // var current_nmb = document.querySelector('.product-in-basket-nmb').value;
    // var current_tr = document.querySelector('.product-in-basket-nmb').closest('tr');
    var current_nmb = $(this).val();
    var current_tr = $(this).closest('tr');
    var current_price = parseFloat(current_tr.find('.product-price').text()).toFixed(2);
    var total_amount = parseFloat(current_nmb * current_price).toFixed(2);
    current_tr.find('.total-product-in-basket-amount').text(total_amount);
    calculatingBasketAmount();
  });

});