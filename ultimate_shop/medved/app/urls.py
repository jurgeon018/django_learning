from django.contrib import admin
from django.urls import path, include, reverse
from django import forms
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from .models import *
from django.views.decorators.csrf import csrf_exempt


class CheckoutContactForm(forms.Form):
  name = forms.CharField(required=True)
  phone = forms.CharField(required=True)


def cart_adding(request, product_id):
  if request.method == 'POST':
    session_key = request.session.session_key
    nmb         = request.POST.get("nmb")
    is_delete   = request.POST.get("is_delete")
    if nmb is not None:
      new_product, created = ProductInBasket.objects.get_or_create(
        session_key=session_key, 
        product_id=product_id,
        is_active=True, 
        defaults={"nmb": nmb}
      )
      if not created:
        new_product.nmb += int(nmb)
        new_product.save(force_update=True)
    if is_delete=='true':
      try:
        product = ProductInBasket.objects.get(pk=product_id)
        product.delete()
      except ProductInBasket.DoesNotExist:
        print('messages.error(request, "This Product Does Not Exist"')
    return redirect(request.META['HTTP_REFERER'])
  if request.method == 'GET':
    return HttpResponse('Ha! Nothing to see here, looser! Go back to <a href="/">home</a>')

def basket_adding(request):
  session_key = request.session.session_key
  product_id  = request.POST.get("product_id")
  nmb         = request.POST.get("nmb")
  is_delete   = request.POST.get("is_delete")
  if is_delete == 'true':
    # ProductInBasket.objects.filter(id=product_id).update(is_active=False)
    # или
    # products_in_basket = ProductInBasket.objects.filter(id=product_id)
    # products_in_basket.update(is_active=False)
    # или
    ProductInBasket.objects.get(id=product_id).delete()
  elif is_delete=='false':
    new_product, created = ProductInBasket.objects.get_or_create(
      session_key=session_key, 
      product_id=product_id,
      is_active=True, 
      defaults={"nmb": nmb}
    )
    if not created:
      print ("not created")
      new_product.nmb += int(nmb)
      new_product.save(force_update=True)

  products_in_basket = ProductInBasket.objects.filter(
    session_key=session_key, 
    is_active=True, 
    order__isnull=True,
  )
  return_dict = {
    'products_total_nmb':products_in_basket.count(),
    'products': []
  }
  for product in products_in_basket:
    product_dict = {
      'id':product.id,
      'name':product.product.name,
      'price_per_item':product.price_per_item,
      'nmb':product.nmb,
    }
    return_dict["products"].append(product_dict)
  return JsonResponse(return_dict)

def checkout(request):
  session_key = request.session.session_key
  products_in_basket = ProductInBasket.objects.filter(
    session_key=session_key, 
    is_active=True, 
    order__isnull=True,
  )
  form = CheckoutContactForm(request.POST or None)
  if request.POST and form.is_valid():
    data = request.POST
    name = data.get("name")
    phone = data["phone"]
    user, created = User.objects.get_or_create(
      username=phone, 
      defaults={"first_name": name},
    )
    order = Order.objects.create(
      user=user, 
      customer_name=name, 
      customer_phone=phone, 
      status_id=1, 
    )
    total_price = 0
    for name, value in data.items():
      if name.startswith("product_in_basket_"):
        product_in_basket_id = name.split("product_in_basket_")[1]
        product_in_basket = ProductInBasket.objects.get(id=product_in_basket_id)
        product_in_basket.nmb = value
        product_in_basket.order = order
        product_in_basket.is_active = False # если не добавить это, то повторное добавление не осуществится #
        product_in_basket.save(force_update=True)
        total_price += product_in_basket.total_price 
    order.total_price = total_price
    order.save(force_update=True)
    return redirect('thank_you')
  return render(request, 'checkout.html', locals())


def home(request):
  products_images = ProductImage.objects.filter(is_active=True, is_main=True, product__is_active=True)
  products_images_phones = products_images.filter(product__category__id=1)
  products_images_laptops = products_images.filter(product__category__id=2)
  return render(request, 'home.html', locals())

def product(request, product_id):
  product = Product.objects.get(pk=product_id)
  session_key = request.session.session_key
  if not session_key:
      request.session.cycle_key()
  return render(request, 'product.html', locals())

def thank_you(request):
  return render(request, 'thank_you.html', {})

urlpatterns = [
    path('', csrf_exempt(home), name='home'),
    path('product/<product_id>/', product, name='product'),
    path('basket_adding/', basket_adding, name='basket_adding'),
    path('cart_adding/<product_id>/', cart_adding, name='cart_adding'),
    path('checkout/', csrf_exempt(checkout), name='checkout'),
    path('thank_you/',thank_you,name='thank_you')
]