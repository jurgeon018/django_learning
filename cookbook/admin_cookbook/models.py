from django.db import models
from django.utils import timezone
from django.conf import settings
from django.utils import timezone

class Person(models.Model):
    def __str__(self):
        return self.name
    name = models.CharField(("Name"), max_length=50, blank=True, null=True)


class Person2(models.Model):
    def __str__(self):
        return self.name
    name = models.CharField(("Name"), max_length=50, blank=True, null=True)


class Membership(models.Model):
    person = models.ForeignKey("Person", on_delete=models.CASCADE, blank=True, null=True)
    group = models.ForeignKey('Group', on_delete=models.CASCADE, blank=True, null=True)
    date_joined = models.DateField(blank=True, null=True)
    invite_reason = models.CharField(max_length=64, blank=True, null=True)


class Group(models.Model):
    def __str__(self):
        return self.name
    name = models.CharField(max_length=128, blank=True, null=True)
    members2 = models.ManyToManyField("Person2", blank=True, null=True)
    members = models.ManyToManyField("Person", through='Membership', blank=True, null=True)


class Friendship(models.Model):
    def __str__(self):
        return f"To {self.to_person.name} from {self.from_person.name}"
    created  = models.DateTimeField(auto_now=False, auto_now_add=True, blank=True, null=True, editable=True)
    updated  = models.DateTimeField(auto_now=True, auto_now_add=False, blank=True, null=True, editable=True)
    editable_date = models.DateTimeField(default=timezone.now)
    to_person = models.ForeignKey("Person", on_delete=models.CASCADE, related_name="friends", blank=True, null=True)
    from_person = models.ForeignKey("Person", on_delete=models.CASCADE, related_name="from_friends", blank=True, null=True)


class Category(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    class Meta:
        verbose_name_plural = "Categories"
    def __str__(self):
        return self.name


class Origin(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    def __str__(self):
        return self.name
    # can be set on Model, can be set on ModelAdmin
    def hero_count(self):
        return self.hero_set.count()
    def villain_count(self):
        return self.villain_set.count()


class Entity(models.Model):
    GENDER_MALE = "Male"
    GENDER_FEMALE = "Female"
    GENDER_OTHERS = "Others/Unknown"
    name = models.CharField(max_length=100, blank=True, null=True)
    alternative_name = models.CharField(
        max_length=100, null=True, blank=True
    )
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True)
    origin = models.ForeignKey(Origin, on_delete=models.CASCADE, blank=True, null=True)
    gender = models.CharField(
        max_length=100,
        choices=(
            (GENDER_MALE, GENDER_MALE),
            (GENDER_FEMALE, GENDER_FEMALE),
            (GENDER_OTHERS, GENDER_OTHERS),
        ), 
        blank=True, null=True
    )
    description = models.TextField(blank=True, null=True)
    def __str__(self):
        return self.name
    class Meta:
        abstract = True


class Hero(Entity):
    class Meta:
        verbose_name_plural = "Heroes"
    is_immortal = models.BooleanField(default=True, blank=True, null=True)
    benevolence_factor = models.PositiveSmallIntegerField(
        help_text="How benevolent this hero is?", blank=True, null=True
    )
    arbitrariness_factor = models.PositiveSmallIntegerField(
        help_text="How arbitrary this hero is?", blank=True, null=True
    )
    father = models.ForeignKey(
        "self", related_name="children", null=True, blank=True, on_delete=models.SET_NULL
    )
    mother = models.ForeignKey(
        "self", related_name="+", null=True, blank=True, on_delete=models.SET_NULL
    )
    spouse = models.ForeignKey(
        "self", related_name="+", null=True, blank=True, on_delete=models.SET_NULL
    )
    added_on   = models.DateTimeField(blank=True, null=True, auto_now=False, auto_now_add=True)#, default=timezone.now)
    headshot   = models.ImageField(null=True, blank=True, upload_to="hero_headshots/")
    edited_by  = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="edited",null=True, blank=True, on_delete=models.SET_NULL)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="created",null=True, blank=True, on_delete=models.SET_NULL)


class HeroProxy(Hero):
    class Meta:
        proxy = True
        verbose_name_plural = 'Hero proxies'


class Villain(Entity):
    is_immortal = models.BooleanField(default=False)
    malevolence_factor = models.PositiveSmallIntegerField(
        help_text="How malevolent this villain is?", blank=True, null=True
    )
    power_factor = models.PositiveSmallIntegerField(
        help_text="How powerful this villain is?", blank=True, null=True
    )
    is_unique = models.BooleanField(default=True, blank=True, null=True)
    count = models.PositiveSmallIntegerField(default=1, blank=True, null=True)
    is_unique = models.BooleanField(default=True, blank=True, null=True)


class Epic(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    participating_heroes = models.ManyToManyField(Hero, blank=True, null=True)
    participating_villains = models.ManyToManyField(Villain, blank=True, null=True)


class Event(models.Model):
    epic = models.ForeignKey(Epic, on_delete=models.CASCADE, blank=True, null=True)
    details = models.TextField(blank=True, null=True)
    years_ago = models.PositiveIntegerField(blank=True, null=True)


class EventHero(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, blank=True, null=True)
    hero = models.ForeignKey(Hero, on_delete=models.CASCADE, blank=True, null=True)
    is_primary = models.BooleanField(default=True)


class EventVillain(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, blank=True, null=True)
    hero = models.ForeignKey(Villain, on_delete=models.CASCADE, blank=True, null=True)
    is_primary = models.BooleanField(default=True)


class HeroAcquaintance(models.Model):
    hero     = models.OneToOneField(Hero, on_delete=models.CASCADE, blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True)
    friends  = models.ManyToManyField("admin_cookbook.Villain", verbose_name=("Friends"))
    def save(self, *args, **kwargs):
        self.category = self.hero.category
        super().save(*args, **kwargs)


class AllEntity(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    class Meta:
        managed = False
        db_table = "admin_cookbook_entity"
        verbose_name_plural = "All entities"