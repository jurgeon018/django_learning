from django.urls import path, include
from django.shortcuts import render
from django.contrib.auth.decorators import user_passes_test
from django import forms 
from django.contrib.auth import views as auth_views
from django.shortcuts import render
from admin_cookbook.custom_admin import custom_admin_site
from django.contrib import admin as default_admin

from .models import Hero


@user_passes_test(lambda u: u.is_superuser)
def custom_page_1(request):
    ctx = {'data': 'test'}
    return render(request, 'admin_custom_page.html', ctx)


def create_multiple_heroes(request):
    return render(request, 'create_multiple_heroes.html', locals())


def index(request):
    return render(request, 'index.html', locals())



urlpatterns = [
    path(
        'admin/password_reset/',
        auth_views.PasswordResetView.as_view(),
        name='admin_password_reset',
    ),
    path(
        'admin/password_reset/done/',
        auth_views.PasswordResetDoneView.as_view(),
        name='password_reset_done',
    ),
    path(
        'reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm',
    ),
    path(
        'reset/done/',
        auth_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete',
    ),
    path('admin/doc/',     include('django.contrib.admindocs.urls')),
    path('admin/',         custom_admin_site.urls),
    # path('default-admin/', default_admin.site.urls),
    # path('event-admin/',   event_admin_site.urls),
    # path('entity-admin/',  entity_admin_site.urls),
    path('custom_page_1/', custom_page_1, name='custom_page_1'),
    path('create_multiple_heroes/', create_multiple_heroes, name='create_multiple_heroes'),
    path('', index, name='index'),


]
