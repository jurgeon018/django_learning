from django.db.models import (
    Q, Subquery, OuterRef, F, Value,
    Case, When, IntegerField, 
    Avg, Max, Min, Sum, Count,
    Func,
)
from django.db.models.functions import (
    Length, Upper, Lower
)
from django.contrib.auth import get_user_model






##############################################
# Querying and Filtering

# 1.Show raw sql-query
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/query.html
qs = Event.objects.all()
print(qs.query)
qs = Event.objects.filter(years_ago__gt=5)
print(qs.query)


# 2. OR query
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/or_query.html
qs = User.objects.filter(
    Q(first_name__startswith='R') \ 
    | Q(last_name__startswith='D'))
qs = User.objects.filter(first_name__startswith='R') \
    | User.objects.filter(last_name__startswith='D')


# 3. AND query
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/and_query.html
qs = User.objects.filter(
    first_name__startswith='R',
    last_name__startswith='D'
)
qs = User.objects.filter(first_name__startswith='R')\
    & User.objects.filter(last_name__startswith='D')
qs = User.objects.filter(
    Q(first_name__startswith='R') &
    Q(last_name__startswith='D')
)


# 4. NOT equal query
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/notequal_query.html
qs = User.objects.filter(~Q(id__lt=5))
qs = User.objects.exclude(id__lt=5)


# 5. Union
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/union.html
q1 = User.objects.filter(id__gte=5)
q2 = User.objects.filter(id__lte=9)
q3 = Villain.objects.all()
q1.union(q2)
q2.union(q1)
q1.union(q3) 
# django.db.utils.OperationalError: SELECTs to the left and right of UNION do not have the same number of result columns
q2.union(q3)
# django.db.utils.OperationalError: SELECTs to the left and right of UNION do not have the same number of result columns

villaion_values = Villain.objects.all().values_list("name", "gender")
user_values     = User.objects.all().values_list("name", "gender")
user_values.union(villaion_values)
# The union operation can be performed only with the querysets having same fields and the datatypes


# 6. Select only some fields
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/select_some_fields.html
qs = User.objects.filter(
    first_name__startswith='R'
).values('first_name', 'last_name')
qs = User.objects.filter(
    first_name__startswith='R'
).values_list('first_name', 'last_name')
qs = User.objects.filter(
    first_name__startswith='R'
).only("first_name", "last_name")
# The only difference between only and values is only also fetches the id


# 7. How to do a subquery expression in Django?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/subquery.html
UserParent.objects.filter(
    user_id__in=Subquery(User.objects.all().values('id'))
)
class Category(models.Model):
    name = models.CharField(max_length=100)
class Hero(models.Model):
    name = models.CharField(max_length=100)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    benevolence_factor = models.PositiveSmallIntegerField(
        help_text="How benevolent this hero is?",
        default=50
    )
qs = Hero.objects.filter(
    category=OuterRef("pk")
).order_by(
    "-benevolence_factor"
).values('name')[:1]
Category.objects.all().annotate(most_benevolent_hero=Subquery(qs))




# 8. How to filter a queryset with criteria based on comparing their field values
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/f_query.html
User.objects.create_user(email="shabda@example.com", username="shabda", first_name="Shabda", last_name="Raaj")
User.objects.create_user(email="guido@example.com",  username="Guido",  first_name="Guido",  last_name="Guido")
User.objects.create_user(email="guido2@example.org", username="Tim",    first_name="Tim",    last_name="Teters")
User.objects.filter(first_name__startswith='R')
User.objects.filter(last_name=F("first_name"))
# allows to find the users where last_name==first_name
User.objects.annotate(
    first=Substr("first_name", 1, 1), 
    last=Substr("last_name", 1, 1)
).filter(
    first=F("last")
)


# 9. How to filter FileField without any file?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/filefield.html
no_files_objects = MyModel.objects.filter(
    Q(file='')|Q(file=None)
)


# 10. How to perform join operations in django ORM?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/join.html
a1 = Article.objects.select_related('reporter') # Using select_related
a2 = Article.objects.filter(reporter__username='John')


# 11. How to find second largest record using Django ORM ?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/second_largest.html
first = User.objects.order_by('-last_login').first()
second_largest = User.objects.order_by('-last_login')[1] 
third_highest  = User.objects.order_by('-last_login')[2] 
last  = User.objects.order_by('-last_login').last()


# 12. Find rows which have duplicate field values
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/duplicate.html
duplicates = User.objects.values(
    'first_name'
).annotate(
    name_count=Count('first_name')
).filter(name_count__gt=1)
items = [item['first_name'] for item in duplicates]
users = User.objects.filter(first_name__in=items)


# 13. How to find distinct field values from queryset?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/distinct.html
distinct = User.objects.values(
    'first_name'
).annotate(
    name_count=Count('first_name')
).filter(name_count=1)
items = [item['first_name'] for item in distinct]
users = User.objects.filter(first_name__in=items)
# Will find users whose names have not been repeated
User.objects.distinct("first_name").all() 
# will pull up the first record when it encounters a distinct first_name


# 14. How to use Q objects for complex queries?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/query_relatedtool.html
# first_name starts with ‘R’, or if the last_name starts with ‘Z’
queryset = User.objects.filter(
    Q(first_name__startswith='R') | Q(last_name__startswith='D')
)
# first_name starts with ‘R’, and if the last_name starts with ‘Z’
queryset = User.objects.filter(
    Q(first_name__startswith='R') & Q(last_name__startswith='D')
)
# first_name starts with ‘R’, but not if the last_name starts with ‘Z’
queryset = User.objects.filter(
    Q(first_name__startswith='R') & ~Q(last_name__startswith='Z')
)


# 15. How to group records in Django ORM?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/aggregation.html

User.objects.all().aggregate(Avg('id'))
User.objects.all().aggregate(Max('id'))
User.objects.all().aggregate(Min('id'))
User.objects.all().aggregate(Sum('id'))
{'id__avg': 7.571428571428571}
{'id__max': 15}
{'id__min': 1}
{'id__sum': 106}

# 16. How to efficiently select a random object from a model?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/random.html
def get_random1():
    Category.objects.order_by("?").first()
def get_random2():
    max_id = Category.objects.all().aggregate(max_id=Max("id"))['max_id']
    pk = random.randint(1, max_id)
    return Category.objects.get(pk=pk)
def get_random3():
    # If your models has deletions, you can slightly modify the functions, 
    # to loop until you get a valid Category.
    max_id = Category.objects.all().aggregate(max_id=Max("id"))['max_id']
    while True:
        pk = random.randint(1, max_id)
        category = Category.objects.filter(pk=pk).first()
        if category:
            return category
timeit.timeit(get_random3, number=100)
0.20055226399563253
timeit.timeit(get_random, number=100)
56.92513192095794

# 17. How to use arbitrary database functions in querysets?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/func_expressions.html
Hero.objects.create(name="Zeus",     description="A greek God", benevolence_factor=80, category_id=12, origin_id=1)
Hero.objects.create(name="ZeuX",     description="A greek God", benevolence_factor=80, category_id=12, origin_id=1)
Hero.objects.create(name="Xeus",     description="A greek God", benevolence_factor=80, category_id=12, origin_id=1)
Hero.objects.create(name="Poseidon", description="A greek God", benevolence_factor=80, category_id=12, origin_id=1)
Hero.objects.annotate(
    like_zeus=Func(
        F('name'), 
        function='levenshtein', 
        template="%(function)s(%(expressions)s, 'Zeus')"
    )
).filter(
    like_zeus__lt=2
)
# or
class LevenshteinLikeZeus(Func):
    function='levenshtein'
    template="%(function)s(%(expressions)s, 'Zeus')"
Hero.objects.annotate(
    like_zeus=LevenshteinLikeZeus(
        F("name")
    )
).filter(
    like_zeus__lt=2
)









#############################################
# Creating, Updating and Deleting things

# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/multiple_objects.html
# 1. How to create multiple objects in one shot?
Category.objects.bulk_create([
    Category(name="God"),
    Category(name="Demi God"),
    Category(name="Mortal")
])


# 2. How to copy or clone an existing model object?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/copy.html
hero = Hero.objects.get(name="needed_name")
hero.pk = None
hero.save()


# 3. How to ensure that only one object can be created?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/singleton.html
class Origin(models.Model):
    name = models.CharField(max_length=100)
    def save(self, *args, **kwargs):
        if self.__class__.objects.count():
            self.pk = self.__class__.objects.first().pk
        super().save(*args, **kwargs)


# 4. How to update denormalized fields in other models on save?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/update_denormalized_fields.html



# 5. How to perform truncate like operation using Django ORM?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/truncate.html
class Category(models.Model):
    # ...
    @classmethod
    def truncate(cls):
        with connection.cursor() as cursor:
            cursor.execute('TRUNCATE TABLE "{0}" CASCADE'.format(cls._meta.db_table))
Category.objects.all().delete() # uses DELETE FROM ...
Category.truncate() # uses TRUNCATE TABLE ...


# 6. What signals are raised by Django during object creation or update?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/signals.html
pre_init
post_init
pre_save
post_save
pre_delete
post_delete


# 7. How to convert string to datetime and store in database?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/datetime.html
user = User.objects.get(id=1)
date_str = "2018-03-11"
from django.utils.dateparse import parse_date # Way 1
from datetime import datetime # Way 2
temp_date1 = parse_date(date_str)
temp_date2 = datetime.strptime(date_str, "%Y-%m-%d").date()
a1 = Article(headline="String converted to date", pub_date=temp_date1)
a2 = Article(headline="String converted to date way 2", pub_date=temp_date2)
a1.save()
a2.save()










#############################################
# Ordering things

# 1. How to order a queryset in ascending or descending order?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/asc_or_desc.html
User.objects.all().order_by('date_joined') 
User.objects.all().order_by('-date_joined') 
User.objects.all().order_by('date_joined', '-last_login')



# 2. How to order a queryset in case insensitive manner?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/case_insensitive.html
User.objects.all().order_by('username').values_list('username', flat=True)
# order_by makes the ordering alphabetically and w.r.t case
User.objects.all().order_by(Lower('username')).values_list('username', flat=True)


# 3. How to order on two fields
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/order_by_two.html
User.objects.all().order_by("is_active", "-last_login", "first_name") 


# 4. How to order on a field from a related model (with a foreign key)?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/order_by_related_model.html
class Category(models.Model):
    name = models.CharField(max_length=100)
class Hero(models.Model):
    name = models.CharField(max_length=100)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
Hero.objects.all().order_by('category__name', 'name')


# 5. How to order on an annotated field?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/order_by_annotated_field.html
Category.objects.annotate(
    hero_count=Count("hero")
).order_by(
    "-hero_count"
)









##############################################
# Database Modelling
# 1. How to model one to one relationships?
# 2. How to model one to many relationships?
# 3. How to model many to many relationships?
# 4. How to include a self-referencing ForeignKey in a model
# 5. How to convert existing databases to Django models?
# 6. How to add a model for a database view?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/database_view.html
class TempUser(models.Model):
    first_name = models.CharField(max_length=100)
    class Meta:
        managed = False
        db_table = "temp_user"


# 7. How to create a generic model which can be related to any kind of entity? (Eg. a Category or a Comment?)
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/generic_models.html
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
class FlexCategory(models.Model):
    name = models.SlugField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
class Hero(models.Model):
    name = models.CharField(max_length=100)
    flex_category = GenericRelation(FlexCategory, related_query_name='flex_category')
class Villain(models.Model):
    name = models.CharField(max_length=100)
    flex_category = GenericRelation(FlexCategory, related_query_name='flex_category')
FlexCategory.objects.create(content_object=hero, name="mythic")
FlexCategory.objects.create(content_object=hero, name="ghost")



# 8. How to specify the table name for a model?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/table_name.html
class TempUser(models.Model):
    first_name = models.CharField(max_length=100)
    class Meta:
        db_table = "temp_user"


# 9. How to specify the column name for model field?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/column_name.html
class ColumnName(models.Model):
    a = models.CharField(max_length=40, db_column='column1')
    column2 = models.CharField(max_length=50)

# 13. How to add multiple databases to the django application ?
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/multiple_databases.html
DATABASE_ROUTERS = ['path.to.DemoRouter']
DATABASE_APPS_MAPPING = {
    'user_data': 'users_db',
    'customer_data':'customers_db'
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
    'users_db': {
        'NAME': 'user_data',
        'ENGINE': 'django.db.backends.postgresql',
        'USER': 'postgres_user',
        'PASSWORD': 'password'
    },
    'customers_db': {
        'NAME': 'customer_data',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'mysql_cust',
        'PASSWORD': 'root'
    }
}

class DemoRouter:
    """
    A router to control all database operations on models in the
    user application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read user models go to users_db.
        """
        if model._meta.app_label == 'user_data':
            return 'users_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write user models go to users_db.
        """
        if model._meta.app_label == 'user_data':
            return 'users_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the user app is involved.
        """
        if obj1._meta.app_label == 'user_data' or \
            obj2._meta.app_label == 'user_data':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth app only appears in the 'users_db'
        database.
        """
        if app_label == 'user_data':
            return db == 'users_db'
        return None
class User(models.Model):
    username = models.Charfield(ax_length=100)
        class Meta:
        app_label = 'user_data'
class Customer(models.Model):
    name = models.TextField(max_length=100)
        class Meta:
        app_label = 'customer_data'
python manage.py migrate --database=users_db