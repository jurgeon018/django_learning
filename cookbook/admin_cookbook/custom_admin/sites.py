from django.contrib import admin

class CustomAdminSite(admin.AdminSite):
    site_header = 'Header'
    site_title  = 'Title'
    site_index  = 'Index'
    # def get_app_list(self, request):
    #     ordering = {
    #         "Heroes":
    #         1,
    #         "Origins":
    #         2,
    #         "Villains":
    #         3,
    #         "Categories":
    #         4,
    #         "All entities":
    #         5,
    #         "Hero proxies":
    #         6
    #     }
    #     app_dict = self._build_app_dict(request)
    #     app_list = sorted(app_dict.values(), key=lambda x: x['name'].lower())
    #     for app in app_list:
    #         app['models'].sort(key=lambda x: ordering[x['name']])
    #     return app_list
    #     from django.contrib.admin.models import(
    #     ADDITION,
    #     CHANGE,
    #     DELETION, 
    #     LogEntry,
    # )
    # addition = LogEntry.objects.filter(action_flag=ADDITION).first()
    # change   = LogEntry.objects.filter(action_flag=CHANGE).first()
    # deletion = LogEntry.objects.filter(action_flag=DELETION).first()



class EntityAdminSite(admin.AdminSite):
    site_header = "EventAdmin Site Header"
    site_title  = "EventAdmin Site Title"
    index_title = "EventAdmin Index Title"


class EventAdminSite(admin.AdminSite):
    site_header = "EventAdmin Site Header"
    site_title  = "EventAdmin Site Title"
    index_title = "EventAdmin Index Title"


custom_admin_site = CustomAdminSite(name="custom_admin_site")
event_admin_site  = EventAdminSite(name="event_admin_site")
entity_admin_site = EntityAdminSite(name="entity_admin_site")


from .admin import *

# admin.site.unregister(User)
# admin.site.unregister(Group)

custom_admin_site.register(Person, PersonAdmin)
custom_admin_site.register(Person2, Person2Admin)
custom_admin_site.register(Membership)
custom_admin_site.register(Group, GroupAdmin)
custom_admin_site.register(Friendship)
custom_admin_site.register(Category, CategoryAdmin)
custom_admin_site.register(Origin, OriginAdmin)
custom_admin_site.register(Hero, HeroAdmin)
custom_admin_site.register(HeroProxy, HeroProxyAdmin)
custom_admin_site.register(Villain, VillainAdmin)
custom_admin_site.register(Epic)
custom_admin_site.register(Event)
custom_admin_site.register(EventHero)
custom_admin_site.register(EventVillain)
custom_admin_site.register(HeroAcquaintance)
# custom_admin_site.register(AllEntity, AllEntiryAdmin) #https://books.agiliq.com/projects/django-admin-cookbook/en/latest/database_view.html
# custom_admin_site.add_action(export_selected_objects, 'expost selected')
# custom_admin_site.disable_action('delete_selected')


event_admin_site.register(Event)
event_admin_site.register(EventHero)
event_admin_site.register(EventVillain)
