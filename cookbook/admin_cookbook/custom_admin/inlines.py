from django.contrib import admin 
from ..models import *


class PersonInline(admin.StackedInline):
    model = Person


class Person2Inline(admin.StackedInline):
    model = Person2


class MembershipInline(admin.StackedInline):
    model = Membership


class MembershipInline2(admin.StackedInline):
    model = Group.members2.through


class GroupInline(admin.StackedInline):
    model = Group


class FriendshipInline(admin.StackedInline):
    model = Friendship
    fk_name = 'to_person' # 'from_person'


class CategoryInline(admin.StackedInline):
    model = Category


class OriginInline(admin.StackedInline):
    model = Origin


class EpicInline(admin.StackedInline):
    model = Epic


class EventInline(admin.StackedInline):
    model = Event


class EventHeroInline(admin.StackedInline):
    model = EventHero


class EventVillainInline(admin.StackedInline):
    model = EventVillain


class VillainInline(admin.StackedInline):
    model = Villain
    # form
    # fieldsets
    # formfield_overrides
    # filter_horizontal
    # filter_vertical
    # prepopulated_fields
    # get_queryset()
    # radio_fields
    # raw_id_fields
    # formfield_for_choice_field()
    # formfield_for_foreignkey()
    # formfield_for_manytomany()
    # has_module_permission()
    # formset
    # classes
    # max_num
    # min_num
    # get_extra()
    # template
    # get_formset(request, obj=None, **kwargs)
    # get_max_num(request, obj=None, **kwargs)
    # get_min_num(request, obj=None, **kwargs)


class HeroAcquaintanceInline(admin.TabularInline):
    model = HeroAcquaintance


class AllEntityInline(admin.StackedInline):
    model = AllEntity