import csv 
import sys
from django.urls import path
from django.db.models import Count
from django.shortcuts import render, redirect
from django.utils.html import mark_safe
from django.urls import reverse 
from django.core import serializers
from django.contrib.auth import get_permission_codename
from django.contrib import admin
from ..models import *
from .inlines import *
from .utils import *
from .mixins import *
from .filters import *
from .forms import *
from .sites import custom_admin_site



# @admin.register(Person, site=custom_admin_site)
class PersonAdmin(admin.ModelAdmin):
    inlines = [
        MembershipInline,
        FriendshipInline
    ]


# @admin.register(Person2, site=custom_admin_site)
class Person2Admin(admin.ModelAdmin):
    inlines = [
        MembershipInline2,
    ]


# @admin.register(Group, site=custom_admin_site)
class GroupAdmin(admin.ModelAdmin):
    inlines = [
        MembershipInline,
    ]
    exclude = ('members',)



# @admin.register(Category, site=custom_admin_site)
class CategoryAdmin(admin.ModelAdmin):
    inlines = [VillainInline]


# @admin.register(Origin, site=custom_admin_site)
class OriginAdmin(admin.ModelAdmin):
    list_display = (
        "name", 
        "hero_count", 
        "villain_count"
    )
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.annotate(
            _hero_count=Count("hero", distinct=True),
            _villain_count=Count("villain", distinct=True),
        )
        return queryset
    # can be set on Model, can be set on ModelAdmin
    def hero_count(self, obj):
        count = obj.hero_set.count()
        # or using queryset and annotation
        count = obj._hero_count
        return count
    def villain_count(self, obj):
        count = obj.villain_set.count()
        # or using queryset and annotation
        count = obj._villain_count
        return count
    hero_count.admin_order_field = '_hero_count'
    villain_count.admin_order_field = '_villain_count'


# @admin.register(Hero, site=custom_admin_site)
class HeroAdmin(admin.ModelAdmin, ExportCsvMixin):
    exclude = [
        'created_by',
        'edited_by',
    ]
    list_display = (
        "name",
        "is_immortal",
        "category",
        "origin",
        "is_very_benevolent",
        'children_display',
    )
    # list_display_links
    # list_editable
    # ordering
    save_as = False
    save_as_continue = True 
    save_on_top = False
    
    list_filter = (
        "is_immortal",
        "category",
        "origin",
        IsVeryBenevolentFilter
    )
    readonly_fields = [
        "father", 
        "mother", 
        "spouse",
        "headshot_image",
        "added_on",
    ]
    raw_id_fields = [
        # "category"
    ]
    actions = [
        "mark_immortal", 
        "export_as_csv",
    ]
    change_list_template = 'heroes_changelist.html'
    inlines = [HeroAcquaintanceInline]
    form = HeroForm
    list_per_page = 250
    list_per_page = sys.maxsize
    date_hierarchy = 'added_on'
    actions_on_top = True
    actions_on_bottom = False
    actions_selection_counter = True
    def is_very_benevolent(self, obj):
        return obj.benevolence_factor > 75
    is_very_benevolent.boolean = True
    def mark_immortal(self, request, queryset):
        queryset.update(is_immortal=True)
    mark_immortal.short_description = "Mark selected heroes as immortal"
    mark_immortal.allowed_permissions = ('custom',) #https://docs.djangoproject.com/en/2.2/ref/contrib/admin/actions/#setting-permissions-for-actions
    def children_display(self, obj):
        option = "change" # "delete | history | change"
        # display_text = ", ".join([
        #     "<a href={}>{}</a>".format(
        #             reverse('admin:{}_{}_{}'.format(obj._meta.app_label, obj._meta.model_name, option),
        #             args=(child.pk,)),
        #         child.name)
        #     for child in obj.children.all()
        # ])
        display_text = []
        for child in obj.children.all():
            app   = obj._meta.app_label
            model = obj._meta.model_name
            url   = f'admin:{app}_{model}_{option}'
            args  = (child.pk,)
            href  = reverse(url, args=args)
            name  = child.name
            display_text.append(f"<a href={href}>{name}</a>")
        ', '.join(display_text)
        if display_text:
            return mark_safe(display_text)
        return "-"
    children_display.short_description = "Children"

    class Media:
        js = (
            'js/admin/placeholder.js',
        )
        css = {
            'all': (
                'css/admin/styles.css',
            )
        }
    def export_as_json(modeladmin, request, queryset):
        response = HttpResponse(content_type="application/json")
        serializers.serialize('json', queryset, stream=response)
        return response
    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def get_urls(self):
        my_urls = [
            path('immortal/', self.set_immortal, name="set_immortal"),
            path('mortal/', self.set_mortal, name="set_mortal"),
            path('import-csv/', self.import_csv, name="import_csv"),
        ]
        return my_urls + super().get_urls()
    def set_immortal(self, request):
        self.model.objects.all().update(is_immortal=True)
        self.message_user(request, 'All heroes are now immortal')
        return HttpResponseRedirect('../')
    def set_mortal(self, request):
        self.model.objects.all().update(is_immortal=False)
        self.message_user(request, 'All heroes are now mortal')
        return HttpResponseRedirect("../")
    def import_csv(self, request):
        if request.method == 'POST':
            csv_file = request.FILES['csv_file']
            csv_file = csv_file.read().decode('utf-8').splitlines()
            reader = csv.reader(csv_file)
            # Create Hero objects from passed in data
            # ...
            for row in reader:
                print(row)
            self.message_user(request, "Your csv file has been imported")
            return redirect('..')
        form = CsvImportForm()
        payload = {'form':form}
        return render(request, 'csv_form.html', payload)

    def save_model(self, request, obj, form, change):
        category_name = form.cleaned_data['category_name']    
        category, _   = Category.objects.get_or_create(name=category_name)
        obj.category  = category
        obj.edited_by = request.user
        if not obj.pk:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)
    # def has_add_permission(self, request):
    #     # if self.model.objects.count() < 1:
    #     #     return super().has_add_permission(request)
    #     return False 
    # def has_change_permission(self, request, obj=None):
    #     pass
    # def has_delete_permission(self, request, obj=None):
    #     return False
    # def has_view_permission()(self, request, obj=None):
    #     pass
    # def has_module_permission(self, request):
    #     pass # дает ошибку
    def has_custom_permission(self, request):
        opts = self.opts
        codename = get_permission_codename('custom', opts)
        return request.user.has_perm('%s.%s' % (opts.app_label, codename))
    def headshot_image(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
            url = obj.headshot.url,
            width=obj.headshot.width,
            height=obj.headshot.height,
            )
    )
    # def get_readonly_fields(self, request, obj=None):
    #     """
    #     makes a field editable while creating, 
    #     but read only in existing objects
    #     """
    #     if obj:
    #         return ["name", "category"]
    #     else:
    #         return []
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        #filters FK dropdown values
        # if db_field.name == "category":
        #     kwargs["queryset"] = Category.objects.filter(name__in=['God', 'Demi God'])
        if db_field.name == 'category':
            return CategoryChoiceField(queryset=Category.objects.all())
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


# @admin.register(HeroProxy, site=custom_admin_site)
class HeroProxyAdmin(admin.ModelAdmin):
    readonly_fields = (
        "name", "is_immortal", "category", "origin",
    )


# @admin.register(Villain, site=custom_admin_site)
class VillainAdmin(admin.ModelAdmin, ExportCsvMixin):
    def response_change(self, request, obj):
        if "_make-unique" in request.POST:
            matching_names_except_this = self.get_queryset(request).filter(name=obj.name).exclude(pk=obj.id)
            matching_names_except_this.delete()
            obj.is_unique = True
            obj.save()
            self.message_user(request, "This villain is now unique")
            return HttpResponseRedirect(".")
        return super().response_change(request, obj)
    list_display = (
        "name",
        "category",
        "origin",
    )
    actions = (
        "export_as_csv"
    )
    change_form_template = "villain_changeform.html"

