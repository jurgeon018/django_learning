from django import forms 
from ..models import *


class CsvImportForm(forms.Form):
    csv_file = forms.FileField()


class HeroForm(forms.ModelForm):
    category_name = forms.CharField(required=False)
    class Meta:
        model = Hero
        exclude = [
            # "category",
        ]


class CategoryChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return f"Category: {obj.name}"