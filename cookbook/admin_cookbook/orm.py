# https://docs.djangoproject.com/en/2.2/ref/models/expressions/
from django.db.models import (
    Q, Subquery, OuterRef, F, Value,
    Case, When, IntegerField, 
    Avg, Max, Min, Sum, Count,
    Func,
)
from django.db.models.functions import (
    Length, Upper, Lower
)



class Meta:
    # https://docs.djangoproject.com/en/2.2/ref/models/options/
    db_table
    abstract
    app_label
    base_manager_name
    db_tablespace
    default_manager_name
    default_related_name
    get_latest_by
    managed
    order_with_respect_to
    ordering
    permissions
    default_permissions
    proxy
    required_db_features
    required_db_vendor
    select_on_save
    indexes
    unique_together
    index_together
    constraints
    verbose_name
    verbose_name_plural
    # Read-only Meta attributes
    label
    label_lower



# https://docs.djangoproject.com/en/2.2/ref/models/querysets/
.all()
.get()
.bulk_create()
.create()
.get_or_create()
.update_or_create()
.delete()
.update()
.save()
.order_by()
.filter()
.exclude()
.in_bulk()
.count()
.first()
.last()
.annotate()
.query
.values_list()
.values()
.only()
.select_related()
.prefetch_related()
.distinct()
.aggregate()
.raw('SELECT * FROM myapp_person')
.add() # m2m
.remove() #m2m
.clear() #m2m










# https://docs.djangoproject.com/en/2.2/topics/db/sql/
from django.db import connection


with connection.cursor() as cursor:
    cursor.execute("UPDATE bar SET foo = 1 WHERE baz = %s", [self.baz])
    cursor.execute("SELECT foo FROM bar WHERE baz = %s", [self.baz])
    row = cursor.fetchone()
