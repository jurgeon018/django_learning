from django.urls import path, include
from django.contrib import admin

from updates.views import (
            json_example_view, 
            JsonCBV, 
            JsonCBV2, 
            SerializedDetialView, 
            SerializedListView
    )

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/auth/', include('accounts.api.urls')),
    path('api/user/', include('accounts.api.user.urls')),
    path('api/status/', include('status.api.urls')),
    path('api/updates/', include('updates.api.urls')), 
]

from django.conf import settings

from django.conf.urls.static import static

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
