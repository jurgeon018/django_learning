from rest_framework import (
  mixins, generics, serializers, 
  status, routers, urlpatterns, 
  views, decorators, response, 
  parsers, renderers, reverse,
  pagination, viewsets, permissions,
  authentication, exceptions, throttling,
  schemas, 
)
from rest_framework.mixins import (
  ListModelMixin, 
  CreateModelMixin, 
  RetrieveModelMixin,
  UpdateModelMixin,
  DestroyModelMixin, 
)
# https://www.django-rest-framework.org/api-guide/generic-views/
from rest_framework.generics import (
  GenericAPIView,

  ListAPIView, 
  CreateAPIView, 
  RetrieveAPIView, 
  UpdateAPIView,
  DestroyAPIView, 

  ListCreateAPIView, 

  RetrieveDestroyAPIView, 
  RetrieveUpdateAPIView, 
  RetrieveUpdateDestroyAPIView
)
# https://www.django-rest-framework.org/api-guide/serializers/
from rest_framework.serializers import (
  Serializer,
  ListSerializer,
  ModelSerializer,
  BaseSerializer,
  HyperlinkedModelSerializer, 
  ValidationError,


)
# https://www.django-rest-framework.org/api-guide/fields/
from rest_framework.fields import (
  EmailField, CharField, DateTimeField, 
  DateField, IntegerField, ReadOnlyField,
  ChoiceField, 
)
# https://www.django-rest-framework.org/api-guide/relations/
from rest_framework.serializers import (
  PrimaryKeyRelatedField, 
  HyperlinkedRelatedField, 
  HyperlinkedIdentityField,
  SerializerMethodField,
  
)
# https://www.django-rest-framework.org/api-guide/validators/
from rest_framework.validators import (
  UniqueValidator
)
# from rest_framework.status import 
# https://www.django-rest-framework.org/api-guide/routers/
from rest_framework.routers import (
  DefaultRouter, Route, DynamicRoute, 
  SimpleRouter,
)
from rest_framework.urlpatterns import format_suffix_patterns
# https://www.django-rest-framework.org/api-guide/views/
from rest_framework.views import APIView
from rest_framework.decorators import (
  api_view, action, throttle_classes, 
  schema, parser_classes, 
)
from rest_framework.response import Response
# https://www.django-rest-framework.org/api-guide/parsers/
from rest_framework.parsers import (
  JSONParser, FormParser, MultiPartParser,
  FileUploadParser, BaseParser
)
# https://www.django-rest-framework.org/api-guide/renderers/
from rest_framework.renderers import (
  JSONRenderer, TemplateHTMLRenderer, StaticHTMLRenderer,
  BrowsableAPIRenderer, AdminRenderer, HTMLFormRenderer,
  MultiPartRenderer,
)
from rest_framework.reverse import reverse
from rest_framework.pagination import PageNumberPagination
# https://www.django-rest-framework.org/api-guide/viewsets/
from rest_framework.viewsets import (
  ViewSet,
  GenericViewSet,
  ModelViewSet,
  ReadOnlyModelViewSet,
)
# https://www.django-rest-framework.org/api-guide/permissions/
from rest_framework.permissions import (
  AllowAny
  IsAuthenticated
  IsAdminUser, 
  IsAuthenticatedOrReadOnly, 
  IsOwnerOrReadOnly,
  IsAuthenticated, 
  IsAdminOrIsSelf,
  DjangoModelPermissions
  DjangoModelPermissionsOrAnonReadOnly
  DjangoObjectPermissions
)
# https://www.django-rest-framework.org/api-guide/authentication/#basicauthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication,

from rest_framework.exceptions import (
  APIException
)

from rest_framework.throttling import (
  UserRateThrottle
)

from rest_framework.schemas import (
  AutoSchema
)

from rest_framework.filters import (
  SearchFilter,
  OrderingFilter,
)
