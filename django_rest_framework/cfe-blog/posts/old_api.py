from django.urls import path
from django.contrib import admin
from rest_framework import (
  filters, permissions, generics, mixins, 
  status, decorators, serializers, pagination,
)
from posts.models import Post
from django.db.models import Q

# pagination
class PostLimitOffsetPagination(pagination.LimitOffsetPagination):
  default_limit = 2
  max_limit = 10

class PostPageNumberPagination(pagination.PageNumberPagination):
  page_size = 3



# permissions
class IsOwnerOrReadOnly(permissions.BasePermission):
  message = 'You must be owner of this object.'
  def has_permission(self, request, view):
    if request.method in permissions.SAFE_METHODS:
      return True
    return False
  def has_object_permission(self, request, view, obj):
    return obj.user == request.user

post_detail_url = serializers.HyperlinkedIdentityField(
  view_name='posts-api:detail',
  lookup_field='slug',
)


# serializers
class PostListSerializer(serializers.ModelSerializer):
  url = serializers.HyperlinkedIdentityField(
    view_name='posts-api:detail',
    lookup_field='slug'
  )
  delete_url = serializers.HyperlinkedIdentityField(
    view_name='posts-api:delete',
    lookup_field='slug'
  )
  user_field = serializers.SerializerMethodField()
  class Meta:
    model = Post
    fields = [
      'url',
      'user_field',
      'delete_url',
      'title', 
      'content', 
      'publish']
  def get_user_field(self, obj):
    return str(obj.user.username)
  
class PostRetrieveSerializer(serializers.ModelSerializer):
  url = post_detail_url
  user_field = serializers.SerializerMethodField()
  image = serializers.ImageField()
  markdown = serializers.SerializerMethodField()
  class Meta:
    model = Post
    # fields = '__all__'
    fields = [
      'id',
      'url',
      'title',
      'markdown',
      'image',
      'user_field',
      'slug',
      'content',
      'publish',
    ]
  def get_markdown(self, obj):
    return obj.get_markdown()
  def get_user_field(self, obj):
    return str(obj.user.username)
  def get_image(self, obj):
    try: 
      image = obj.image.url
    except:
      image = None
    return image


class PostCreateUpdateSerializer(serializers.ModelSerializer):
  class Meta:
    model = Post
    # fields = '__all__'
    fields = [
      'title',
      'content', 
      'publish'
    ]


# views
class PostListAPIView(generics.ListAPIView):
  serializer_class = PostListSerializer
  filter_backends = [filters.SearchFilter, filters.OrderingFilter]
  search_fields = ['title', 'content', 'user__first_name']
  queryset = Post.objects.all()
  # pagination_class = pagination.PageNumberPagination
  # pagination_class = pagination.LimitOffsetPagination
  # pagination_class = PostLimitOffsetPagination
  pagination_class = PostPageNumberPagination

  
  def get_queryset(self, *args, **kwargs):
    # queryset_list = super().get_queryset(self, *args, **kwargs)
    queryset_list = Post.objects.all()#.filter(user=self.request.user)
    query = self.request.GET.get("q")
    if query:
      queryset_list = queryset_list.filter(
        Q(title__icontains=query)|
        Q(content__icontains=query)|
        Q(user__first_name__icontains=query) |
        Q(user__last_name__icontains=query)
      ).distinct()
    return queryset_list


class PostCreateAPIView(generics.CreateAPIView):
  queryset = Post.objects.all()
  serializer_class = PostCreateUpdateSerializer
  lookup_field = 'slug'
  permission_classes = [permissions.IsAuthenticated, permissions.IsAdminUser]
  # def perform_update(self, serializer):
  def perform_create(self, serializer):
    serializer.save(
      user=self.request.user, 
      # title='my title'
    )

class PostRetrieveAPIView(generics.RetrieveAPIView):
  queryset = Post.objects.all()
  serializer_class = PostRetrieveSerializer
  lookup_field = 'slug'

class PostUpdateAPIView(generics.RetrieveUpdateAPIView):
  queryset = Post.objects.all()
  serializer_class = PostCreateUpdateSerializer
  lookup_field = 'slug'
  permission_classes = [permissions.IsAuthenticated, 
    permissions.IsAdminUser, IsOwnerOrReadOnly
  ]

class PostDestroyAPIView(generics.DestroyAPIView):
  queryset = Post.objects.all()
  serializer_class = PostRetrieveSerializer
  lookup_field = 'slug'
  permission_classes = [permissions.IsAuthenticated, permissions.IsAdminUser]

# urls
app_name = 'posts-api'

urlpatterns = [
	path('', PostListAPIView.as_view(), name='list'),
  path('create/', PostCreateAPIView.as_view(), name='create'),
  path('<slug>/', PostRetrieveAPIView.as_view(), name='detail'),
  path('<slug>/edit/', PostUpdateAPIView.as_view(), name='update'),
  path('<slug>/delete/', PostDestroyAPIView.as_view(), name='delete'),

]
