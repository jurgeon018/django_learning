from django.conf import settings
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib import admin

from accounts.views import (login_view, register_view, logout_view)
from rest_framework_jwt.views import obtain_jwt_token
urlpatterns = [
    
    path('admin/', admin.site.urls),
    path('comments/', include("comments.urls")),


    path('register/', register_view, name='register'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('', include("posts.urls")),

    path('api/auth/token/', obtain_jwt_token),
    # path('api/accounts/', include('accounts.api.urls')),
    # path('api/comments/', include('comments.api.urls')),
    # path('api/posts/', include('posts.api.urls')),
    # path('api/accounts/', include('api.accounts.urls')),
    # path('api/comments/', include('api.comments.urls')),
    # path('api/posts/', include('api.posts.urls')),

    path('rest-auth', include('rest_framework.urls')),
    # path('accounts/', include('allauth.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)







# from django.conf import settings
# from django.conf.urls import include, url
# from django.conf.urls.static import static
# from django.contrib import admin
# from django.views.generic.base import TemplateView
# from rest_framework_jwt.views import obtain_jwt_token

# from ang.views import AngularTemplateView

# urlpatterns = [
#     url(r'^admin/', admin.site.urls),
#     url(r'^api/auth/token/', obtain_jwt_token),
#     url(r'^api/users/', include("accounts.api.urls", namespace='users-api')),
#     url(r'^api/comments/', include("comments.api.urls", namespace='comments-api')),
#     url(r'^api/posts/', include("posts.api.urls", namespace='posts-api')),
#     url(r'^api/templates/(?P<item>[A-Za-z0-9\_\-\.\/]+)\.html$',  AngularTemplateView.as_view())
# ]

# if settings.DEBUG:
#     urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urlpatterns +=  [
    # url(r'', TemplateView.as_view(template_name='ang/home.html'))
# ]



