from django.db import models
from django.contrib import admin

class Album(models.Model):
    album_name = models.CharField(max_length=100)
    artist = models.CharField(max_length=100)
    def __str__(self):
        return '%s - %s' % (self.artist, self.album_name )

class Track(models.Model):
    album = models.ForeignKey(
        Album, 
        related_name='tracks', 
        on_delete=models.CASCADE
    )
    order = models.IntegerField()
    title = models.CharField(max_length=100)
    duration = models.IntegerField()

    class Meta:
        unique_together = ['album', 'order']
        ordering = ['order']

    def __str__(self):
        return '%d.%s' % (self.order, self.title)

admin.site.register(Album)
admin.site.register(Track)