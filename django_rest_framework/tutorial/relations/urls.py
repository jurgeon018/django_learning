from django.urls import path
from rest_framework import serializers, permissions, generics, decorators, response
from .models import *
from django.http import Http404


class TrackSerializer(serializers.ModelSerializer):
  class Meta:
    model = Track
    fields = '__all__'



class AlbumSerializer(serializers.ModelSerializer):
  # tracks = serializers.StringRelatedField(many=True)
  tracks = serializers.PrimaryKeyRelatedField(
    many=True, 
    # queryset=Album.objects.all(),
    read_only=True,
    allow_null=False,
    # pk_field=UUIDField(format='hex')
  )
  tracks = serializers.HyperlinkedRelatedField(
    view_name='track-detail',
    # queryset=Album.objects.all(),
    read_only=True, 
    many=True, 
    allow_null=False,
    lookup_field='pk',
    lookup_url_kwarg='pk',
    format=False,
  )
  tracks = serializers.SlugRelatedField(
    many=True,
    # queryset=Track.objects.all()
    read_only=True,
    slug_field='title',
  )
  # track_listing = serializers.HyperlinkedIdentityField(
  #   view_name='track-list'
  # )
  tracks = TrackSerializer(many=True, read_only=True)
  class Meta:
    model = Album
    fields = ['album_name','artist','tracks', 
    # 'track_listing'
    ]
  def create(self, validated_data):
    tracks_data = validated_data.pop('tracks')
    album = Album.objects.create(**validated_data)
    for track_data in tracks_data:
      Track.objects.create(album=album, **track_data)
    return album



class AlbumList(decorators.APIView):
  def get(self, request, *args, **kwargs):
    serializer = AlbumSerializer(Album.objects.all(), many=True, context={'request':request})
    return response.Response(serializer.data)


class TrackList(generics.ListAPIView):
  serializer_class = TrackSerializer
  queryset = Track.objects.all()
  lookup_field = 'pk'

# class TrackDetail(decorators.APIView):
  # def get(self, request, pk, *args, **kwargs):
  #   try:
  #     track = Track.objects.get(pk=pk)
  #     print(track,'8888888888')
  #   except Track.DoesNotExist:
  #     raise Http404
  #   serializer = TrackSerializer(track)
  #   return response.Response(serializer.data)

class TrackDetail(generics.RetrieveAPIView):
  serializer_class = TrackSerializer
  queryset = Track.objects.all()
  lookup_field = 'pk'


urlpatterns = [
    path('album/', AlbumList.as_view()),
    path('track/', TrackList.as_view(), name='track-list'),
    path('track/<pk>/', TrackDetail.as_view(), name='track-detail'),
]
