import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = ')jv42lk21x^eb_tew28t-4re4k_v8a3tl5w&7#qei#0oe3-4=w'
DEBUG = True
ALLOWED_HOSTS = []
INSTALLED_APPS = [
  'django.contrib.admin',
  'django.contrib.auth',
  'django.contrib.contenttypes',
  'django.contrib.sessions',
  'django.contrib.messages',
  'django.contrib.staticfiles',
  'snippets',
  'relations',
  'djoser',
  'rest_framework',
  'rest_framework.authtoken',
]
MIDDLEWARE = [
  'django.middleware.security.SecurityMiddleware',
  'django.contrib.sessions.middleware.SessionMiddleware',
  'django.middleware.common.CommonMiddleware',
  'django.middleware.csrf.CsrfViewMiddleware',
  'django.contrib.auth.middleware.AuthenticationMiddleware',
  'django.contrib.messages.middleware.MessageMiddleware',
  'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
ROOT_URLCONF = 'tutorial.urls'
TEMPLATES = [
  {
      'BACKEND': 'django.template.backends.django.DjangoTemplates',
      'DIRS': [],
      'APP_DIRS': True,
      'OPTIONS': {
          'context_processors': [
              'django.template.context_processors.debug',
              'django.template.context_processors.request',
              'django.contrib.auth.context_processors.auth',
              'django.contrib.messages.context_processors.messages',
          ],
      },
  },
]
WSGI_APPLICATION = 'tutorial.wsgi.application'
DATABASES = {
  'default': {
      'ENGINE': 'django.db.backends.sqlite3',
      'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
  }
}
AUTH_PASSWORD_VALIDATORS = [
  {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',},
  {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',},
  {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',},
  {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',},
]
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
STATIC_URL = '/static/'

REST_FRAMEWORK = {
  'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
  'PAGE_SIZE': 10
  # Use Django's standard `django.contrib.auth` permissions,
  # or allow read-only access for unauthenticated users.
  # Нужно для того, чтобы не указывать в каждой вьюхе permissions, 
  # и применить сразу для всех через настройки
  # 'DEFAULT_PERMISSION_CLASSES': [
  #     'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
  #     'rest_framework.permissions.IsAuthenticatedOrReadOnly',
  # ]
  # Нужно для того, чтобы не указывать в каждой вьюхе authentication_class, 
  # и применить сразу для всех через настройки
  # 'DEFAULT_AUTHENTICATION_CLASSES': (
  #   'rest_framework.authentication.BasicAuthentication',
  #   'rest_framework.authentication.SessionAuthentication',
  #   'rest_framework.authentication.TokenAuthentication',
  #   'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
  # )
}
