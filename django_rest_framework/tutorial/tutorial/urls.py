from django.contrib import admin
from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    # path('', include('snippets.0')),
    # path('', include('snippets.1')),
    # path('', include('snippets.2')),
    # path('', include('snippets.3_1')),
    # path('', include('snippets.3_2')),
    # path('', include('snippets.3_3')),
    # path('', include('snippets.4,5')),
    # path('', include('snippets.6_1')),
    # path('', include('snippets.6_2')),
    # path('', include('snippets.cars')),
    # path('', include('snippets.justdjango')),
    # path('', include('snippets.cfe-RESTAPI-basics')),
    # path('', include('snippets.test')),

    path('', include('relations.urls')),






    path('admin/', admin.site.urls),

    path('api-auth-jwt/', obtain_jwt_token, name='api-login'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # drf не работает на версии 3.9. Используй pip install djangorestframework==3.8
    path('auth/', include('djoser.urls')),
    path('auth_token/', include('djoser.urls.authtoken')),
]





