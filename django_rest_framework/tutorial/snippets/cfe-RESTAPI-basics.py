from django.urls import path
from django.db.models import Q
from rest_framework import generics, mixins, serializers, permissions
from .models import BlogPost2
from rest_framework_jwt.views import obtain_jwt_token

# permissions
class IsOwnerOrReadOnly(permissions.BasePermission):
  """
  Object-level permission to only allow owners of an object to edit it.
  Assumes the model instance has an `owner` attribute.
  """
  def has_object_permission(self, request, view, obj):
      # Read permissions are allowed to any request,
      # so we'll always allow GET, HEAD or OPTIONS requests.
      if request.method in permissions.SAFE_METHODS:
          return True
      # Instance must have an attribute named `owner`.
      return obj.owner == request.user


# serializers
class BlogPostSerializer(serializers.ModelSerializer): # forms.ModelForm
  url = serializers.SerializerMethodField(read_only=True)
  class Meta:
      model = BlogPost2
      fields = [
          'url',
          'id',
          'user',
          'title',
          'content',
          'timestamp',
      ]
      read_only_fields = ['id', 'user']
  def get_url(self, obj):
      request = self.context.get("request")
      return obj.get_api_url(request=request)
  def validate_title(self, value):
      qs = BlogPost2.objects.filter(title__iexact=value)
      if self.instance:
          qs = qs.exclude(pk=self.instance.pk)
      if qs.exists():
          raise serializers.ValidationError("This title has already been used")
      return value


# views
class BlogPostAPIView(mixins.CreateModelMixin, generics.ListAPIView): # DetailView CreateView FormView
  lookup_field            = 'pk' # slug, id # url(r'?P<pk>\d+')
  serializer_class        = BlogPostSerializer
  #queryset                = BlogPost2.objects.all()
  def get_queryset(self):
      qs = BlogPost2.objects.all()
      query = self.request.GET.get("q")
      if query is not None:
        qs = qs.filter(
          Q(title__icontains=query)|
          Q(content__icontains=query)
        ).distinct()
      return qs
  def perform_create(self, serializer):
    serializer.save(user=self.request.user)
  def post(self, request, *args, **kwargs):
    return self.create(request, *args, **kwargs)
  def get_serializer_context(self, *args, **kwargs):
    return {"request": self.request}


class BlogPostRudView(generics.RetrieveUpdateDestroyAPIView): # DetailView CreateView FormView
  lookup_field            = 'pk' # slug, id # url(r'?P<pk>\d+')
  serializer_class        = BlogPostSerializer
  permission_classes      = [IsOwnerOrReadOnly]
  #queryset                = BlogPost2.objects.all()
  def get_queryset(self):
    return BlogPost2.objects.all()
  def get_serializer_context(self, *args, **kwargs):
    return {"request": self.request}
  # def get_object(self):
  #     pk = self.kwargs.get("pk")
  #     return BlogPost2.objects.get(pk=pk)


# urls
app_name = 'api-postings'
urlpatterns = [
  path('', BlogPostAPIView.as_view(), name='post-listcreate'),
  path('<pk>/', BlogPostRudView.as_view(), name='post-rud')
]   
