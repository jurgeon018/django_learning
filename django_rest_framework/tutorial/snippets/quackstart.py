from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, serializers
from django.urls import path, include
from rest_framework.routers import DefaultRouter


# serializers.py
class UserSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = User
    fields = ['url', 'username', 'email', 'groups']

class GroupSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = Group
    fields = ['url', 'name']


# views.py
class UserViewSet(viewsets.ModelViewSet):
  queryset = User.objects.all().order_by('-date_joined')
  serializer_class = UserSerializer

class GroupViewSet(viewsets.ModelViewSet):
  queryset = Group.objects.all()
  serializer_class = GroupSerializer


# urls.py
router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)
urlpatterns = [
  path('', include(router.urls)),
]


# curl -H 'Accept: application/json; indent=4' -u admin:admin http://127.0.0.1:8000/users/
# http -a admin:admin http://127.0.0.1:8000/users/