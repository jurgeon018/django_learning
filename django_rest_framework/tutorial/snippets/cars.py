from django.urls import path
from django.shortcuts import render, redirect, reverse
from rest_framework import generics, serializers, permissions, authentication
from .models import *



class IsOwnerOrReadOnly(permissions.BasePermission):
  def has_object_permission(self, request, views, obj):
    if request.method in permissions.SAFE_METHODS:
      return True
    return obj.user == request.user


class CarDetailSerializer(serializers.ModelSerializer):
  user = serializers.HiddenField(default=serializers.CurrentUserDefault())
  email = serializers.Serializer(label='1234', default='some_default', required=False)
  class Meta:
    model = Car
    fields = '__all__'

class CarListSerializer(serializers.ModelSerializer):
  class Meta:
    model = Car
    fields = ['id', 'vin', 'user']


class CarCreateView(generics.CreateAPIView):
  serializer_class = CarDetailSerializer
  def post(self, request, *args, **kwargs):
    print(request.data)
    print(request.POST)
    return Response({'1':'123'})

class CarsListView(generics.ListAPIView):
  serializer_class = CarListSerializer
  queryset = Car.objects.all()
  # permission_classes = [permissions.IsAuthenticated, ]
  permission_classes = [permissions.IsAdminUser, ]

class CarDetailView(generics.RetrieveUpdateDestroyAPIView):
  queryset = Car.objects.all()
  serializer_class = CarDetailSerializer
  authentication_classes = (authentication.TokenAuthentication, 
    authentication.SessionAuthentication)
  permission_classes = [IsOwnerOrReadOnly, ]


app_name = 'car'

urlpatterns = [
  path('car/create/', CarCreateView.as_view(), name='car_create'),
  path('all/', CarsListView.as_view(), name='cars-list'),
  path('car/detail/<int:pk>/', CarDetailView.as_view(), name='car-detail'),
]