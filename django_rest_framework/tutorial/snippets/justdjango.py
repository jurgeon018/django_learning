from django.urls import path
from django.shortcuts import render
from django.http import JsonResponse
from django.db import models
from django.contrib import admin
from rest_framework import generics, serializers, permissions
from django.views.generic.base import TemplateView
from .models import *

# views.py
def blog_list(request):
  context = {
    'title': 'some title here',
    'description': 'some desctiption here',
  }
  return JsonResponse(context)


# api/serializers.py
class BlogPostSerializer(serializers.ModelSerializer):
  class Meta:
    model = BlogPost
    fields = ['pk', 'title', 'description']
    read_only_fields = ['pk']


# api/views.py
class BlogListAPIView(generics.ListCreateAPIView):
  serializer_class = BlogPostSerializer
  lookup_field = 'pk'
  queryset = BlogPost.objects.all()
  permission_classes = (permissions.IsAdminUser, )


urlpatterns = [
  path('', blog_list, name='blog_list'),
  path('main/', TemplateView.as_view(
      template_name='index.html'), name='main'),
  path('api/', BlogListAPIView.as_view(), name='blog-api'),

]

