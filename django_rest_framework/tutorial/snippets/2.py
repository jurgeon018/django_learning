from rest_framework import status, serializers
from rest_framework.decorators import api_view
from rest_framework.response import Response
from snippets.models import *
from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns


# serializers.py
# class SnippetSerializer(serializers.Serializer):
#   id = serializers.IntegerField(read_only=True)
#   title = serializers.CharField(required=False, allow_blank=True, max_length=100)
#   code = serializers.CharField(style={'base_template':'textarea.html'})
#   linenos = serializers.BooleanField(required=False)
#   language = serializers.ChoiceField(choices=LANGUAGE_CHOICES, default='python')
#   style= serializers.ChoiceField(choices=STYLE_CHOICES, default='friendly')
#   def create(self, validated_data):
#     return Snippet.objects.create(**validated_data)
#   def update(self, instance, validated_data):
#     instance.title = validated_data.get('title', instance.title)
#     instance.code = validated_data.get('code', instance.code)
#     instance.linenos = validated_data.get('linenos', instance.linenos)
#     instance.language = validated_data.get('language', instance.language)
#     instance.style = validated_data.get('style', instance.style)
#     instance.save()
#     return instance

class SnippetSerializer(serializers.ModelSerializer):
  class Meta:
    model = Snippet
    fields = ['id', 'title', 'code', 'linenos', 'language', 'style']


# views.py
@api_view(['GET','POST'])
def snippet_list(request, format=None):
  if request.method == 'GET':
    snippets = Snippet.objects.all()
    serializer = SnippetSerializer(snippets, many=True)
    return Response(serializer.data)
  elif request.method == 'POST':
    data = request.data
    serializer = SnippetSerializer(data=data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET','PUT', 'DELETE'])
def snippet_detail(request, pk, format=None):
  try:
    snippet = Snippet.objects.get(pk=pk)
  except Snippet.DoesNotExist:
    return Response(status=status.HTTP_404_NOT_FOUND)
  if request.method == 'GET':
    serializer = SnippetSerializer(snippet)
    return  Response(serializer.data)
  elif request.method == 'PUT':
    data = request.data
    serializer = SnippetSerializer(snippet, data=data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  elif request.method == 'DELETE':
    snippet.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

    
# urls.py
urlpatterns = [
    path('', snippet_list),
    path('<int:pk>/', snippet_detail),
]
urlpatterns = format_suffix_patterns(urlpatterns)


# http http://127.0.0.1:8000/snippets/
# http http://127.0.0.1:8000/snippets/ Accept:application/json  # Request JSON
# http http://127.0.0.1:8000/snippets/ Accept:text/html         # Request HTML
# http http://127.0.0.1:8000/snippets.json  # JSON suffix
# http http://127.0.0.1:8000/snippets.api   # Browsable API suffix
# http --form POST http://127.0.0.1:8000/snippets/ code="print(123)"  # POST using form data
# http --json POST http://127.0.0.1:8000/snippets/ code="print(456)"  # POST using JSON