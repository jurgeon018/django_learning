from django.db import models
from pygments.lexers import get_all_lexers, get_lexer_by_name
from pygments.styles import get_all_styles
from pygments.formatters.html import HtmlFormatter
from pygments import highlight
# from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.contrib import admin
User = get_user_model() # ЛУЧШИЙ СПОСОБ ДОСТАТЬ ТЕКУЩЕГО ЮЗЕРА. 
# То же самое, что и settings.AUTH_USER_MODEL, но с дополнительными проверками

# tutorial
LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted([(item, item) for item in get_all_styles()])


class Snippet(models.Model):
  owner = models.ForeignKey('auth.User', related_name='snippets', on_delete=models.CASCADE, blank=True, null=True)
  created = models.DateTimeField(auto_now_add=True)
  title = models.CharField(max_length=100, blank=True, default='')
  code = models.TextField()
  linenos = models.BooleanField(default=False)
  language = models.CharField(choices=LANGUAGE_CHOICES, default='python', max_length=100)
  style = models.CharField(choices=STYLE_CHOICES, default='friendly', max_length=100)
  highlighted = models.TextField(blank=True, null=True)
  class Meta:
    ordering = ['created']
  def save(self, *args, **kwargs):
    lexer = get_lexer_by_name(self.language)
    linenos = 'table' if self.linenos else False
    options = {'title': self.title} if self.title else {}
    formatter = HtmlFormatter(style=self.style,linenos=linenos,full=True, **options)
    self.highlighted = highlight(self.code, lexer, formatter)
    super(Snippet, self).save(*args, **kwargs)
admin.site.register(Snippet)


class Car(models.Model):
  CAR_TYPES = (
    (1, 'Седан'),
    (2, 'Хэчбек'),
    (3, 'Универсал'),
    (4, 'Купе'),
  )
  vin = models.CharField(verbose_name='Vin', unique=True, db_index=True, max_length=64)
  color = models.CharField(verbose_name='Color', db_index=True, max_length=64)
  brand = models.CharField(verbose_name='Brand', db_index=True, max_length=64)
  car_type = models.IntegerField(verbose_name='Car Type', choices=CAR_TYPES)
  user = models.ForeignKey(User, verbose_name='ПОльзователь', on_delete=models.CASCADE)
admin.site.register(Car)


# ???
class BlogPost(models.Model):
    title = models.CharField(max_length=120)
    description = models.TextField()
    def __str__(self):
        return self.title
admin.site.register(BlogPost)




# cfe REST-API-basics
from django.conf import settings
from django.urls import reverse
from rest_framework.reverse import reverse as api_reverse

class BlogPost2(models.Model):
    user        = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE) 
    title       = models.CharField(max_length=120, null=True, blank=True)
    content     = models.TextField(max_length=120, null=True, blank=True)
    timestamp   = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return str(self.user.username)
    @property
    def owner(self):
        return self.user
    # def get_absolute_url(self):
    #     return reverse("api-postings:post-rud", kwargs={'pk': self.pk}) '/api/postings/1/'
    def get_api_url(self, request=None):
        return api_reverse("api-postings:post-rud", kwargs={'pk': self.pk}, request=request)
admin.site.register(BlogPost2)