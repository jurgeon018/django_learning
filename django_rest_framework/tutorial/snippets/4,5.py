from django.contrib.auth.models import User
from snippets.models import *
from rest_framework import generics, permissions, renderers, serializers
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path


# permissions.py
class IsOwnerOrReadOnly(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if request.method in permissions.SAFE_METHODS:
      return True
    return obj.owner == request.user


# serializers.py
class SnippetSerializer(serializers.ModelSerializer):
  owner = serializers.ReadOnlyField(source='owner.username')
  highlight = serializers.HyperlinkedIdentityField(
    view_name='snippet-highlight', format='html'
  )
  class Meta:
    model = Snippet
    # fields = ['id', 'highlight','owner', 'title', 
    fields = ['url','id', 'highlight','owner', 'title', 
              'code', 'linenos', 'language','style'
    ]
# class UserSerializer(serializers.ModelSerializer):
class UserSerializer(serializers.HyperlinkedModelSerializer):
  # snippets = serializers.PrimaryKeyRelatedField(many=True, queryset=Snippet.objects.all())
  snippets = serializers.HyperlinkedRelatedField(many=True, view_name='snippet-detail', read_only=True)
  class Meta:
    model = User
    # fields = ['id', 'username', 'snippets']
    fields = ['url','id', 'username', 'snippets']


# views.py
@api_view(['GET'])
def api_root(request, format=None):
  return Response({
    'users':reverse('user-list', request=request, format=format),
    'snippets':reverse('snippet-list', request=request, format=format)
  })

class SnippetHighlight(generics.GenericAPIView):
  queryset = Snippet.objects.all()
  renderer_classes = [renderers.StaticHTMLRenderer]
  def get(self, request, *args, **kwargs):
    snippet = self.get_object()
    return Response(snippet.highlighted)

class SnippetList(generics.ListCreateAPIView):
  queryset = Snippet.objects.all()
  serializer_class = SnippetSerializer
  permission_classes = [permissions.IsAuthenticatedOrReadOnly]
  def perform_create(self, serializer):
    serializer.save(owner=self.request.user)

class SnippetDetail(generics.RetrieveUpdateDestroyAPIView):
  queryset = Snippet.objects.all()
  serializer_class = SnippetSerializer
  permission_classes = [
    permissions.IsAuthenticatedOrReadOnly,
    IsOwnerOrReadOnly
  ]

class UserList(generics.ListAPIView):
  queryset = User.objects.all()
  serializer_class = UserSerializer
  permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class UserDetail(generics.RetrieveAPIView):
  queryset = User.objects.all()
  serializer_class = UserSerializer
  permission_classes = [permissions.IsAuthenticatedOrReadOnly]


# urls.py
urlpatterns = [
  path('', api_root),
  path('snippets/', SnippetList.as_view(), name='snippet-list'),
  path('snippets/<int:pk>/', SnippetDetail.as_view(), name='snippet-detail'),
  path('snippets/<int:pk>/highlight/', SnippetHighlight.as_view(), name='snippet-highlight'),
  path('users/', UserList.as_view(), name='user-list'),
  path('users/<int:pk>/', UserDetail.as_view(), name='user-detail'),
]
urlpatterns = format_suffix_patterns(urlpatterns)


# http POST http://127.0.0.1:8000/snippets/ code="print(123)"
# http -a admin:admin POST http://127.0.0.1:8000/snippets/ code="print(789)"