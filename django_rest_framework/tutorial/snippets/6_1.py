from django.contrib.auth.models import User
from django.urls import include, path
from snippets.models import Snippet
from rest_framework import permissions, renderers, viewsets, serializers
from rest_framework.decorators import detail_route, action, api_view
from rest_framework.response import Response
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.reverse import reverse


# permissions.py
class IsOwnerOrReadOnly(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if request.method in permissions.SAFE_METHODS:
      return True
    return obj.owner == request.user


# serializers.py
class SnippetSerializer(serializers.HyperlinkedModelSerializer):
  owner = serializers.ReadOnlyField(source='owner.username')
  highlight = serializers.HyperlinkedIdentityField(
    view_name='snippet-highlight', format='html'
  )
  class Meta:
    model = Snippet
    fields = (
      'url', 'id', 'highlight', 'owner', 'title', 'code', 'linenos', 'language', 'style'
    )

class UserSerializer(serializers.ModelSerializer):
  snippets = serializers.PrimaryKeyRelatedField(many=True, queryset=Snippet.objects.all())
  class Meta:
    model = User
    field
    s = ['id', 'username', 'snippets']
    
class UserSerializer(serializers.HyperlinkedModelSerializer):
  snippets = serializers.HyperlinkedRelatedField(
    many=True, view_name='snippet-detail', read_only=True
  )
  class Meta:
    model = User
    fields = ('url', 'id', 'username', 'snippets')


# views.py
@api_view(['GET'])
def api_root(request, format=None):
  return Response({
    'users':reverse('user-list', request=request, format=format),
    'snippets':reverse('snippet-list', request=request, format=format)
  })

class SnippetViewSet(viewsets.ModelViewSet):
  queryset = Snippet.objects.all()
  serializer_class = SnippetSerializer
  permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly)
  # @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])
  @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
  def highlight(self, request, *args, **kwargs):
    snippet = self.get_object()
    return Response(snippet.highlighted)
  def perform_create(self, serializer):
    serializer.save(owner=self.request.user)

class UserViewSet(viewsets.ReadOnlyModelViewSet):
  queryset = User.objects.all()
  serializer_class = UserSerializer


# urls.py
snippet_list = SnippetViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
snippet_detail = SnippetViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})
snippet_highlight = SnippetViewSet.as_view({
    'get': 'highlight'
}, renderer_classes=[renderers.StaticHTMLRenderer])
user_list = UserViewSet.as_view({
    'get': 'list'
})
user_detail = UserViewSet.as_view({
    'get': 'retrieve'
})

urlpatterns = [
    path('', api_root),
    path('snippets/', snippet_list, name='snippet-list'),
    path('snippets/<int:pk>/', snippet_detail, name='snippet-detail'),
    path('snippets/<int:pk>/highlight/', snippet_highlight, name='snippet-highlight'),
    path('users/', user_list, name='user-list'),
    path('users/<int:pk>/', user_detail, name='user-detail')
]
urlpatterns = format_suffix_patterns(urlpatterns)