from snippets.models import Snippet
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, serializers
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns


# serializers.py
# class SnippetSerializer(serializers.Serializer):
#   id = serializers.IntegerField(read_only=True)
#   title = serializers.CharField(required=False, allow_blank=True, max_length=100)
#   code = serializers.CharField(style={'base_template':'textarea.html'})
#   linenos = serializers.BooleanField(required=False)
#   language = serializers.ChoiceField(choices=LANGUAGE_CHOICES, default='python')
#   style= serializers.ChoiceField(choices=STYLE_CHOICES, default='friendly')
#   def create(self, validated_data):
#     return Snippet.objects.create(**validated_data)
#   def update(self, instance, validated_data):
#     instance.title = validated_data.get('title', instance.title)
#     instance.code = validated_data.get('code', instance.code)
#     instance.linenos = validated_data.get('linenos', instance.linenos)
#     instance.language = validated_data.get('language', instance.language)
#     instance.style = validated_data.get('style', instance.style)
#     instance.save()
#    return instance

class SnippetSerializer(serializers.ModelSerializer):
  class Meta:
    model = Snippet
    fields = ['id', 'title', 'code', 'linenos', 'language', 'style']


# views.py
class SnippetList(APIView):
  def get(self, request, format=None):
    snippets = Snippet.objects.all()
    serializer = SnippetSerializer(snippets, many=True)
    return Response(serializer.data)
  def post(self, request, format=None):
    serializer = SnippetSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.errors, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class SnippetDetail(APIView):
  def get_object(self, pk):
    try:
      return Snippet.objects.get(pk=pk)
    except Snippet.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = SnippetDetail.get_object(self, pk)
    serializer = SnippetSerializer(snippet)
    return Response(serializer.data)
  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = SnippetSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  def delete(self, request, pk, format=None):
    snippet = self.get_object(pk)
    snippet.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)


# urls.py
urlpatterns = [
    path('snippets/', SnippetList.as_view()),
    path('snippets/<int:pk>/', SnippetDetail.as_view()),
]
urlpatterns = format_suffix_patterns(urlpatterns)