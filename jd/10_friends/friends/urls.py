from django.urls import path, include
from django.contrib import admin
from accounts.views import *


urlpatterns = [
    path('admin/', admin.site.urls),

    path('users/', users_list, name='list'),
    path('users/<slug>/', profile_view),
    path('users/friend-request/send/<id>/', send_friend_request),
    path('users/friend-request/cancel/<id>/', cancel_friend_request),
    path('users/friend-request/accept/<id>/', accept_friend_request),
    path('users/friend-request/delete/<id>/', delete_friend_request),

    path('accounts/', include('allauth.urls')),
]
