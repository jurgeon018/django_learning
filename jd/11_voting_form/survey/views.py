from django import forms
from django.shortcuts import render
from django.db import models, transaction
from django.contrib import admin
from django.db.models import F

# models.py
class Vote(models.Model):
  book_name = models.CharField(max_length=200)
  count = models.IntegerField(default=0)
  def __str__(self):
      return '%s: %d votes' % (self.book_name, self.count)
  @classmethod
  def bulk_vote(cls, book_names):
    with transaction.atomic():
      for book_name in book_names:
        if len(book_name) == 0:
          continue
        vote = Vote.objects.filter(book_name=book_name)
        if vote.exists():
          vote.update(count=F('count') + 1)
        else:
          Vote.objects.create(book_name=book_name, count=1)
admin.site.register(Vote)

# forms.py
class VotingForm(forms.Form):
  chosen_books_options = forms.MultipleChoiceField(
    choices=[], 
    label="Book Name", 
    required=False, 
    widget=forms.CheckboxSelectMultiple(attrs={
      'class': 'container'
    })
  )
  other_book_name = forms.CharField(
    label='Other', 
    max_length=100, 
    required=False,
    widget=forms.TextInput(attrs={
      'class': 'form-control',
      'placeholder': 'Did we miss something?'
    })
  )
  def __init__(self, *args, **kwargs):
      super().__init__(*args, **kwargs)
      unique_books_names = Vote.objects \
                            .order_by('book_name') \
                            .values_list('book_name', flat=True) \
                            .distinct()
      self.fields['chosen_books_options'].choices = [
        (book_name, book_name) for book_name in unique_books_names
      ]

def index(request):
  if request.method == 'POST':
    form = VotingForm(request.POST)
    if form.is_valid():
      chosen_books_options = form.cleaned_data.get(
       'chosen_books_options', []
      )
      other_book_name = form.cleaned_data.get('other_book_name', '')
      Vote.bulk_vote(chosen_books_options + [other_book_name])
    message = 'Thank You For Your Contribution!'
  elif request.method == 'GET':
    message = ''
  form = VotingForm()
  for field in form:
    print(dir(field.form))
  return render(request, 'survey.html', {'form': form, 'message': message})
