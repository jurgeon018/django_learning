from django.contrib import admin
from django.urls import path
from django.shortcuts import render
from django.db import models
from django.db.models import Q
from .models import *


class Author(models.Model):
  name = models.CharField(max_length=30)
  def __str__(self):
    return self.name

class Category(models.Model):
  name = models.CharField(max_length=20)
  def __str__(self):
    return self.name

class Journal(models.Model):
  title = models.CharField(max_length=120)
  author = models.ForeignKey(Author2, on_delete=models.CASCADE)
  categories = models.ManyToManyField(Category)
  # publish_date = models.DateTimeField(auto_now_add=True)
  publish_date = models.DateTimeField()
  views = models.IntegerField(default=0)
  reviewed = models.BooleanField(default=False)
  def __str__(self):
    return self.title
    
admin.site.register(Author)
admin.site.register(Category)
admin.site.register(Journal)

def is_valid_queryparam(param):
  return param != '' and param is not None

def BootstrapFilterView(request):
  qs = Journal.objects.all()
  categories = Category.objects.all()
  title_contains_query = request.GET.get('title_contains')
  id_exact_query = request.GET.get('id_exact')
  title_or_author_query = request.GET.get('title_or_author')
  view_count_min = request.GET.get('view_count_min')
  view_count_max = request.GET.get('view_count_max')
  date_max = request.GET.get('date_max')
  date_min = request.GET.get('date_min')  
  category = request.GET.get('category')
  reviewed = request.GET.get('reviewed')
  not_reviewed = request.GET.get('not_reviewed')
  


  if is_valid_queryparam(title_contains_query):
    qs = qs.filter(title__icontains=title_contains_query)
  elif is_valid_queryparam(id_exact_query):
    # id__iexact == id. Если лукап не указан, то он всегда по умолчанию __iexact.
    # Также __iexact - case insensetive, __exact - case sensetive
    qs = qs.filter(id__iexact=id_exact_query)
  elif is_valid_queryparam(title_or_author_query):
    # Иногда запрос может вернуть результат обоих выражений, поэтому вернет 1 пост 2 раза. 
    # Для этого указывается .distinct(), который возвращает 1 результат при двойном совпадении
    qs = qs.filter(Q(title__icontains=title_or_author_query)
                   |Q(author__name__icontains=title_or_author_query)
                  ).distinct()

  # __gt - >, __gte - >=, __lt - <, __lte - <=
  if is_valid_queryparam(view_count_min):
    qs = qs.filter(views__gte=view_count_min)
  
  if is_valid_queryparam(view_count_max):
    qs = qs.filter(views__lt=view_count_max)

  if is_valid_queryparam(date_min):
    qs = qs.filter(publish_date__gte=date_min)

  if is_valid_queryparam(date_max):
    qs = qs.filter(publish_date__lt=date_max)
  
  if is_valid_queryparam(category) and category!='Choose...':
    qs = qs.filter(categories__name=category)

    
  if reviewed == 'on':
    qs = qs.filter(reviewed=True)    
  elif not_reviewed == 'on':
    qs = qs.filter(reviewed=False)    

  return render(request, 'djfilter/bootstrap_form.html', {'queryset':qs, 'categories':categories}) 

urlpatterns = [
  path('', BootstrapFilterView, name='bootstrap'),
]
