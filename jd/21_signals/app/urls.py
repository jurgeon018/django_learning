from django.dispatch import receiver, Signal
from django.db import models
from django.urls import path
from django.http import HttpResponse
from django.shortcuts import render
from django.core.signals import request_finished, request_started
from django.db.models.signals import pre_save, post_save, post_delete, pre_delete

class Post(models.Model):
  title = models.CharField(max_length=200)
  def __str__(self):
    return self.title






# pre_save, post_save, pre_delete, post_delete

def save_post1(sender, instance, *args, **kwargs):
  print('instance:',instance)
  print('save_post1')

def delete_post1(sender, instance, *args, **kwargs):
  print('instance:',instance)
  print('delete_post1')

# @receiver(pre_save)
@receiver(post_save)
def save_post2(sender, instance, *args, **kwargs):
  print('instance:',instance)
  print('save_post2')

# @receiver(pre_delete)
@receiver(post_delete)
def delete_post2(sender, instance, *args, **kwargs):
  print('instance:',instance)
  print('delete_post2')

post_save.connect(receiver=save_post1, sender=Post)
pre_save.connect(receiver=save_post1, sender=Post)
post_delete.connect(receiver=delete_post1, sender=Post)
pre_delete.connect(receiver=delete_post1, sender=Post)

# python manage.py shell
# >>> from app.urls import Post
# >>> Post.objects.create(title='title1')
# something
# >>> p = Post(); p.title='title2'; 
# >>> p.save()
# something
# >>> p.delete()
# you deleted something






# request_started, request_finished

@receiver(request_finished)
def post_request_finished_receiver1(sender, **kwargs):
  print('***************************')
  print(sender)
  print('post_request_finished_receiver1')
  print('***************************')

@receiver(request_started)
def post_request_started_receiver1(sender, **kwargs):
  print('***************************')
  print(sender)
  print('post_request_started_receiver1')
  print('***************************')

def post_request_finished_receiver2(sender, **kwargs):
  print('***************************')
  print(sender)
  print('post_request_finished_receiver2')
  print('***************************')

def post_request_started_receiver2(sender, **kwargs):
  print('***************************')
  print(sender)
  print('post_request_started_receiver2')
  print('***************************')

request_finished.connect(receiver=post_request_finished_receiver2, sender=None)
request_started.connect(receiver=post_request_started_receiver2, sender=None)

# post_request_started_receiver1
# post_request_started_receiver2
# [10/Jul/2019 13:18:19] "GET / HTTP/1.1" 200 20
# post_request_finished_receiver1
# post_request_finished_receiver2





# custom signals

request_counter_signal = Signal(providing_args=['timestamp',])

@receiver(request_counter_signal)
def post_counter_signal_receiver(sender, **kwargs):
  print('post_counter_signal_receiver')
  print(kwargs)

def home(request):
  request_counter_signal.send(sender=Post, timestamp='2019-05-11')
  return HttpResponse('Here is the response')

# post_counter_signal_receiver
# {'signal': <django.dispatch.dispatcher.Signal object at 0x7fb1a1cff780>, 'timestamp': '2019-05-11'}





# urls.py
urlpatterns = [
    path('', home, name='home'),
]
