from django import forms
from django.http import Http404, JsonResponse
from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.db import models, transaction
from django.contrib import admin
from django.conf import settings
from import_export.admin import ImportExportModelAdmin
######################################################################
# <Analytics App>
# We want to be able to see where users are going or what are users viewing, and display count of how many times certain object has been viewed, to keep track of user's actions.
######################################################################
# models.py
class View(models.Model):
  # user        = subject, who actually saw blogpost
    user        = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
  # post        = object, some visual object that users can see
    post        = models.ForeignKey('BlogPost', on_delete=models.CASCADE)
  # views_count = actual counter of views
    views_count = models.IntegerField(default=0)
    def __str__(self):
        return f"Post:{self.post}  -  Views:{self.views_count}  -  User:{self.user}"

# admin.py
@admin.register(View)
class ViewAdmin(ImportExportModelAdmin):
    pass

# views.py
def analytics_view(request):
    context = {}
    views = View.objects.all()
    for view in views:
        print({
            'user':f'{view.user.pk}_{view.user.username}',
            'post':f'{view.post.pk}_{view.post.title}',
            'views_count':view.views_count
        })
        print(context)
        context.update(
            # {'View':
                {
                    'user':f'{view.user.pk}_{view.user.username}',
                    'post':f'{view.post.pk}_{view.post.title}',
                    'views_count':view.views_count
                }
            # }
        )
    return JsonResponse(context)
######################################################################
# <Blogs App>
######################################################################
# models.py
class BlogPost(models.Model):
    title = models.CharField(max_length=120)
    description = models.TextField()
    def __str__(self):
        return self.title  
admin.site.register(BlogPost)

# views.py
class BlogListView(ListView):
    model = BlogPost
    template_name = 'analytics/blogpost_list.html'

class BlogDetailView(DetailView):
    model = BlogPost
    template_name = 'analytics/blogpost_detail.html'
    # Every time user goes to BlogDetailView('/blogs/<pk>/'),
    # get_object() runs, and increases views_count by 1.
    def get_object(self):
        # Grab the primary key of the object
        post_pk = self.kwargs.get("pk")
        # Returns a list with the result, filtered by primary key.
        post_query = BlogPost.objects.filter(pk=post_pk)
        # If list with result exists,
        if post_query.exists():
            # grab the first item in the list.
            post_object = post_query.first()
            # Specify the fields for the View object to be created.
            view, created = View.objects.get_or_create(
                user=self.request.user,
                post=post_object
            )
            # If view exists,
            if view:
                # add 1 view onto the view tally,
                view.views_count += 1
                # and save view.
                view.save()
            # Else if view doesn't exists return
            return post_object
        # if the object does not exist
        raise Http404  




######################################################################
# <Survey App> (11, how to make a voting form)
######################################################################
# models.py


class Vote(models.Model):
    book_name = models.CharField(max_length=200)
    count = models.IntegerField(default=0)

    def __str__(self):
        return '%s: %d votes' % (self.book_name, self.count)

    @classmethod
    def bulk_vote(cls, book_names):
        with transaction.atomic():
            for book_name in book_names:
                if len(book_name) == 0:
                    continue
                if Vote.objects.filter(book_name=book_name).exists():
                    Vote.objects.filter(book_name=book_name).update(
                        count=models.F('count') + 1)
                else:
                    Vote.objects.create(book_name=book_name, count=1)


admin.site.register(Vote)

# forms.py
class VotingForm(forms.Form):
    chosen_books_options = forms.MultipleChoiceField(choices=[], label='Book Name', required=False, widget=forms.SelectMultiple(
        attrs={
            'class': 'form-control'
        }
    ))
    other_book_name = forms.CharField(
        label='Other', max_length=100, required=False,
            widget=forms.TextInput(
                attrs={
                'class': 'form-control',
                    'placeholder': 'Did we miss something?'
                }
            ))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        unique_books_names = Vote.objects.order_by(
            'book_name').values_list('book_name', flat=True).distinct()
        self.fields['chosen_books_options'].choices = [
            (book_name, book_name) for book_name in unique_books_names]


def index(request):
    if request.method == 'POST':
        form = VotingForm(request.POST)
        if form.is_valid():
            chosen_books_options = form.cleaned_data.get(
                'chosen_books_options', [])
            other_book_name = form.cleaned_data.get('other_book_name', '')
            Vote.bulk_vote(chosen_books_options + [other_book_name])
        message = 'Thank You For Your Contribution!'
    elif request.method == 'GET':
        message = ''
    form = VotingForm()
    return render(request, 'survey.html', {'form': form, 'message': message})
