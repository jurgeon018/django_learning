from django.contrib import admin
from django.urls import path, include
from analytics.views import *


urlpatterns = [
    path('admin/', admin.site.urls),
    path('analytics/', analytics_view, name='analytics'),
    path('blogs/', BlogListView.as_view(), name='list'),
    path('blogs/<pk>/', BlogDetailView.as_view(), name='detail'),
    path('survey/', index)

]
