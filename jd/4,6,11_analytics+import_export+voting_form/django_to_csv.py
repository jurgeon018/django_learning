# console
pip install django-import-export

# settings.py
INSTALLED_APPS = [
  ....
  'import_export',
  ....
]


# admin.py
from django.contrib import admin
from .models import SomeModel
from import_export.admin import ImportExportModelAdmin

@admin.register(SomeModel)
class ViewAdmin(ImportExportModelAdmin):
  pass

Идешь в админку и ищешь кнопки IMport export