from app.models import *
from django.contrib import admin

# models.py
class Course(models.Model):
    slug = models.SlugField()
    title = models.CharField(max_length=120)
    description = models.TextField()
    allowed_memberships = models.ManyToManyField(Membership)
    
class Lesson(models.Model):
    slug = models.SlugField()
    title = models.CharField(max_length=120)
    course = models.ForeignKey(Course, on_delete=models.SET_NULL, null=True)
    position = models.IntegerField()
    video_url = models.CharField(max_length=200)
    thumbnail = models.ImageField()

# 
class InLineLesson(admin.TabularInline):
    model = Lesson
    extra = 1
    max_num = 3

class CourseAdmin(admin.ModelAdmin):
    inlines = [InLineLesson]
    list_display = ('title', 'slug', 'description', 'combine_title_and_slug')
    list_display_links = ('title', 'combine_title_and_slug')
    list_editable = ('slug', )
    list_filter = ('title', 'slug')
    search_fields = ('title', 'slug')
    fieldsets = (
        (None, {
            'fields': (
                'slug',
                'title',
                'description',
                'allowed_memberships'
            )
        }),
    )

    def combine_title_and_slug(self, obj):
        return "{} - {}".format(obj.title, obj.slug)
        
admin.site.register(Course, CourseAdmin)
