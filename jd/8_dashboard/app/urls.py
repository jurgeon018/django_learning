from django.urls import path

from app.finance import company_article_list, ChartData, dash, dash_ajax

from app.news import scrape

from app.notepad import list_view, create_view, delete_view, update_view

from app.home import home

urlpatterns = [
    # .
    path('', home, name='home'),
    # notepad
    path('notes/', list_view, name='list'),
    path('notes/create/', create_view, name='create'),
    path('notes/<id>/delete/', delete_view, name='delete'),
    path('notes/<id>/update/', update_view, name='update'),
    # news
    path('scrape/', scrape, name='scrape'),
    # finance
    path('companies/', company_article_list, name='companies'),
    path('api/chart/data/', ChartData.as_view(), name='api-chart-data'),
    path('dash/', dash),
    path('_dash', dash_ajax),
]