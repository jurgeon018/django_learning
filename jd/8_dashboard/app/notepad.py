from django.urls import path
from django.shortcuts import render, redirect, get_object_or_404
from django import forms
from django.contrib import admin
from django.conf import settings
from django.db import models
from django.urls import reverse

# models.py
class Note(models.Model):
  user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
  title = models.CharField(max_length=120)
  image = models.ImageField(null=True, blank=True)
  url = models.URLField(null=True, blank=True)
  timestamp = models.DateTimeField(auto_now_add=True)
  def __str__(self):
  		return self.title
  def get_delete_url(self):
  		return reverse("notes:delete", kwargs={
			"id": self.id
		})
  def get_update_url(self):
  		return reverse("notes:update", kwargs={
			"id": self.id
		})
admin.site.register(Note)

# forms.py
class NoteModelForm(forms.ModelForm):
	class Meta:
		model = Note
		fields = ['title','url','image']

# views.py
def create_view(request):
	form = NoteModelForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		form.instance.user = request.user
		form.save()
		return redirect('/home/')
	return render(request, "notepad/create.html", {'form': form})
def list_view(request):
	notes = Note.objects.all()
	return render(request, "notepad/list.html", {'object_list': notes})

def delete_view(request, id):
	item_to_delete = Note.objects.filter(pk=id) # return a list
	if item_to_delete.exists():
		if request.user == item_to_delete[0].user:
			item_to_delete[0].delete()
	return redirect('/notes/list')

def update_view(request, id):
	unique_note = get_object_or_404(Note, id=id)
	form = NoteModelForm(request.POST or None, request.FILES or None, instance=unique_note)
	if form.is_valid():
		form.instance.user = request.user
		form.save()
		return redirect('/')
	return render(request, "notepad/create.html", {'form': form})