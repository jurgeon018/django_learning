from django.shortcuts import render, redirect
import math
import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup
from datetime import timedelta, timezone, datetime
import os
import shutil
from datetime import datetime
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.signals import post_save
from django.contrib import admin

User = get_user_model()

class Headline(models.Model):
  title = models.CharField(max_length=120)
  image = models.ImageField()
  url = models.TextField()
  def __str__(self):
  		return self.title

class UserProfile(models.Model):
  user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
  last_scrape = models.DateTimeField(auto_now_add=True)
  def __str__(self):
  		return "{}-{}".format(self.user, self.last_scrape)

def post_user_signup_receiver(sender, instance, **kwargs):
  	userprofile, created = UserProfile.objects.get_or_create(user=instance)
post_save.connect(post_user_signup_receiver, sender=User)
admin.site.register(Headline)
admin.site.register(UserProfile)

def scrape(request):
	user_p = UserProfile.objects.filter(user=request.user).first()
	user_p.last_scrape = datetime.now(timezone.utc)
	user_p.save()
	session = requests.Session()
	session.headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.109 Safari/537.36"}
	url = 'https://www.theonion.com/'
	content = session.get(url, verify=False).content
	soup = BeautifulSoup(content, "html.parser")
	posts = soup.find_all('div',{'class':'curation-module__item'}) # returns a list
	for i in posts:
		link = i.find_all('a',{'class':'js_curation-click'})[1]['href']
		title = i.find_all('a',{'class':'js_curation-click'})[1].text
		image_source = i.find('img',{'class':'featured-image'})['data-src']
	# <stackoverflow solution>
		media_root = '/Users/matthew/Downloads/dashboard/media_root'
		if not image_source.startswith(("data:image", "javascript")):
			local_filename = image_source.split('/')[-1].split("?")[0]
			r = session.get(image_source, stream=True, verify=False)
			with open(local_filename, 'wb') as f:
				for chunk in r.iter_content(chunk_size=1024):
					f.write(chunk)
			current_image_absolute_path = os.path.abspath(local_filename)
			shutil.move(current_image_absolute_path, media_root)
	# </end of stackoverflow>
		new_headline = Headline()
		new_headline.title = title
		new_headline.url = link
		new_headline.image = local_filename
		new_headline.save()
	return redirect('/home/')

