from django.urls import path

# from .sync_consumers import ChatConsumer
# from .async_consumers import ChatConsumer
from .consumers import ChatConsumer

websocket_urlpatterns = [
  path('ws/chat/<room_name>/', ChatConsumer)
]