from django.urls import path
import json

from .views import index, room, ChatConsumer

websocket_urlpatterns = [
  path('ws/chat/<room_name>/', ChatConsumer)
]

urlpatterns = [
    path('', index, name='index'),
    path('<room_name>/', room, name='room'),
]
