from django.urls import path
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.contrib import admin
from django.db import models
from django.conf import settings
from django import forms
######################################################################
######################################################################
# USECASES OF MODEL MANAGERS AND MODEL QUERYSETS.
# Sometimes you add so much methods on the model itself, that it ends up being completely cluttered. 
# Instead we could just handle all of those methods on the manager
# Менеджеры и методы моделей взаимозаменяемые, менеджеры надо использовать тогда, когда у модели становится слишком много методов, которые мешают удобочитаемости кода

# managers.py
class PostQuerySet(models.QuerySet):
    def get_author_posts(self, username):
        return self.filter(author__user__username=username)
    def get_editor_posts(self, username):
        return self.filter(editor__user__username=username)

class SizeQuerySet(models.QuerySet):
    def smaller_than(self, size):
        return self.filter(comments__lt=size)
    def greater_than(self, size):
        return self.filter(comments__gt=size)

class SizeManager(models.Manager):
    def smaller_than(self, size):
        return self.filter(comments__lt=size)
    def greater_than(self, size):
        return self.filter(comments__gt=size)

class AuthorManager(models.Manager):
    def get_queryset(self):
        return PostQuerySet(self.model, using=self._db)
    def get_users_posts(self, username):
        return self.get_queryset().get_author_posts(username)
    def all(self):
        return self.get_queryset()

class EditorManager(models.Manager):
    def get_queryset(self):
        return PostQuerySet(self.model, using=self._db)
    def get_users_posts(self, username):
        return self.get_queryset().get_editor_posts(username)    
######################################################################
######################################################################
# models.py
User = settings.AUTH_USER_MODEL

class Post(models.Model):
    title = models.CharField(max_length=120)
    description = models.TextField()
    author = models.ForeignKey('Author', on_delete=models.CASCADE)
    editor = models.ForeignKey(
        'Editor', on_delete=models.CASCADE, blank=True, null=True)
    image = models.ImageField()
    slug = models.SlugField(unique=True)
    comments = models.IntegerField(default=0)

    size_query_set = SizeQuerySet.as_manager()
    size_manager = SizeManager()
    authors = AuthorManager()
    editors = EditorManager()

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        return "/posts/{}/".format(self.slug)
    def get_update_url(self):
        return "/posts/{}/update/".format(self.slug)
    def get_delete_url(self):
        return "/posts/{}/delete/".format(self.slug)

class Author(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    email = models.EmailField()
    cellphone_num = models.IntegerField()
    def __str__(self):
        return self.user.username

class Editor(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    def __str__(self):
        return self.user.username
admin.site.register(Post)
admin.site.register(Editor)
admin.site.register(Author)
######################################################################
######################################################################
# forms.py
# class PostForm(forms.Form):
# 	title = forms.CharField()
# 	description = forms.CharField()
# 	image = forms.ImageField()
# 	slug = forms.CharField()
class PostModelForm(forms.ModelForm):
  class Meta:
    model = Post
    fields = ['title', 'description', 'image', 'slug']
######################################################################
######################################################################
# views.py
def posts_list(request):
    all_posts = Post.authors.all()
    admins_posts = Post.authors.get_users_posts('admin')
    editor_dudes_posts = Post.editors.get_users_posts('editor_dude')
    context = {
        'all_posts': all_posts,
        'admins_posts': admins_posts,
        'editor_dudes_posts': editor_dudes_posts
    }
    messages.info(request, 'Here are all the current blog posts')
    return render(request, "posts_list.html", context)

# CRUD
def posts_detail(request, slug):
    unique_post = get_object_or_404(Post, slug=slug)
    context = {
        'post': unique_post
    }
    messages.info(request, 'This is the specific detail view')
    return render(request, "posts_detail.html", context)

def posts_create(request):
    author, created = Author.objects.get_or_create(
        user=request.user,
        email=request.user.email,
        cellphone_num=894382982)
    form = PostModelForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.instance.author = author
        form.save()
        messages.info(request, 'Successfully created a new blog post!')
        return redirect('/posts/')
    context = {
        'form': form
    }
    return render(request, "posts_create.html", context)

def posts_update(request, slug):
    unique_post = get_object_or_404(Post, slug=slug)
    form = PostModelForm(request.POST or None,
                         request.FILES or None,
                         instance=unique_post)
    if form.is_valid():
        form.save()
        messages.info(request, 'Successfully updated your blog post.')
        return redirect('/posts/')
    context = {
        'form': form
    }
    return render(request, "posts_create.html", context)

def posts_delete(request, slug):
    unique_post = get_object_or_404(Post, slug=slug)
    unique_post.delete()
    messages.info(request, 'Successfully deleted blog post.')
    return redirect('/posts/')
######################################################################
######################################################################
# urls.py
urlpatterns = [
    path('', posts_list),
    path('create/', posts_create),
    path('<slug>/update/', posts_update),
    path('<slug>/delete/', posts_delete),
    path('<slug>/', posts_detail)
]
