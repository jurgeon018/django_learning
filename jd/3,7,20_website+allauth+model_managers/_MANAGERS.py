# USECASES OF MODEL MANAGERS AND MODEL QUERYSETS.
# Sometimes you add so much methods on the model itself, that it ends up being completely cluttered. 
# Instead we could just handle all of those methods on the manager
# Менеджеры и методы моделей взаимозаменяемые, менеджеры надо использовать тогда, когда у модели становится слишком много методов, которые мешают удобочитаемости кода
# managers.py
class PostQuerySet(models.QuerySet):
    def get_author_posts(self, username):
        return self.filter(author__user__username=username)
    def get_editor_posts(self, username):
        return self.filter(editor__user__username=username)

class SizeQuerySet(models.QuerySet):
    def smaller_than(self, size):
        return self.filter(comments__lt=size)
    def greater_than(self, size):
        return self.filter(comments__gt=size)

class SizeManager(models.Manager):
    def smaller_than(self, size):
        return self.filter(comments__lt=size)
    def greater_than(self, size):
        return self.filter(comments__gt=size)

class AuthorManager(models.Manager):
    def get_queryset(self):
        return PostQuerySet(self.model, using=self._db)
    def get_users_posts(self, username):
        return self.get_queryset().get_author_posts(username)
    def all(self):
        return self.get_queryset()

class EditorManager(models.Manager):
    def get_queryset(self):
        return PostQuerySet(self.model, using=self._db)
    def get_users_posts(self, username):
        return self.get_queryset().get_editor_posts(username)    
