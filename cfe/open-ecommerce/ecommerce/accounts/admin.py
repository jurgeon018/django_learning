from django.contrib import admin
from .models import UserStripe, EmailConfirmed, EmailMarketingSignUp, UserAddress, UserDefaultAddress


class UserAddressAdmin(admin.ModelAdmin):
	class Meta:
		model = UserAddress



class EmailMarketingSignUpAdmin(admin.ModelAdmin):
	list_display = ['__unicode__', 'timestamp']
	class Meta:
		model = EmailMarketingSignUp


admin.site.register(UserAddress, UserAddressAdmin)
admin.site.register(UserDefaultAddress)
admin.site.register(UserStripe)
admin.site.register(EmailConfirmed)
admin.site.register(EmailMarketingSignUp, EmailMarketingSignUpAdmin)