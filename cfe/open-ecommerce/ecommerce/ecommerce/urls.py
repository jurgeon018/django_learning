from django.conf import settings
# from django.conf.urls import patterns, include, url
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from products.views import *
from carts.views import *
from orders.views import orders, checkout
from marketing.views import dismiss_marketing_message, email_signup
from accounts.views import add_user_address, logout_view, login_view, registration_view, add_user_address, activation_view
admin.autodiscover()

urlpatterns = [
    # Examples:
    url(r'^$', home, name='home'),
    url(r'^s/$', search, name='search'),
    url(r'^products/$', all, name='products'),
    url(r'^products/(?P<slug>[\w-]+)/$', single, name='single_product'),
    url(r'^cart/(?P<id>\d+)/$', remove_from_cart, name='remove_from_cart'),
    url(r'^cart/(?P<slug>[\w-]+)/$', add_to_cart, name='add_to_cart'),
    url(r'^cart/$', view, name='cart'),
    url(r'^checkout/$', checkout, name='checkout'),
    url(r'^orders/$', orders, name='user_orders'),
    url(r'^ajax/dismiss_marketing_message/$', dismiss_marketing_message, name='dismiss_marketing_message'),
    url(r'^ajax/email_signup/$', email_signup, name='ajax_email_signup'),
    url(r'^ajax/add_user_address/$', add_user_address, name='ajax_add_user_address'),

    # url(r'^blog/', include('blog.urls')),
    #(?P<all_items>.*)
    #(?P<id>\d+)
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/logout/$', logout_view, name='auth_logout'),
    url(r'^accounts/login/$', login_view, name='auth_login'),
    url(r'^accounts/register/$', registration_view, name='auth_register'),
    url(r'^accounts/address/add/$', add_user_address, name='add_user_address'),
    url(r'^accounts/activate/(?P<activation_key>\w+)/$', activation_view, name='activation_view'),
]


if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)