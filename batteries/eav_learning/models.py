from copy import copy
from django.contrib.contenttypes import fields as generic
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.base import ModelBase
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


from .utils import (
    IllegalAssignmentException,
    EavDatatypeField, EavSlugField,
    register,
    validate_text,
    validate_float,
    validate_int,
    validate_date,
    validate_bool,
    validate_object,
    validate_enum
)


class EnumValue(models.Model):
    value = models.CharField(_('Value'), db_index=True, unique=True, max_length=50)
    def __str__(self):
        return '<EnumValue {}>'.format(self.value)


class EnumGroup(models.Model):
    name = models.CharField(_('Name'), unique = True, max_length = 100)
    values = models.ManyToManyField(EnumValue, verbose_name = _('Enum group'))
    def __str__(self):
        return '<EnumGroup {}>'.format(self.name)


class Attribute(models.Model):
    class Meta:
        ordering = ['name']
    TYPE_TEXT    = 'text'
    TYPE_FLOAT   = 'float'
    TYPE_INT     = 'int'
    TYPE_DATE    = 'date'
    TYPE_BOOLEAN = 'bool'
    TYPE_OBJECT  = 'object'
    TYPE_ENUM    = 'enum'
    DATATYPE_CHOICES = (
        (TYPE_TEXT,    _('Text')),
        (TYPE_DATE,    _('Date')),
        (TYPE_FLOAT,   _('Float')),
        (TYPE_INT,     _('Integer')),
        (TYPE_BOOLEAN, _('True / False')),
        (TYPE_OBJECT,  _('Django Object')),
        (TYPE_ENUM,    _('Multiple Choice')),
    )
    datatype = EavDatatypeField(
        verbose_name = _('Data Type'),
        choices      = DATATYPE_CHOICES,
        max_length   = 6
    )
    name = models.CharField(
        verbose_name = _('Name'),
        max_length   = 100,
        help_text    = _('User-friendly attribute name')
    )
    slug = EavSlugField(
        verbose_name = _('Slug'),
        max_length   = 50,
        db_index     = True,
        unique       = True,
        help_text    = _('Short unique attribute label')
    )
    required = models.BooleanField(verbose_name = _('Required'), default = False)
    enum_group = models.ForeignKey(
        EnumGroup,
        verbose_name = _('Choice Group'),
        on_delete    = models.PROTECT,
        blank        = True,
        null         = True
    )
    description = models.CharField(
        verbose_name = _('Description'),
        max_length   = 256,
        blank        = True,
        null         = True,
        help_text    = _('Short description')
    )
    display_order = models.PositiveIntegerField(
        verbose_name = _('Display order'),
        default = 1
    )
    modified = models.DateTimeField(
        verbose_name = _('Modified'),
        auto_now     = True
    )
    created = models.DateTimeField(
        verbose_name = _('Created'),
        default      = timezone.now,
        editable     = False
    )
    @property
    def help_text(self):
        return self.description

    def get_validators(self):
        DATATYPE_VALIDATORS = {
            'text':   validate_text,
            'float':  validate_float,
            'int':    validate_int,
            'date':   validate_date,
            'bool':   validate_bool,
            'object': validate_object,
            'enum':   validate_enum,
        }
        return [DATATYPE_VALIDATORS[self.datatype]]

    def validate_value(self, value):
        for validator in self.get_validators():
            validator(value)

        if self.datatype == self.TYPE_ENUM:
            if isinstance(value, EnumValue):
                value = value.value
            if not self.enum_group.values.filter(value=value).exists():
                raise ValidationError(
                    _('%(val)s is not a valid choice for %(attr)s')
                    % dict(val = value, attr = self)
                )

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = EavSlugField.create_slug_from_name(self.name)

        self.full_clean()
        super(Attribute, self).save(*args, **kwargs)

    def clean(self):
        if self.datatype == self.TYPE_ENUM and not self.enum_group:
            raise ValidationError(
                _('You must set the choice group for multiple choice attributes')
            )
        if self.datatype != self.TYPE_ENUM and self.enum_group:
            raise ValidationError(
                _('You can only assign a choice group to multiple choice attributes')
            )

    def get_choices(self):
        return self.enum_group.values.all() if self.datatype == Attribute.TYPE_ENUM else None

    def save_value(self, entity, value):
        ct = ContentType.objects.get_for_model(entity)

        try:
            value_obj = self.value_set.get(
                entity_ct = ct,
                entity_id = entity.pk,
                attribute = self
            )
        except Value.DoesNotExist:
            if value == None or value == '':
                return

            value_obj = Value.objects.create(
                entity_ct = ct,
                entity_id = entity.pk,
                attribute = self
            )

        if value == None or value == '':
            value_obj.delete()
            return

        if value != value_obj.value:
            value_obj.value = value
            value_obj.save()

    def __str__(self):
        return '{} ({})'.format(self.name, self.get_datatype_display())


class Value(models.Model):
    entity_ct = models.ForeignKey(
        ContentType,
        on_delete    = models.PROTECT,
        related_name = 'value_entities'
    )
    entity_id = models.IntegerField()
    entity = generic.GenericForeignKey(ct_field = 'entity_ct', fk_field = 'entity_id')
    value_text  = models.TextField(blank = True, null = True)
    value_float = models.FloatField(blank = True, null = True)
    value_int   = models.IntegerField(blank = True, null = True)
    value_date  = models.DateTimeField(blank = True, null = True)
    value_bool  = models.NullBooleanField(blank = True, null = True)
    value_enum  = models.ForeignKey(
        EnumValue,
        blank        = True,
        null         = True,
        on_delete    = models.PROTECT,
        related_name = 'eav_values'
    )
    generic_value_id = models.IntegerField(blank=True, null=True)
    generic_value_ct = models.ForeignKey(
        ContentType,
        blank        = True,
        null         = True,
        on_delete    = models.PROTECT,
        related_name ='value_values'
    )
    value_object = generic.GenericForeignKey(
        ct_field = 'generic_value_ct',
        fk_field = 'generic_value_id'
    )
    created = models.DateTimeField(_('Created'), default = timezone.now)
    modified = models.DateTimeField(_('Modified'), auto_now = True)
    attribute = models.ForeignKey(
        Attribute,
        db_index     = True,
        on_delete    = models.PROTECT,
        verbose_name = _('Attribute')
    )

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Value, self).save(*args, **kwargs)

    def _get_value(self):
        return getattr(self, 'value_%s' % self.attribute.datatype)

    def _set_value(self, new_value):
        setattr(self, 'value_%s' % self.attribute.datatype, new_value)

    value = property(_get_value, _set_value)

    def __str__(self):
        return '{}: "{}" ({})'.format(self.attribute.name, self.value, self.entity)

    def __repr__(self):
        return '{}: "{}" ({})'.format(self.attribute.name, self.value, self.entity.pk)


class Entity(object):

    @staticmethod
    def pre_save_handler(sender, *args, **kwargs):
        instance = kwargs['instance']
        entity = getattr(kwargs['instance'], instance._eav_config_cls.eav_attr)
        entity.validate_attributes()

    @staticmethod
    def post_save_handler(sender, *args, **kwargs):
        instance = kwargs['instance']
        entity = getattr(instance, instance._eav_config_cls.eav_attr)
        entity.save()

    def __init__(self, instance):
        self.instance = instance
        self.ct = ContentType.objects.get_for_model(instance)

    def __getattr__(self, name):
        if not name.startswith('_'):
            try:
                attribute = self.get_attribute_by_slug(name)
            except Attribute.DoesNotExist:
                raise AttributeError(
                    _('%(obj)s has no EAV attribute named %(attr)s')
                    % dict(obj = self.instance, attr = name)
                )

            try:
                return self.get_value_by_attribute(attribute).value
            except Value.DoesNotExist:
                return None

        return getattr(super(Entity, self), name)

    def get_all_attributes(self):
        return self.instance._eav_config_cls.get_attributes().order_by('display_order')

    def _hasattr(self, attribute_slug):
        return attribute_slug in self.__dict__

    def _getattr(self, attribute_slug):
        return self.__dict__[attribute_slug]

    def save(self):
        for attribute in self.get_all_attributes():
            if self._hasattr(attribute.slug):
                attribute_value = self._getattr(attribute.slug)
                if attribute.datatype == Attribute.TYPE_ENUM and not isinstance(attribute_value, EnumValue):
                    attribute_value = EnumValue.objects.get(value=attribute_value)
                attribute.save_value(self.instance, attribute_value)

    def validate_attributes(self):
        values_dict = self.get_values_dict()
        for attribute in self.get_all_attributes():
            value = None
            if self._hasattr(attribute.slug):
                value = self._getattr(attribute.slug)
                values_dict.pop(attribute.slug, None)
            else:
                value = values_dict.pop(attribute.slug, None)
            if value is None:
                if attribute.required:
                    raise ValidationError(
                        _('{} EAV field cannot be blank'.format(attribute.slug))
                    )
            else:
                try:
                    attribute.validate_value(value)
                except ValidationError as e:
                    raise ValidationError(
                        _('%(attr)s EAV field %(err)s')
                        % dict(attr = attribute.slug, err = e)
                    )
        illegal = values_dict or (
            self.get_object_attributes() - self.get_all_attribute_slugs())
        if illegal:
            raise IllegalAssignmentException(
                'Instance of the class {} cannot have values for attributes: {}.'
                .format(self.instance.__class__, ', '.join(illegal))
            )

    def get_values_dict(self):
        return {v.attribute.slug: v.value for v in self.get_values()}

    def get_values(self):
        return Value.objects.filter(
            entity_ct = self.ct,
            entity_id = self.instance.pk
        ).select_related()

    def get_all_attribute_slugs(self):
        return set(self.get_all_attributes().values_list('slug', flat=True))

    def get_attribute_by_slug(self, slug):
        return self.get_all_attributes().get(slug=slug)

    def get_value_by_attribute(self, attribute):
        return self.get_values().get(attribute=attribute)

    def get_object_attributes(self):
        return set(copy(self.__dict__).keys()) - set(['instance', 'ct'])

    def __iter__(self):
        return iter(self.get_values())


class EAVModelMeta(ModelBase):
    def __new__(cls, name, bases, namespace, **kwds):
        result = super(EAVModelMeta, cls).__new__(cls, name, bases, dict(namespace))
        register(result)
        return result