from copy import deepcopy
from django.contrib.admin.widgets import AdminSplitDateTime
from django.forms import (
    BooleanField, CharField, ChoiceField, DateTimeField,
    FloatField, IntegerField, ModelForm,
)
from django.utils.translation import ugettext_lazy as _


class BaseDynamicEntityForm(ModelForm):
    FIELD_CLASSES = {
        'text': CharField,
        'float': FloatField,
        'int': IntegerField,
        'date': DateTimeField,
        'bool': BooleanField,
        'enum': ChoiceField,
    }

    def __init__(self, data=None, *args, **kwargs):
        super(BaseDynamicEntityForm, self).__init__(data, *args, **kwargs)
        config_cls = self.instance._eav_config_cls
        self.entity = getattr(self.instance, config_cls.eav_attr)
        self._build_dynamic_fields()

    def _build_dynamic_fields(self):
        self.fields = deepcopy(self.base_fields)
        for attribute in self.entity.get_all_attributes():
            value = getattr(self.entity, attribute.slug)
            defaults = {
                'label': attribute.name.capitalize(),
                'required': attribute.required,
                'help_text': attribute.help_text,
                'validators': attribute.get_validators(),
            }
            datatype = attribute.datatype
            if datatype == attribute.TYPE_ENUM:
                values = attribute.get_choices().values_list('id', 'value')
                choices = [('', '-----')] + list(values)
                defaults.update({'choices': choices})
                if value:
                    defaults.update({'initial': value.pk})
            elif datatype == attribute.TYPE_DATE:
                defaults.update({'widget': AdminSplitDateTime})
            elif datatype == attribute.TYPE_OBJECT:
                continue
            MappedField = self.FIELD_CLASSES[datatype]
            self.fields[attribute.slug] = MappedField(**defaults)
            if value and not datatype == attribute.TYPE_ENUM:
                self.initial[attribute.slug] = value

    def save(self, commit=True):
        if self.errors:
            raise ValueError(_(
                'The %s could not be saved because the data'
                'didn\'t validate.' % self.instance._meta.object_name
            ))
        instance = super(BaseDynamicEntityForm, self).save(commit=False)
        for attribute in self.entity.get_all_attributes():
            value = self.cleaned_data.get(attribute.slug)
            if attribute.datatype == attribute.TYPE_ENUM:
                if value:
                    value = attribute.enum_group.values.get(pk=value)
                else:
                    value = None
            setattr(self.entity, attribute.slug, value)
        if commit:
            instance.save()
            self._save_m2m()
        return instance
