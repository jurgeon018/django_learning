from itertools import count
from functools import wraps
from django.core.exceptions import FieldError, ObjectDoesNotExist
from django.db import models
from django.db.models import Case, IntegerField, Q, When
from django.db.models.query import QuerySet
from django.db.utils import NotSupportedError

from .models import Attribute, Value, EnumValue


def is_eav_and_leaf(expr, gr_name):
    return (
        getattr(expr, 'connector', None) == 'AND' and
        len(expr.children) == 1 and
        expr.children[0][0] in ['pk__in', '{}__in'.format(gr_name)]
    )


def rewrite_q_expr(model_cls, expr):
    if isinstance(expr, Q):
        config_cls = getattr(model_cls, '_eav_config_cls', None)
        gr_name = config_cls.generic_relation_attr
        expr.children = [rewrite_q_expr(model_cls, c) for c in expr.children]
        rewritable = [c for c in expr.children if is_eav_and_leaf(c, gr_name)]
        if len(rewritable) > 1:
            q = None
            other = [c for c in expr.children if not c in rewritable]
            for child in rewritable:
                if not (child.children and len(child.children) == 1):
                    raise AssertionError('Child must have exactly one descendant')
                attrval = child.children[0]
                if not isinstance(attrval, tuple):
                    raise AssertionError('Attribute-value must be a tuple')
                fname = '{}__in'.format(gr_name)
                if attrval[0] == fname or hasattr(attrval[1], '__contains__'):
                    _q = model_cls.objects.filter(**{fname: attrval[1]})
                else:
                    _q = attrval[1]
                q = q if q != None else _q
                if expr.connector == 'AND':
                    q &= _q
                else:
                    q |= _q
            if q != None:
                expr.children = other + [('pk__in', q)]
    return expr


def eav_filter(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        nargs = []
        nkwargs = {}
        for arg in args:
            if isinstance(arg, Q):
                arg = expand_q_filters(arg, self.model)
                arg = rewrite_q_expr(self.model, arg)
            nargs.append(arg)
        for key, value in kwargs.items():
            nkey, nval = expand_eav_filter(self.model, key, value)
            if nkey in nkwargs:
                nkwargs[nkey] = (nkwargs[nkey] & nval).distinct()
            else:
                nkwargs.update({nkey: nval})
        return func(self, *nargs, **nkwargs)
    return wrapper


def expand_q_filters(q, root_cls):
    new_children = []
    for qi in q.children:
        if isinstance(qi, tuple):
            key, value = expand_eav_filter(root_cls, *qi)
            new_children.append(Q(**{key: value}))
        else:
            new_children.append(expand_q_filters(qi, root_cls))
    q.children = new_children
    return q


def expand_eav_filter(model_cls, key, value):
    fields = key.split('__')
    config_cls = getattr(model_cls, '_eav_config_cls', None)

    if len(fields) > 1 and config_cls and fields[0] == config_cls.eav_attr:
        slug = fields[1]
        gr_name = config_cls.generic_relation_attr
        datatype = Attribute.objects.get(slug=slug).datatype

        if datatype == Attribute.TYPE_ENUM and not isinstance(value, EnumValue):
            lookup = '__value__{}'.format(fields[2]) if len(fields) > 2 else '__value'
        else:
            lookup = '__{}'.format(fields[2]) if len(fields) > 2 else ''
        kwargs = {
            'value_{}{}'.format(datatype, lookup): value,
            'attribute__slug': slug
        }
        value = Value.objects.filter(**kwargs)

        return '%s__in' % gr_name, value

    try:
        field = model_cls._meta.get_field(fields[0])
    except models.FieldDoesNotExist:
        return key, value

    if not field.auto_created or field.concrete:
        return key, value
    else:
        sub_key = '__'.join(fields[1:])
        key, value = expand_eav_filter(field.model, sub_key, value)
        return '{}__{}'.format(fields[0], key), value


class EavQuerySet(QuerySet):

    @eav_filter
    def filter(self, *args, **kwargs):
        return super(EavQuerySet, self).filter(*args, **kwargs)

    @eav_filter
    def exclude(self, *args, **kwargs):
        return super(EavQuerySet, self).exclude(*args, **kwargs)

    @eav_filter
    def get(self, *args, **kwargs):
        return super(EavQuerySet, self).get(*args, **kwargs)

    def order_by(self, *fields):
        order_clauses = []
        query_clause = self
        config_cls = self.model._eav_config_cls
        for term in [t.split('__') for t in fields]:
            if len(term) == 2 and term[0] == config_cls.eav_attr:
                try:
                    attr = Attribute.objects.get(slug=term[1])
                except ObjectDoesNotExist:
                    raise ObjectDoesNotExist(
                        'Cannot find EAV attribute "{}"'.format(term[1])
                    )
                field_name = 'value_%s' % attr.datatype
                pks_values = Value.objects.filter(
                    attribute__slug=attr.slug,
                    entity_id__in=self
                ).order_by(
                    field_name
                ).values_list(
                    'entity_id', field_name
                )
                _, ordered_values = zip(*pks_values)
                val2ind = dict(zip(ordered_values, count()))
                entities_pk = [(pk, val2ind[val]) for pk, val in pks_values]
                when_clauses = [
                    When(id=pk, then=i)
                    for pk, i in entities_pk
                ]
                order_clause = Case(
                    *when_clauses,
                    output_field=IntegerField()
                )
                clause_name = '__'.join(term)
                query_clause = query_clause.annotate(
                    **{clause_name: order_clause}
                )
                order_clauses.append(clause_name)
            elif len(term) >= 2 and term[0] == config_cls.eav_attr:
                raise NotSupportedError(
                    'EAV does not support ordering through '
                    'foreign-key chains'
                )
            else:
                order_clauses.append(term[0])
        return QuerySet.order_by(query_clause, *order_clauses)
