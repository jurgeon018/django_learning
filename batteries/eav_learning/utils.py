# exceptions.py
class IllegalAssignmentException(Exception):
	pass


# decorators.py
from . import register
from django.db.models import Model


def register_eav(**kwargs):
    def _model_eav_wrapper(model_class):
        if not issubclass(model_class, Model):
            raise ValueError('Wrapped class must subclass Model.')
        register(model_class, **kwargs)
        return model_class
    return _model_eav_wrapper


# managers.py
from django.db import models
from .queryset import EavQuerySet


class EntityManager(models.Manager):
    _queryset_class = EavQuerySet

    def create(self, **kwargs):
        config_cls = getattr(self.model, '_eav_config_cls', None)
        if not config_cls or config_cls.manager_only:
            return super(EntityManager, self).create(**kwargs)
        prefix = '%s__' % config_cls.eav_attr
        new_kwargs = {}
        eav_kwargs = {}
        for key, value in kwargs.items():
            if key.startswith(prefix):
                eav_kwargs.update({key[len(prefix):]: value})
            else:
                new_kwargs.update({key: value})
        obj = self.model(**new_kwargs)
        obj_eav = getattr(obj, config_cls.eav_attr)
        for key, value in eav_kwargs.items():
            setattr(obj_eav, key, value)
        obj.save()
        return obj

    def get_or_create(self, **kwargs):
        try:
            return self.get(**kwargs), False
        except self.model.DoesNotExist:
            return self.create(**kwargs), True


# fields.py
import re
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _


class EavSlugField(models.SlugField):

    def validate(self, value, instance):
        super(EavSlugField, self).validate(value, instance)
        slug_regex = r'[a-z][a-z0-9_]*'
        if not re.match(slug_regex, value):
            raise ValidationError(_(
                'Must be all lower case, start with a letter, and contain '
                'only letters, numbers, or underscores.'
            ))

    @staticmethod
    def create_slug_from_name(name):
        name = name.strip().lower()
        name = '_'.join(name.split())
        return re.sub('[^\w]', '', name)


class EavDatatypeField(models.CharField):
    
    def validate(self, value, instance):
        super(EavDatatypeField, self).validate(value, instance)
        if not instance.pk:
            return
        if type(instance).objects.get(pk=instance.pk).datatype == instance.datatype:
            return
        if instance.value_set.count():
            raise ValidationError(_(
                'You cannot change the datatype of an attribute that is already in use.'
            ))


# validators.py
import datetime
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _


def validate_text(value):
    if not isinstance(value, str):
        raise ValidationError(_(u"Must be str or unicode"))


def validate_float(value):
    try:
        float(value)
    except ValueError:
        raise ValidationError(_(u"Must be a float"))


def validate_int(value):
    try:
        int(value)
    except ValueError:
        raise ValidationError(_(u"Must be an integer"))


def validate_date(value):
    if not isinstance(value, datetime.datetime) and not isinstance(value, datetime.date):
        raise ValidationError(_(u"Must be a date or datetime"))


def validate_bool(value):
    if not isinstance(value, bool):
        raise ValidationError(_(u"Must be a boolean"))


def validate_object(value):
    if not isinstance(value, models.Model):
        raise ValidationError(_(u"Must be a django model object instance"))
    if not value.pk:
        raise ValidationError(_(u"Model has not been saved yet"))


def validate_enum(value):
    from .models import EnumValue
    if isinstance(value, EnumValue) and not value.pk:
        raise ValidationError(_(u"EnumValue has not been saved yet"))


#  __init__.py
def register(model_cls, config_cls=None):
    from .registry import Registry
    Registry.register(model_cls, config_cls)


def unregister(model_cls):
    from .registry import Registry
    Registry.unregister(model_cls)
