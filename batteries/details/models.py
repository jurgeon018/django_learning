from django.db import models
from eav.decorators import register_eav
import eav 
from eav.registry import EavConfig
from eavkit.registry import EavConfig, registry
from eavkit.models import BaseAttributeOptions






class Page(models.Model):
    title = models.CharField(max_length=100)
    value = models.TextField()
    def __str__(self):
        return self.title


class Value(models.Model):
    # page = models.ForeignKey(to='Page', on_delete=models.CASCADE)
    attribute = models.CharField(max_length=100)
    value = models.TextField()
    def __str__(self):
        return self.page, self.attribute, self.value