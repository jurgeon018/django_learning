from django.urls import path, include
import eav
from .models import *
from django.shortcuts import render 
from eav.models import Attribute
from eav.registry import EavConfig



def index(request):
    Attribute.objects.get_or_create(name='slider_content_1', datatype=Attribute.TYPE_TEXT)
    page, _ = Page.objects.get_or_create(title='title1')
    page.name='1'
    page.eav.slider_content_1 = 'Hello'
    page.save()
    return render(request, 'index.html', locals())



urlpatterns = [
    path('', index, name='index'),
]