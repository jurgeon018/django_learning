from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Category(models.Model):
    name = models.CharField(max_length=50, unique=True)
    def __str__(self):
        return f"{self.name}"


class Genre(MPTTModel):
    name = models.CharField(max_length=50, unique=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    def __str__(self):
        try: return f"{self.name}, {self.parent.name}"
        except: return f"{self.name}"
    class MPTTMeta:
        order_insertion_by = ['name']
        # level_attr   = 'mptt_level' # 'level'
        # parent_attr  = 'parent'
        # left_attr    = 'lft'
        # right_attr   = 'rght'
        # tree_id_attr = 'tree_id'
