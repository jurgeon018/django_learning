from django.contrib import admin
from django.db import models
from .models import *
import mptt
from mptt.fields import TreeForeignKey
from mptt.admin import (
    MPTTModelAdmin, DraggableMPTTAdmin,
    TreeRelatedFieldListFilter,
)
from django.contrib.auth.models import Group
from django.utils.html import format_html


class CustomMPTTModelAdmin(MPTTModelAdmin):
    mptt_level_indent = 20
    # mptt_indent_field = "name"


class MyDraggableMPTTAdmin(DraggableMPTTAdmin):
    list_display = ('tree_actions', 'something')
    list_display_links = ('something',)
    mptt_level_indent = 5

    def something(self, instance):
        return format_html(
            '<div style="text-indent:{}px">{}</div>',
            instance._mpttfield('level') * self.mptt_level_indent,
            instance.name,  # Or whatever you want to put here
        )
    something.short_description = ('something nice')

class MyTreeRelatedFieldListFilter(TreeRelatedFieldListFilter):
    mptt_level_indent = 20


class MyModelAdmin(admin.ModelAdmin):
    model = Genre
    list_filter = (
        ('name', TreeRelatedFieldListFilter),
        # ('name', MyTreeRelatedFieldListFilter),
    )


# admin.site.register(Genre, MPTTModelAdmin)
# admin.site.register(Genre, CustomMPTTModelAdmin)
# admin.site.register(Genre, DraggableMPTTAdmin,
#     list_display=('tree_actions', 'indented_title'),
#     list_display_links=('indented_title')
# )
admin.site.register(Genre, MyDraggableMPTTAdmin)
# admin.site.register(Genre, MyModelAdmin)

tree_group = TreeForeignKey(Group, on_delete=models.CASCADE, blank=True, null=True)
tree_group.contribute_to_class(Group, 'parent')
mptt.register(Group, order_insertion_by=['name'], level_attr='level')