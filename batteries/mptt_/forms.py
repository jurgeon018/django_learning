# https://django-mptt.readthedocs.io/en/latest/forms.html

from django import forms
from mptt.forms import (
    TreeNodeChoiceField, TreeNodeMultipleChoiceField, 
    TreeNodePositionField, TreeNodeChoiceFieldMixin
)

class GenreForm(forms.Form):
    category = models.ModelChoiceField(queryset=Category.objects.all())
    category = TreeNodeChoiceField(
        queryset=Category.objects.all(),
        level_indicator=u'*--'
    )
    category = TreeNodeMultipleChoiceField(queryset=Category.objects.all(),)

    category  = forms.ChoiceField()    
    category  = TreeNodePositionField()