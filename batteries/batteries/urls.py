from django.contrib import admin
from django.urls import path, include 


urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', include('mptt_.urls')),
    path('', include('details.urls')),
]
