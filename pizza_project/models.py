# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)
    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class CeleryTaskmeta(models.Model):
    task_id = models.CharField(unique=True, max_length=255)
    status = models.CharField(max_length=50)
    result = models.TextField(blank=True, null=True)
    date_done = models.DateTimeField()
    traceback = models.TextField(blank=True, null=True)
    hidden = models.BooleanField()
    meta = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'celery_taskmeta'


class CeleryTasksetmeta(models.Model):
    taskset_id = models.CharField(unique=True, max_length=255)
    result = models.TextField()
    date_done = models.DateTimeField()
    hidden = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'celery_tasksetmeta'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey('PizzaAuthAppCustomuser', models.DO_NOTHING)
    action_flag = models.PositiveSmallIntegerField()

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjceleryCrontabschedule(models.Model):
    minute = models.CharField(max_length=64)
    hour = models.CharField(max_length=64)
    day_of_week = models.CharField(max_length=64)
    day_of_month = models.CharField(max_length=64)
    month_of_year = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'djcelery_crontabschedule'


class DjceleryIntervalschedule(models.Model):
    every = models.IntegerField()
    period = models.CharField(max_length=24)

    class Meta:
        managed = False
        db_table = 'djcelery_intervalschedule'


class DjceleryPeriodictask(models.Model):
    name = models.CharField(unique=True, max_length=200)
    task = models.CharField(max_length=200)
    args = models.TextField()
    kwargs = models.TextField()
    queue = models.CharField(max_length=200, blank=True, null=True)
    exchange = models.CharField(max_length=200, blank=True, null=True)
    routing_key = models.CharField(max_length=200, blank=True, null=True)
    expires = models.DateTimeField(blank=True, null=True)
    enabled = models.BooleanField()
    last_run_at = models.DateTimeField(blank=True, null=True)
    total_run_count = models.PositiveIntegerField()
    date_changed = models.DateTimeField()
    description = models.TextField()
    crontab = models.ForeignKey(DjceleryCrontabschedule, models.DO_NOTHING, blank=True, null=True)
    interval = models.ForeignKey(DjceleryIntervalschedule, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'djcelery_periodictask'


class DjceleryPeriodictasks(models.Model):
    ident = models.SmallIntegerField(primary_key=True)
    last_update = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'djcelery_periodictasks'


class DjceleryTaskstate(models.Model):
    state = models.CharField(max_length=64)
    task_id = models.CharField(unique=True, max_length=36)
    name = models.CharField(max_length=200, blank=True, null=True)
    tstamp = models.DateTimeField()
    args = models.TextField(blank=True, null=True)
    kwargs = models.TextField(blank=True, null=True)
    eta = models.DateTimeField(blank=True, null=True)
    expires = models.DateTimeField(blank=True, null=True)
    result = models.TextField(blank=True, null=True)
    traceback = models.TextField(blank=True, null=True)
    runtime = models.FloatField(blank=True, null=True)
    retries = models.IntegerField()
    hidden = models.BooleanField()
    worker = models.ForeignKey('DjceleryWorkerstate', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'djcelery_taskstate'


class DjceleryWorkerstate(models.Model):
    hostname = models.CharField(unique=True, max_length=255)
    last_heartbeat = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'djcelery_workerstate'


class PizzaAppAddress(models.Model):
    full = models.CharField(max_length=150)

    class Meta:
        managed = False
        db_table = 'pizza_app_address'


class PizzaAppPizzaingredient(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'pizza_app_pizzaingredient'


class PizzaAppPizzamenuitem(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'pizza_app_pizzamenuitem'


class PizzaAppPizzamenuitemIngredients(models.Model):
    pizzamenuitem = models.ForeignKey(PizzaAppPizzamenuitem, models.DO_NOTHING)
    pizzaingredient = models.ForeignKey(PizzaAppPizzaingredient, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'pizza_app_pizzamenuitem_ingredients'
        unique_together = (('pizzamenuitem', 'pizzaingredient'),)


class PizzaAppPizzaorder(models.Model):
    comment = models.CharField(max_length=140)
    delivered = models.BooleanField()
    date_created = models.DateTimeField()
    date_delivered = models.DateTimeField(blank=True, null=True)
    delivery = models.ForeignKey(PizzaAppAddress, models.DO_NOTHING)
    kind = models.ForeignKey(PizzaAppPizzamenuitem, models.DO_NOTHING)
    size = models.ForeignKey('PizzaAppPizzasize', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'pizza_app_pizzaorder'


class PizzaAppPizzaorderExclude(models.Model):
    pizzaorder = models.ForeignKey(PizzaAppPizzaorder, models.DO_NOTHING)
    pizzaingredient = models.ForeignKey(PizzaAppPizzaingredient, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'pizza_app_pizzaorder_exclude'
        unique_together = (('pizzaorder', 'pizzaingredient'),)


class PizzaAppPizzaorderExtra(models.Model):
    pizzaorder = models.ForeignKey(PizzaAppPizzaorder, models.DO_NOTHING)
    pizzaingredient = models.ForeignKey(PizzaAppPizzaingredient, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'pizza_app_pizzaorder_extra'
        unique_together = (('pizzaorder', 'pizzaingredient'),)


class PizzaAppPizzasize(models.Model):
    size = models.CharField(max_length=2)

    class Meta:
        managed = False
        db_table = 'pizza_app_pizzasize'


class PizzaAuthAppCustomuser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()
    our_note = models.CharField(max_length=140)
    favourite_pizza = models.ForeignKey(PizzaAppPizzamenuitem, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pizza_auth_app_customuser'


class PizzaAuthAppCustomuserGroups(models.Model):
    customuser = models.ForeignKey(PizzaAuthAppCustomuser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'pizza_auth_app_customuser_groups'
        unique_together = (('customuser', 'group'),)


class PizzaAuthAppCustomuserUserPermissions(models.Model):
    customuser = models.ForeignKey(PizzaAuthAppCustomuser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'pizza_auth_app_customuser_user_permissions'
        unique_together = (('customuser', 'permission'),)
