from django.urls import path, include
from django.contrib import admin
from django.conf import settings
from django.shortcuts import render
from django.template import RequestContext
from django.views.generic import View
from django.contrib import messages
from django import forms
from django.core.exceptions import ValidationError


class MyForm(forms.Form):
  name = forms.CharField()
  message = forms.CharField()
  def clean_name(self):
    """auto magic of form validation
    """
    name = self.cleaned_data['name']
    if len(name) <= 3:
        raise ValidationError('Name is too short')
    return name
  def clean(self):
    print('Cleaning self')
    if self.cleaned_data.get('name', None) == 'Ivan':
        raise ValidationError('NOOOO!')
    return self.cleaned_data


def index(request):
  if request.method == 'GET':
    return render(request, 'my_app/index.html', {'name': 'Ivan'})


class MyView(View):
  def get(self, request):
    form = MyForm()
    return render(request, 'my_app/form.html', locals())
  def post(self, request):
    form = MyForm(data=request.POST)
    if form.is_valid():
      messages.success(request, form.cleaned_data['message'])
    else:
      messages.error(request, 'Validation failed')
    return render(request, 'my_app/form.html', locals())

urlpatterns = [
  path('admin/', admin.site.urls),
  path('', index, name='index'),
  path('form/', MyView.as_view(), name='form'),
]


if settings.DEBUG:
  from django.conf.urls.static import static
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
