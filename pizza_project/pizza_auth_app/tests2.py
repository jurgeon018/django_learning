def test_upper_true():
  assert 'foo'.upper() == 'FOO'
  # assert 'foo'.upper() != 'FOO'
  assert 'foo'.upper() in 'FOO'
  # assert 'foo'.upper() not in 'FOO'
