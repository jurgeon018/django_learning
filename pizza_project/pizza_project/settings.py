import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = 'wpvv820_@gfr*2z^()o7g%=uheq7h$*lj3&&(#xts4f#0c!vk-'

DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',


    'debug_toolbar',
    'rest_framework',
    'djcelery',
    'raven.contrib.django.raven_compat',


    'pizza_app',
    'pizza_auth_app',
    'rest_api_app',
]

INTERNAL_IPS = [
    '127.0.0.1',
]


MIDDLEWARE = [

    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'qinspect.middleware.QueryInspectMiddleware',

    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'pizza_project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'pizza_project.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Authorization
# https://docs.djangoproject.com/es/2.0/topics/auth/

AUTH_USER_MODEL = 'pizza_auth_app.CustomUser'
LOGIN_URL = '/auth/login'
LOGIN_REDIRECT_URL = '/pizza/create'

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]

#LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'translations', 'locale'),
)


STATIC_URL = '/static/'

# Celery
from datetime import timedelta
import djcelery
djcelery.setup_loader()

# Redis settings:

REDIS_BACKEND = {
    'HOST': 'localhost',
    'PORT': 6379,
    'DB': 0,
}

REDIS_BACKEND_URL = 'redis://{host}:{port}/{db}'.format(
    host=REDIS_BACKEND['HOST'],
    port=REDIS_BACKEND['PORT'],
    db=REDIS_BACKEND['DB'],
)


# CELERY SETTINGS

# If you want to use Redis for storing results (probably not):
# CELERY_RESULT_BACKEND = 'redis://{host}:{port}/{db}'.format(
#     host=REDIS_BACKEND['HOST'],
#     port=REDIS_BACKEND['PORT'],
#     db=REDIS_BACKEND['DB'],
# )

CELERY_RESULT_BACKEND = 'djcelery.backends.database.DatabaseBackend'

CELERY_TASK_RESULT_EXPIRES = 18000  # 5 hours

CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

BROKER_URL = REDIS_BACKEND_URL

# Periodic tasks:
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
CELERYBEAT_SCHEDULE = {
    'greet-every-5-seconds': {
        'task': 'pizza_app.tasks.greet_new_orders',
        'schedule': timedelta(seconds=5),
        # 'args': (16, 16),
    },
}


# https://github.com/dobarkod/django-queryinspect
QUERY_INSPECT_ENABLED = True


# LOGGING = {
#     'version': 1,
#     'filters': {
#         'require_debug_true': {
#             '()': 'django.utils.log.RequireDebugTrue',
#         }
#     },
#     'handlers': {
#         'console': {
#             'level': 'DEBUG',
#             'filters': ['require_debug_true'],
#             'class': 'logging.StreamHandler',
#         }
#     },
#     'loggers': {
#         # 'django.db.backends': {
#         #     'level': 'DEBUG',
#         #     'handlers': ['console'],
#         # },
#         'qinspect': {
#             'handlers': ['console'],
#             'level': 'DEBUG',
#             'propagate': True,
#         },
#     }
# }



# import raven
#
# RAVEN_CONFIG = {
#     'dsn': '',
#     # If you are using git, you can also automatically configure the
#     # release based on the git info.
#     'release': raven.fetch_git_sha(os.path.abspath(os.pardir)),
# }