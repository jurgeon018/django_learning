from django.contrib.auth.models import (
    AbstractBaseUser, 
    AbstractUser, 
    PermissionsMixin
)
from django.db import models

class CustomUser(AbstractUser):
    phone = models.CharField(max_length=120, blank=True, null=True)
