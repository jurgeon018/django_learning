
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}

$(function() {
  var object_constructor = {
    color_napkins: {
      name: gettext('Колір серветок'),
      items: {
        0: {
          class: 'white',
          name: gettext('Білий'),
          value: '#fff',
          colorText: 'black__text'
        },
        1: {
          class: 'yellow',
          name: gettext('Жовтий'),
          value: '#FEDE00',
          colorText: 'black__text'
        },
        2: {
          class: 'turquoise',
          name: gettext('Бірюза'),
          value: '#6BD7E1'
        },
        3: {
          class: 'red',
          name: gettext('Червоний'),
          value: '#EE1C25'
        },
        4: {
          class: 'blue',
          name: gettext('Синій'),
          value: '#263B7A'
        },
        5: {
          class: 'green',
          name: gettext('Зелений'),
          value: '#008000'
        },
        6: {
          class: 'burgundy',
          name: gettext('Бордовий'),
          value: '#641C34'
        },
        7: {
          class: 'black',
          name: gettext('Чорний'),
          value: '#000'
        }
      }
    },
    form_factor: {
      name: gettext('Форм фактор'),
      items: {
        0: {
          name: '33x33',
          value: '33x33'
        },
        1: {
          name: '24x24',
          value: '24x24'
        }
      }
    },
    embossing: {
      name: gettext('Тиснення'),
      items: {
        0: {
          name: gettext('Квадрат'),
          value: '/static/img/tisnena/tik1.png',
          //value: 'Квадрат',
          icon: '/static/img/svg/polosu.svg'
        },
        1: {
          name: gettext('Гірлянда'),
          value: '/static/img/tisnena/tis.png',
          //value: 'Гірлянда',
          icon: '/static/img/svg/polosu.svg'
        }
      }
    },
    kind: {
      name: gettext('Розворот серветки'),
      items: {
        0: {
          name: gettext('Прямокутник'),
          value: 'rectangle',
          icon: '/static/img/svg/rectangle.svg'
        },
        1: {
          name: gettext('Трикутник'),
          value: 'trickster',
          icon: '/static/img/svg/trickster.svg'
        }
      }
    }
  };
  var form_constructor = document.getElementById('constructor__form');

  for (var key in object_constructor) {
    var fieldset = document.createElement('fieldset');
    fieldset.classList.add('form__row');
    var form__group = document.createElement('div');
    form__group.classList.add('form__group');
    var form__group_label = document.createElement('label');
    form__group_label.classList.add('form__input_name');
    form__group_label.setAttribute("for", key);
    form__group_label.innerHTML = object_constructor[key].name;
    form__group.appendChild(form__group_label);
    var form__group_input = document.createElement('input');
    form__group_input.classList.add('input_value');
    form__group_input.setAttribute("type", "hidden");
    form__group_input.setAttribute("id", key);
    form__group_input.setAttribute("name", key);
    form__group_input.setAttribute("value", object_constructor[key].items[0].value);
    form__group.appendChild(form__group_input);
    var form__group_input_control = document.createElement('div');
    form__group_input_control.classList.add('form__input_control');
    var form__group_input_control_icon = document.createElement('div');
    form__group_input_control_icon.classList.add('partner__next_item');
    form__group_input_control.appendChild(form__group_input_control_icon);
    form__group.appendChild(form__group_input_control);
    var form__items = document.createElement('div');
    form__items.classList.add('form__items', 'form__items-mod', 'form__items-wid50');

    for (var key_id in object_constructor[key].items) {
      /* create item */

      var form__item = document.createElement('div');
      form__item.classList.add('form__items_item', 'item');
      if (key_id === '0') {
        form__item.classList.add('form__items_item-active');
      }
      form__item.dataset.value = object_constructor[key].items[key_id].value;

      if (typeof object_constructor[key].items[key_id].colorText !== 'undefined') {
        form__item.classList.add(object_constructor[key].items[key_id].colorText);
      }
      /* create icon in item */

      var form__item_icon = document.createElement('div');

      if (typeof object_constructor[key].items[key_id].class !== 'undefined') {
        form__item_icon.classList.add('item_color');
        form__item_icon.classList.add(object_constructor[key].items[key_id].class);
      } else {
        form__item_icon.classList.add('item_icon');
      }

      if (typeof object_constructor[key].items[key_id].icon !== 'undefined') {
        /* create img in icon */
        var form__item_icon_img = document.createElement('img');
        form__item_icon_img.src = object_constructor[key].items[key_id].icon;
        form__item_icon.appendChild(form__item_icon_img);
      }

      form__item.appendChild(form__item_icon);
      /* create name in item */

      var form__item_name = document.createElement('div');
      form__item_name.classList.add('item_name');
      form__item_name.innerHTML = object_constructor[key].items[key_id].name;
      form__item.appendChild(form__item_name);
      form__items.appendChild(form__item);
    }

    form__group.appendChild(form__items);
    fieldset.appendChild(form__group);

    form_constructor.appendChild(fieldset);

  }
  var fieldset = document.createElement('fieldset');
  fieldset.classList.add('form__row', 'item__done');
  var form__group = document.createElement('div');
  form__group.classList.add('form__group');
  var form__group_label = document.createElement('label');
  form__group_label.classList.add('form__input_name-done');
  form__group_label.innerHTML = 'Розворот серветки';
  var form__group_input_control = document.createElement('div');
  form__group_input_control.classList.add('constructor__section-mod');
  form__group.appendChild(form__group_label);
  form__group.appendChild(form__group_input_control);
  fieldset.appendChild(form__group);
  form_constructor.appendChild(fieldset);

  var constructor__canvas,
    photo__canvas,
    font_constructor,
    embossing,
    form_inputArr;
  var constructor__canvas = new fabric.Canvas("constructor_box1");
  form_inputArr = $('#constructor__form').serializeArray();
  font_constructor = new fabric.Rect({
    width: constructor__canvas.width,
    fill: form_inputArr[0]['value'],
    height: constructor__canvas.width,
    hasControls: false,
    evented: false,
    selectable: false
  });
  constructor__canvas.add(font_constructor);
  addImage(form_inputArr[2]['value'], 'rectangle');

  function addImage(imgLink, kind) {
    fabric.Image.fromURL(imgLink, function(img) {
      var objs = constructor__canvas.getObjects();

      if (kind == 'rectangle') {
        img.set({
          selectable: false,
          // angle: 50,
          evented: false,
          hasControls: false,
          scaleX: constructor__canvas.getWidth() / img.width,
          //new update
          scaleY: constructor__canvas.getHeight() / img.height
        });

        if (objs.length > 1) {
          objs.forEach(function(e) {
            if (e && e.type !== 'rect') {
              if (e.cornerStyle === "circle") {
                constructor__canvas.renderAll();
              } else {
                if (e.hasControls === false && e._element !== undefined) {
                  constructor__canvas.add(img);
                  constructor__canvas.remove(e); // constructor__canvas.bringToFront(img)
                } else {
                  constructor__canvas.add(img);
                }
              }
            }
          });
        } else {
          constructor__canvas.add(img);
        }
      } else {
        img.set({
          width: 500,
          height: 500,
          selectable: false,
          left: constructor__canvas.getWidth() / 2,
          top: constructor__canvas.getWidth(),
          angle: 135,
          evented: false,
          hasControls: false,
          originX: 'center',
          originY: 'center',
          scaleX: 0.85,
          //new update
          scaleY: 0.85 //  new update

        });

        if (objs.length > 1) {
          objs.forEach(function(e) {
            if (e && e.type !== 'rect') {
              if (e.cornerStyle === "circle") {
                constructor__canvas.renderAll();
              } else {
                if (e.hasControls === false && e._element !== undefined) {
                  console.log(objs.length);
                  e._element.src = imgLink;
                  constructor__canvas.renderAll(); // constructor__canvas.clearContext(e)
                  constructor__canvas.add(img);
                  constructor__canvas.remove(e);
                } else {
                  constructor__canvas.add(img);
                }
              }
            }
          });
        } else {
          constructor__canvas.add(img);
        }
      }
    }, {crossOrigin: 'anonymous'});
  }

  var input_file = document.getElementById('photo');

  if (input_file) {
    input_file.addEventListener('change', onChangePhoto, false);
  }

  function onChangePhoto(evt) {
    var files = evt.target.files,
    fileType = null,
    input_change = evt.input_change || null,
    photo_change = evt.photo_change || null; // Loop through the FileList and render image files as thumbnails.
    var formData = new FormData();
    for(var i=0; i<files.length; i++){
      formData.append('file', files[i])
    }
    $.ajax({
      url:window.location.origin+"/set_logo/",
      data:formData,
      async:false,
      contentType: false,
      processData: false,
      method:'POST',
      type:'POST',
      success:function(data){
        console.log(data)
      }
    })
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (f.type.match('image.png')) {
        var reader = new FileReader(); // Closure to capture the file information.

        reader.onload = function(theFile) {
          return function(e) {
            // Render thumbnail.
            var file_logo = document.getElementById('file-logo');
            var file_logo_parent = file_logo.parentElement;

            var img = '<img class="thumb" src="' + e.target.result + '" title="' + escape(theFile.name) + '"/> ';
            file_logo.innerHTML = img;
            file_logo.classList.add("form__input__img-send");
            var url = URL.createObjectURL(theFile);
            // delete__file();

            photo__canvas = fabric.Image.fromURL(url, function(img1) {
              var reduction_factor;
              if (img1.height <= 300) {
                reduction_factor = 1;
              } else {
                // height_logo = 200;
                reduction_factor = 300 / img1.height;

              }
              if (reduction_factor === 1 && img1.width <= 300) {
                reduction_factor = 1;
              } else {
                reduction_factor = 300 / img1.width;
              }

              img1.set({
                top: constructor__canvas.getHeight() / 2,
                left: constructor__canvas.getWidth() / 2,
                originX: 'center',
                originY: 'center',
                scaleX: reduction_factor,
                scaleY: reduction_factor,
                cornerColor: 'rgb(120,240,180)',
                cornerSize: 28,
                transparentCorners: false,
                cornerStyle: 'circle'
              });

              var objs = constructor__canvas.getObjects();

              objs.forEach(function(e) {
                if (e && e.type !== 'rect') {
                  if (e.cornerStyle === "circle") {
                    constructor__canvas.add(img1);
                    constructor__canvas.remove(e);
                  } else {
                  constructor__canvas.add(img1);
                  }
                }
              });





              constructor__canvas.bringToFront(img1);

              var delete__file = document.createElement('div');
                delete__file.classList.add('delete__file');
                delete__file.id = 'delete__file';
                file_logo_parent.appendChild(delete__file);
                delete_logo();

            });
          };
        }(f); // Read in the image file as a data URL.

        reader.readAsDataURL(f);
      } else {
        alert("Формат логотипу не підходить, потрібен '.png'");
      }
    }
  }

  $(".form__items_item").on('click', function() {
    var id_items,
      input_value;
    var items_list = $(this);

    if (!$(items_list).hasClass('form__items_item-active')) {
      id_items = $(items_list).data('value');
      input_value = $(items_list).parent(".form__items").siblings(".input_value");
      $('.form__items_item').removeClass('form__items_item-active');
      $(items_list).toggleClass('form__items_item-active');
      input_value[0].value = id_items; // console.log(input_value);

      form_inputArr = $('#constructor__form').serializeArray(); // createCanvas({change: true});
      if (form_inputArr[3]['value'] == "rectangle") {
        var _font_constructor$set;

        font_constructor.set((_font_constructor$set = {
          width: constructor__canvas.width,
          height: constructor__canvas.width,
          fill: form_inputArr[0]['value'],
          hasControls: false,
          evented: false,
          selectable: false,
          left: 0,
          top: 0,
          angle: 0,
          scaleY: 1,
          scaleX: 1
        }, _defineProperty(_font_constructor$set, "left", constructor__canvas.getWidth() / 2), _defineProperty(_font_constructor$set, "top", constructor__canvas.getWidth() / 2), _defineProperty(_font_constructor$set, "originX", "center"), _defineProperty(_font_constructor$set, "originY", "center"), _font_constructor$set));
      } else {
        var _font_constructor$set2;

        font_constructor.set((_font_constructor$set2 = {
          fill: form_inputArr[0]['value'],
          height: constructor__canvas.width,
          hasControls: false,
          evented: false,
          originX: 'center',
          originY: 'center',
          left: constructor__canvas.getWidth() / 2,
          width: constructor__canvas.getHeight()
        }, _defineProperty(_font_constructor$set2, "height", constructor__canvas.getHeight()), _defineProperty(_font_constructor$set2, "selectable", false), _defineProperty(_font_constructor$set2, "top", constructor__canvas.getWidth()), _defineProperty(_font_constructor$set2, "angle", 135), _defineProperty(_font_constructor$set2, "scaleY", 0.7), _defineProperty(_font_constructor$set2, "scaleX", 0.7), _font_constructor$set2));
      }

      font_constructor.set('fill', form_inputArr[0]['value']);
      constructor__canvas.renderAll();
      addImage(form_inputArr[2]['value'], form_inputArr[3]['value']);
      constructor__canvas.bringToFront(photo__canvas);
    }
  });
  create_canvas_img('.constructor_submit');

  function create_canvas_img(element) {
    $(element).on('click', function(event) {
      var photo_base64 = constructor__canvas.toDataURL();
      var form = $('#constructor__form').serializeArray()
      var colour_code  = form[0]['value']
      var form_factor_code = form[1]['value']
      var embossing_code = form[2]['value']
      var folding_code = form[3]['value']
      var data = {}
      data['base64'] = photo_base64
      data['colour_code'] = colour_code
      data['form_factor_code'] = form_factor_code
      data['folding_code'] = folding_code
      data['embossing_code'] = embossing_code

      $.ajax({
        url:window.location.origin+"/set_params/",
        data:data, async:false, type:'POST',
        success: function(e){
        }
      })

    })
  }


  $('.form__input__img').on('click', function() {
    $('.form__items').removeClass('form__items-active');
    $("#photo").trigger("click");
  });

  function delete_logo() {
    $("#delete__file").on('click', function(event) {
      var constructor_obj = constructor__canvas.getObjects();
      constructor_obj.forEach(function(e) {
        if (e && e.type !== 'rect') {
          if (e.cornerStyle === "circle") {
            constructor__canvas.remove(e);
          }
        }
      })
      $.ajax({
        url:window.location.origin+"/remove_logo/",
        async:false, type:'POST',
        success: function(e){
          console.log(e)
          // event.target.click()
        }
      })

      var file_logo = document.getElementById('file-logo');
      var img = '<svg width="21" height="17" viewBox="0 0 21 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.6379 0.172363H0.362069C0.162207 0.172363 0 0.334208 0 0.534432V16.4655C0 16.6657 0.162207 16.8275 0.362069 16.8275H20.6379C20.8378 16.8275 21 16.6657 21 16.4655V0.534432C21 0.334208 20.8378 0.172363 20.6379 0.172363ZM20.2759 16.1034H0.724138V0.896501H20.2759V16.1034Z" fill="#969696" /><path d="M5.79322 8.18775C6.90513 8.18775 7.80958 7.28331 7.80958 6.17175C7.80958 5.05948 6.90513 4.15503 5.79322 4.15503C4.6813 4.15503 3.77686 5.05948 3.77686 6.17139C3.77686 7.28331 4.6813 8.18775 5.79322 8.18775ZM5.79322 4.87917C6.50577 4.87917 7.08544 5.4592 7.08544 6.17139C7.08544 6.88358 6.50577 7.46362 5.79322 7.46362C5.08067 7.46362 4.50099 6.88394 4.50099 6.17175C4.50099 5.45956 5.08067 4.87917 5.79322 4.87917Z" fill="#969696" /><path d="M2.53462 14.6552C2.61935 14.6552 2.7048 14.6255 2.77359 14.5651L8.68002 9.36502L12.4101 13.0947C12.5516 13.2363 12.7805 13.2363 12.922 13.0947C13.0636 12.9531 13.0636 12.7243 12.922 12.5827L11.1816 10.8423L14.5057 7.20202L18.583 10.9397C18.7303 11.0747 18.9595 11.0646 19.0946 10.9172C19.2296 10.7698 19.2198 10.5407 19.0721 10.4056L14.7273 6.42285C14.6563 6.35804 14.5618 6.32581 14.4666 6.32798C14.3707 6.33233 14.2801 6.37469 14.2153 6.44566L10.6692 10.3296L8.95193 8.61228C8.81652 8.47722 8.6 8.47035 8.45662 8.59635L2.2953 14.0212C2.14504 14.1534 2.13055 14.3822 2.26271 14.5325C2.3344 14.6139 2.43433 14.6552 2.53462 14.6552Z" fill="#969696" /></svg>';
      file_logo.innerHTML = img;
      file_logo.classList.remove("form__input__img-send");
      document.getElementById('delete__file').remove();
      return false;
    });
  }

  function createPrevButton(i) {
    var stepName = "step" + i;
    $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Prev' class='prev constructor__pagination_prev'>Назад</a>");
    $("#" + stepName + "Prev").bind("click", function(e) {
      $("#" + stepName).hide();
      $("#step" + (
      i - 1)).show();
      // selectStep(i - 1);
    });
  }

  function createNextButton(i) {
    var stepName = "step" + i;

    $("#" + stepName + "commands").append("<div class='constructor__pagination_next'><a href='#' id='" + stepName + "Next' class=' btn btn_control-constructor'>Продовжити  </a> </div>");

    $("#" + stepName + "Next").bind("click", function(e) {
      if (i == 0) {}

      $("#" + stepName).hide();

      $("#step" + (
      i + 1)).show();
      // selectStep(i + 1);
    });

  }
  function createSaveButton(i) {
    var stepName = "step" + i;

    $("#" + stepName + "commands").append("<div class='constructor__pagination_next'><a href='/order_napkin/' class=' btn constructor_submit '>Замовити серветку</a> </div>");
    create_canvas_img('.constructor_submit');
  }
  var isResizeble = false;
  var isResizeblemax = false;

  function resize() {
    console.log(isResizeble);
    if ($(window).width() < 768) {
      if (!isResizeble) {
        console.log($('.canvas-container')[0]);
        $('.constructor__section-mod')[0].appendChild($('.canvas-container')[0]);

        var steps = $("#constructor__form fieldset");
        // console.log(steps);
        var count = steps.length;
        steps.each(function(i) {
          var page_step = i + 1;
          $(this).wrap("<div class='fieldset__box' id='step" + i + "'></div>");
          $(this).prepend("<div class='constructor__pagination'><div class='constructor__pagination_current'>" + 0 + page_step + "</div><div class='constructor__pagination_centr'>/</div><div class='constructor__pagination_all'>" + 0 + count + "</div></div>");
          $(this).append("<div id='step" + i + "commands' class='constructor__pagination_box'></div>");
          if (i == 0) {

            createNextButton(i); // to do
            // selectStep(i);  to do
          } else if (i == count - 1) {
            $("#step" + i).hide();
            createPrevButton(i); // to do
            createSaveButton(i);
          } else {
            $("#step" + i).hide();
            createPrevButton(i); // to do
            createNextButton(i); // to do
          }
        })

        $('.select__items-line').addClass('select__item_mobil');
        $('.select_value-line').addClass('select_value-line_mobil');
        isResizeblemax = false;
        isResizeble = true;
      }

    } else {

      if (!isResizeblemax) {

        isResizeble = false;
        $('.constructor__section-desctop')[0].appendChild($('.canvas-container')[0]);
        isResizeblemax = true;
      }
    }
  }

  resize();



});
window.addEventListener('resize', function(event) {
  resize();
});
