$(function() {





  // console.log($('.hero__form').serialize());
  $('.hero__form form .btn').click(function(event) {
    event.preventDefault();
    alert($('.hero__form').serialize());
  });
  // alert($(e.target).attr('title'));

  $('.profil__navigation_link').on('click', function() {
    $('.profil__navigation_link').removeClass('profil__navigation_link-active');
    $(this).addClass('profil__navigation_link-active');
    var id_link = $(this).data('nav');
    var profil__content = $('.profil__info')
    var active_profil__content = profil__content.find('#' + id_link);
    $('.profil__content').removeClass('profil__content-active');
    active_profil__content.addClass('profil__content-active');
  });
  $('.search__icon').on('click', serch_clisk_heder)

  function serch_clisk_heder() {
    if ($('#header__search_form').hasClass('search__form-active')) {
      $('#header__search_form').removeClass('search__form-active');
          if($('.search__input').val().length){
              $('#header__search_form').submit();
          }
        } else {
      $('#header__search_form').addClass('search__form-active');
    }

    $(document).mouseup(function(e) {
      // событие клика по веб-документу
      var div = $('#header__search_form'); // тут указываем класс элемента

      if (!div.is(e.target) // если клик был не по нашему блоку
      && div.has(e.target).length === 0) {
        // и не по его дочерним элементам
        $('#header__search_form').removeClass('search__form-active');

      }
    });
  }

  $('.header__main_item').on('click', function(event) {
    event.preventDefault();
    if ($(this).children('a').length > 0) {
      window.location = $(this).children('a').attr('href');
    }
  });

  if ($('input[type="tel"]').length > 0) {
    $('input[type="tel"]').mask("+380(99)999-99-99");
  }

  /* auth start */
  $('.sign-up').on('click', function(event) {
    event.preventDefault();
    $('.auth_form').removeClass('form-active');
    $('.register-form').addClass('form-active');
  });
  $('.login-form-btn').on('click', function(event) {
    event.preventDefault();
    $('.auth_form').removeClass('form-active');
    $('.login-form').addClass('form-active');
  });
  $('.forgot-btn').on('click', function(event) {
    event.preventDefault();
    $('.auth_form').removeClass('form-active');
    $('.forgot-form').addClass('form-active');
  });
  /* auth end */

  /* product cart start */

  if (document.getElementsByClassName('Gallery').length > 0) {

    var galleryTop = new Swiper('.Gallery', {
          direction: 'vertical',
          spaceBetween: 10,
    	 		loop: true,
    			loopedSlides: 4
        });
        var galleryThumbs = new Swiper('.Thumbs', {
          direction: 'vertical',
          spaceBetween: 10,
          centeredSlides: true,
          slidesPerView: 5,
          touchRatio: 0.2,
          slideToClickedSlide: true,
    			loop: true,
          navigation: {
            nextEl: '.product-card__pagination_top',
            prevEl: '.product-card__pagination_buttoms',
          },
    			loopedSlides: 4
        });
        galleryTop.controller.control = galleryThumbs;
        galleryThumbs.controller.control = galleryTop;

  }

  $('.product-cart__size_item').on('click', function() {
    $('.product-cart__size_item').removeClass('product-cart__size_item-active');
    $(this).toggleClass('product-cart__size_item-active');
    if ($('.product-cart__size-input').length > 0) {
      $('.product-cart__size-input').val($(this).data('size'));
    }

  })
  //select product cart size start
  $('.select_size').on('click', function(event) {
    console.log("crash");
    var name_item,
      id_item,
      current_value,
      temp_item = {};
    name_item = $(this).text();
    id_item = $(this).data('size');
    console.log(id_item);
    current_value = $(this).parent(".select__items").siblings(".select_value").find('.select_value_name');
    temp_item.value = current_value.text();
    temp_item.id = current_value.data('size');
    console.log(temp_item.id);
    current_value.text(name_item);
    current_value.data('size', id_item);
    $(this).text(temp_item.value)
    $(this).data('size', temp_item.id)
    if ($('.product-cart__size-input').length > 0) {
      $('.product-cart__size-input').val(id_item);
    }
  })
  //select product cart size end
  /* product cart end */

  // CARD LIKE START

  $(".like__box").on('click', function(e) {
    $(this).toggleClass('like__box-active');
  });
  // CARD LIKE END

  $('.product__group_plus').on('click', function() {
    var element = $(this);
    // update_count(element);
    // update_prise(element);

  });
  $('.product__group_minus').on('click', function() {
    var element = $(this);
    // console.log(element);
    // update_count(element);
    // update_prise(element);

  });

  $('.product__quantity_count').on('keyup', function() {
    // update_prise($(this));
  })

  function update_count(element) {
    var product_count_value = $(element).siblings('.product__quantity_count').val();
    var product_count = $(element).siblings('.product__quantity_count');

    if ($(element).hasClass('product__group_plus')) {
      product_count.val(parseInt(product_count_value) + 1);
      console.log('1');
    } else {
      console.log("2");
      if (product_count_value > 1) {
        product_count.val(parseInt(product_count_value) - 1);
        console.log("3");
      }
    }
  }
  // function update_prise(element) {
  //   var product_count_value;
  //   var product_count = element.parent('.product__quantity');
  //
  //   var product_prise = parseFloat(element.parent('.product__quantity').data('prise'));
  //
  //   if ($(element).hasClass('product__quantity_count')) {
  //     product_count_value = $(element).val();
  //   } else {
  //     product_count_value = element.siblings('.product__quantity_count').val();
  //   }
  //   var all_product_prise = product_count.siblings('.product__price_box').find('.product__price').text(parseFloat(product_prise * product_count_value).toFixed(2));
  // }

  if (document.getElementsByClassName('like-list-form').length > 0) {
    var like_modaal = $(".like-list-form").modaal({
      content_source: '#like_list', hide_close: true,
      // background: #131313,
      background_scroll: false
    });
    $('.basket_close').on('click', function() {
      like_modaal.modaal('close');
      // basket_modaal.modaal('close');
    });
  }

  if (document.getElementsByClassName('basket-form').length > 0) {
    var basket_modaal = $(".basket-form").modaal({
      content_source: '#basket', hide_close: true,
      // background: #131313,
      background_scroll: false
    });
    $('.basket_close').on('click', function() {
      basket_modaal.modaal('close');
    });
  }

  var $slickElement = $('.slider__wrap');

  if ($slickElement.length > 0) {
    $slickElement.slick({
      dots: true,
      infinite: true,
      arrows: false,
      speed: 300,
      slidesToShow: 1,
      customPaging: function(slider, i) {
        return '<button class="tab">' + '<div class="title">' + $(slider.$slides[i]).attr('title') + '</div>' + '</button>';
      },
      adaptiveHeight: true
    });
    $slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
      var i = (
        currentSlide
        ? currentSlide
        : 0) + 1;
      $('.slider__pagination_current').text(
        i > 10
        ? i
        : '0' + i);
      $('.slider__pagination_all').text(
        slick.slideCount > 10
        ? slick.slideCount
        : '0' + slick.slideCount);
    });
  }
  var $partner__wrap = $('.partner__wrap');
  if ($partner__wrap.length > 0) {
    $partner__wrap.slick({
      dots: true,
      infinite: true,
      arrows: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      // centerMode: true,
      dotsClass: "partner__dots",
      variableWidth: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        }
      ]
    });
  }

  $(".partner__prew_item").on('click', function() {
    $('.partner__wrap').slick('slickPrev');
  });
  $(".partner__next_item").on('click', function() {
    $('.partner__wrap').slick('slickNext');
  });

  $('.lang__arrow').on('click', function(event) {
    event.preventDefault();
    $(this).toggleClass("lang__arrow-active");
    $(this).siblings('.lang__items').toggleClass("lang__items-active");
  });

  $(".js-hamburger").on('click', function(e) {
    $('.js-hamburger').toggleClass('is-active');
    $('.header__main').toggleClass('header__main-active');
    $('.main-menu__backgaunt').toggleClass('main-menu__backgaunt-active');
  });

  // $('.form__input__img').on('click', function() {
  //   $('.form__items').removeClass('form__items-active');
  //   $("#photo").trigger("click");
  // });

  $(".form__input_name,.form__input_control").on('click', function() {
    var items_list = $(this).siblings(".form__items");
    if ($(items_list).hasClass('form__items-active')) {
      $(items_list).toggleClass('form__items-active');
    } else {
      $('.form__items').removeClass('form__items-active');
      $(items_list).toggleClass('form__items-active');
    }
  })

  $(".form__items_item").on('click', function() {
    var id_items,
      input_value;
    var items_list = $(this);
    if (!$(items_list).hasClass('form__items_item-active')) {
      id_items = $(items_list).data('id');
      input_value = $(items_list).parent(".form__items").siblings(".input_value");
      $('.form__items_item').removeClass('form__items_item-active');
      $(items_list).toggleClass('form__items_item-active');
      input_value[0].value = id_items;
    }
  })
  /*
  sellect
      */
  $('.select__sort').on('click', function() {
    $('.catalog__filter_box').removeClass('catalog__filter_box-active');
    $('.catalog__filter_img').removeClass('catalog__filter_img-active');

  });

  $('.select_value').on('click', select_clisc);
  function select_clisc() {
    console.log(this);
    console.log(this.classList);
// console.log("step1");
    if ($(this).hasClass('select__value-active')) {
// console.log("step1_class-Yes");
      $(this).siblings(".select__items_wrap").find('.select__items').removeClass("select__items-active");
      $(this).removeClass("select__value-active");
      $(this).siblings(".select__items_wrap").removeClass("select__items_wrap-active");
      $(this).siblings(".select__items_wrap").removeClass("select__items_wrap-active");
// console.log("step1_class-Yes delete");
    } else {
      // console.log("step1_class-NO ");
      $('.select_value').removeClass("select__value-active");
      $('.select__items').removeClass("select__items-active");
      $(this).siblings(".select__items_wrap").addClass("select__items_wrap-active");

      $(this).siblings('.select__items_wrap').find(".select__items").addClass("select__items-active");
      $(this).addClass("select__value-active");
// console.log("step1_class-NO delete");
    }

    $(document).mouseup(function(e) {
      // событие клика по веб-документу
      var div = $('.select__items_wrap'); // тут указываем класс элемента

      if (!div.is(e.target) // если клик был не по нашему блоку
      && div.has(e.target).length === 0) {
        // и не по его дочерним элементам
        // $('.select__items_wrap').removeClass('select__items_wrap-active');
        $(".select__items_wrap").removeClass("select__items_wrap-active");

        $('.select__items').removeClass('select__items-active');
        $('.select_value').removeClass('select_value-active');

      }
    });
  }

  if ($('.select__group').length > 0) {
    $('.select__group').each(function(i, item) {
      var id_select = item.querySelector('.select_value_name').dataset.id;
      var select_element = item.querySelector('.select__item[data-id=' + id_select + ']');
      select_element.classList.add('select__item-active','select__item-disable')

    })
  }

  $('.select__item').on('click', function(event) {

    var parent_item = null;
    var name_item,
      id_item,
      current_value,
      temp_item = {};
    name_item = $(this).text();
    id_item = $(this).data('id');
    current_value = $(this).parents(".select__items_wrap").siblings(".select_value").find('.select_value_name');
    parent_item = $(this).parent(".select__items").children();

    parent_item.each(function(i, item) {
      $(item).removeClass('select__item-active')
    });

    current_value.text(name_item);
    current_value.data('id', id_item);
    current_value[0].dataset.id = id_item;



    if ($('.product-cart__size-input').length > 0) {
      $('.product-cart__size-input').val(id_item);
    }


    $(this).addClass('select__item-active')
    $('.select__items_wrap').removeClass('select__items_wrap-active');
    $(this).parents(".select__items_wrap").removeClass("select__items_wrap-active");

    $('.select_value').removeClass('select__value-active');

  });

  $('.catalog__filter_img').on('click', function() {
    $(this).toggleClass('catalog__filter_img-active');
    $(this).siblings('.catalog__filter_box').toggleClass('catalog__filter_box-active');
    $(document).mouseup(function(e) {
      // событие клика по веб-документу
      var div = $('.catalog__filter_box '); // тут указываем класс элемента

      if (!div.is(e.target) // если клик был не по нашему блоку
      && div.has(e.target).length === 0) {
        // и не по его дочерним элементам
        // $('.select__items_wrap').removeClass('select__items_wrap-active');
        $(".catalog__filter_img").removeClass("catalog__filter_img-active");

        $('.catalog__filter_box').removeClass('catalog__filter_box-active');

      }
    });
  })

  $('.select__item').removeClass('select__item_mobil');

  /* lazy loader start */

  var imageObserver = new IntersectionObserver(function(entries, imgObserver) {
    entries.forEach(function(entry) {
      if (entry.isIntersecting) {
        var lazyImage = entry.target;
        // console.log("lazy loading ", lazyImage);
        lazyImage.src = lazyImage.dataset.src;
        lazyImage.classList.remove("lzy_img");
        imgObserver.unobserve(lazyImage);
      }
    });
  });
  var arr = document.querySelectorAll('.lazyload');
  arr.forEach(function(v) {
    imageObserver.observe(v);
  });
  /* lazy loader end */

  function sprite_hover(options) {
    var spriteOptions = null,
      curentFrame = null,
      images = null,
      ctx = null,
      canvas = null,
      background = null;
    var spriteOptions = {};
    spriteOptions.swan = options.swan || false;
    spriteOptions.one_photo = options.one_photo || false;
    spriteOptions.curentFrameflag = options.curentFrame;
    curentFrame = options.curentFrame;
    spriteOptions.x = options.x;
    spriteOptions.y = options.y;
    spriteOptions.width = options.width;
    spriteOptions.height = options.height;
    spriteOptions.frameCount = options.frameCount;
    spriteOptions.img_src = options.src;
    spriteOptions.background_src = options.background_src;
    spriteOptions.canvas_id = options.canvas_id;
    spriteOptions.step_photo = options.step_photo;
    spriteOptions.shag = options.shag || false;
    spriteOptions.speed = options.speed || 15;
    spriteOptions.timerId_over = false;
    spriteOptions.timerId_out = false;
    spriteOptions.mobill_animation  = options.mobill_animation || false;

    srcX = 0;
    srcY = 0;

    // anime images
    images = new Image();
    images.src = spriteOptions.img_src;
    images.onload = function() {
      images.width = (this.width / spriteOptions.step_photo) - 1;

      // canvas poligon
      canvas = document.getElementById(spriteOptions.canvas_id);
      canvas.width = options.width;
      canvas.height = options.height;
      ctx = canvas.getContext("2d");

      // background images
      background = new Image();
      background.src = spriteOptions.background_src;
      background.onload = function() {
        // console.log(ctx);


        ctx.drawImage(background, 0, 0, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
        ctx.drawImage(images, srcX, srcY, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
      }

      canvas.onmouseover = function(e) {
        clearInterval(spriteOptions.timerId_out);
        spriteOptions.drawImage = function() {
          spriteOptions.timerId_over = setInterval(function() {
            if (spriteOptions.curentFrameflag < spriteOptions.frameCount) {
              if (curentFrame < images.width) {
                spriteOptions.updateFrame();
                ctx.drawImage(background, 0, 0, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
                ctx.drawImage(images, srcX, srcY, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
              }
            }
          }, spriteOptions.speed);
        };

        // update drawing photo
        spriteOptions.updateFrame = function() {
          if (spriteOptions.curentFrameflag < spriteOptions.frameCount) {
            curentFrame = ++curentFrame;
          } else {
            curentFrame = --curentFrame;
          }
          $bird_shag = curentFrame;
          srcX = curentFrame * spriteOptions.step_photo;
          srcY = 0;
          ctx.clearRect(spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
        };
        spriteOptions.drawImage();
      }

      canvas.onmouseout = function() {
        clearInterval(spriteOptions.timerId_over);
        spriteOptions.drawImage = function() {
          spriteOptions.timerId_out = setInterval(function() {
            if (0 <= curentFrame) {
              spriteOptions.updateFrame();
              ctx.drawImage(background, 0, 0, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
              ctx.drawImage(images, srcX, srcY, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
            }
          }, spriteOptions.speed);
        };
        // update drawing photo
        spriteOptions.updateFrame = function() {
          if (curentFrame > 0) {
            curentFrame = --curentFrame;
          }
          $bird_shag = curentFrame;
          srcX = curentFrame * spriteOptions.step_photo;
          srcY = 0;
          ctx.clearRect(spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
        };
        spriteOptions.drawImage();
      }
      spriteOptions.updateFrame = function() {
        if (spriteOptions.curentFrameflag < spriteOptions.frameCount) {
          curentFrame = ++curentFrame;
        } else {
          curentFrame = --curentFrame;
        }
        $bird_shag = curentFrame;
        srcX = curentFrame * spriteOptions.step_photo;
        srcY = 0;
        ctx.clearRect(spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
      };
      // clearInterval(spriteOptions.timerId_out);
var iterations = 0;

if(spriteOptions.mobill_animation===true){
  spriteOptions.timerId_over = setInterval(function() {
    iterations++;
    // console.log(spriteOptions.curentFrameflag);
    // console.log(spriteOptions.frameCount);
    if (spriteOptions.curentFrameflag < spriteOptions.frameCount) {
      if (curentFrame < images.width) {
        spriteOptions.updateFrame();
        ctx.drawImage(background, 0, 0, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
        ctx.drawImage(images, srcX, srcY, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);


      }
    }
    if (iterations >= 60)
           clearInterval(spriteOptions.timerId_over)
  }, spriteOptions.speed);

}



    }
  }

  var $bird_shag;
  var $bird = document.getElementById('bird');
  if (typeof($bird) != 'undefined' && $bird != null) {
    sprite_hover({
      x: 0,
      y: 0,
      width: 300,
      height: 300,
      frameCount: 49,
      curentFrame: 0,
      src: '/static/img/bird.png',
      background_src: '/static/img/bird_bg.png',
      canvas_id: 'bird',
      step_photo: 304
    })

  }

  var flower = document.getElementById('flower');
  if (typeof(flower) != 'undefined' && flower != null) {

    sprite_hover({
      x: 0,
      y: 0,
      width: 300,
      height: 300,
      frameCount: 45,
      curentFrame: 0,
      src: '/static/img/flower.png',
      background_src: '/static/img/flower_bg.png',
      canvas_id: 'flower',
      step_photo: 300
    })

  }

  var ship = document.getElementById('ship');
  if (typeof(ship) != 'undefined' && ship != null) {

    sprite_hover({
      x: 0,
      y: 0,
      width: 300,
      height: 300,
      frameCount: 49,
      curentFrame: 0,
      src: '/static/img/ship.png',
      background_src: '/static/img/ship_bg.png',
      canvas_id: 'ship',
      step_photo: 300
    })
  }

  var swan = document.getElementById('swan');
  if (typeof(swan) != 'undefined' && swan != null) {
    sprite_hover({
      x: 0,
      y: 0,
      width: 300,
      height: 300,
      frameCount: 43,
      curentFrame: 0,
      src: '/static/img/swan.png',
      background_src: '/static/img/swan_bg.png',
      canvas_id: 'swan',
      step_photo: 300
    })

  }

  // if ($(window).width() < 768) {
  //    var wow = new WOW(
  //     {
  //       boxClass:     'check',      // класс, скрывающий элемент до момента отображения на экране (по умолчанию, wow)
  //       // animateClass: 'animated',
  //       offset:       300,          // расстояние в пикселях от нижнего края браузера до верхней границы элемента, необходимое для начала анимации (по умолчанию, 0)
  //       mobile:       true,       // включение/отключение WOW.js на мобильных устройствах (по умолчанию, включено)
  //       live:         true,       // поддержка асинхронно загруженных элементов (по умолчанию, включена)
  //       callback:     function(box){
  //       console.log(box.id);
  //         // функция срабатывает каждый раз при старте анимации
  //         // аргумент box — элемент, для которого была запущена анимация
  //         if(box.id==='bird'){
  //           // sprite_hover({
  //           //   x: 0,
  //           //   y: 0,
  //           //   width: 300,
  //           //   height: 300,
  //           //   frameCount: 49,
  //           //   curentFrame: 0,
  //           //   src: '/static/img/'+box.id+'.png',
  //           //   background_src: '/static/img/'+box.id+'_bg.png',
  //           //   canvas_id: box.id,
  //           //   step_photo: 304,
  //           //   mobill_animation: true,
  //           // })
  //         }else if (box.id==='flower') {
  //           // sprite_hover({
  //           //   x: 0,
  //           //   y: 0,
  //           //   width: 300,
  //           //   height: 300,
  //           //   frameCount: 45,
  //           //   curentFrame: 0,
  //           //   src: '/static/img/flower.png',
  //           //   background_src: '/static/img/flower_bg.png',
  //           //   canvas_id: 'flower',
  //           //   step_photo: 300,
  //           //   mobill_animation: true,
  //           // })
  //         }else if (box.id==='ship') {
  //           // sprite_hover({
  //           //   x: 0,
  //           //   y: 0,
  //           //   width: 300,
  //           //   height: 300,
  //           //   frameCount: 49,
  //           //   curentFrame: 0,
  //           //   src: '/static/img/ship.png',
  //           //   background_src: '/static/img/ship_bg.png',
  //           //   canvas_id: 'ship',
  //           //   step_photo: 300,
  //           //   mobill_animation: true,
  //           // })
  //         }else if (box.id==='swan') {
  //           // sprite_hover({
  //           //   x: 0,
  //           //   y: 0,
  //           //   width: 300,
  //           //   height: 300,
  //           //   frameCount: 43,
  //           //   curentFrame: 0,
  //           //   src: '/static/img/swan.png',
  //           //   background_src: '/static/img/swan_bg.png',
  //           //   canvas_id: 'swan',
  //           //   step_photo: 300,
  //           //   mobill_animation: true,
  //           // })
  //         }
  //
  //
  //
  //
  //       },
  //       scrollContainer: null // селектор прокручивающегося контейнера (опционально, по умолчанию, window)
  //     }
  //   );
  //   wow.init();
  // }


var $win = $(window);
var $marker = $('#bird');

//отслеживаем событие прокрутки страницы
// $win.scroll(function() {
//   //Складываем значение прокрутки страницы и высоту окна, этим мы получаем положение страницы относительно нижней границы окна, потом проверяем, если это значение больше, чем отступ нужного элемента от верха страницы, то значит элемент уже появился внизу окна, соответственно виден
//   if($win.scrollTop() + $win.height() - 150 >= $marker.offset().top) {
//     console.log("виден"); //выполняем действия если элемент виден
//   }else{
//     console.log("не виден");   //выполняем действия если не элемент виден
//   }
// });

  $(".tabs-list li a").click(function(e) {
    e.preventDefault();
  });

  $(".header__main_item-admin").click(function() {
    console.log($(this))
    var tabid = $(this).attr("href");
    $(".tabs-list li,.tabs div.tab").removeClass("active"); // removing active class from tab
    $(".tab").hide(); // hiding open tab
    $(tabid).show(); // show tab
    $(this).addClass("active"); //  adding active class to clicked tab

  });
  $('.catalog__item').mouseenter(function() {
    console.log("crash");
    $(this).addClass('catalog__item-active');
  }).mouseleave(function() {
    $(this).removeClass('catalog__item-active');
  });
  var forEach = function(array, callback, scope) {
    for (var i = 0; i < array.length; i++) {
      callback.call(scope, i, array[i]); // passes back stuff we need
    }
  };

  var card_item = document.getElementsByClassName('catalog__item');
  forEach(card_item, function(index, value) {
    card_item[i].mouseenter(function() {
      value.addClass('catalog__item-active');
    })
    card_item[i].mouseleave(function() {
      value.removeClass('catalog__item-active');
    });
  })
})
