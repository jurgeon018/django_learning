$(function() {

  Onload();
})

function Onload() {

  valide_form('#hero__form', '.form__container_box');
  valide_form('#privatelabel_form', '.form__container_box');
  valide_form('#partnershp_form', '.form__container_box');
  valide_form('#contacts_form', '.form__container_box');

  valide_form_order('#make_order_form');

}

function location_leng() {
  return location.pathname.split('/')[1];
}

function valide_form(id_form, append_error_box) {

  if ($(id_form).length > 0) {

    var lang_site;
    var errore_text = {};

    lang_site = location_leng();
    switch (lang_site) {
      case 'uk':
        errore_text.required = 'Поле обов\'язково для заповнення';
        errore_text.email = 'Поле має містити email';
        break;
      case 'ru':
        errore_text.required = 'Поле обязательно для заполнения';
        errore_text.email = 'Поле должно содержать email';
        break;
      case 'en':
        errore_text.required = 'The field is required';
        errore_text.email = 'The field must contain an email';
        break;
      default:
        errore_text.required = 'Поле обов\'язково для заповнення.';
        errore_text.email = 'Поле має містити email.';
    }


    $(id_form).validate({
      errorPlacement: function(event, validator) {
        $(validator).parents(append_error_box).append($(event));
      },
      rules: {
        email: {
          required: true,
          email: true,
        },
        tel: {
          required: true,

        }
      },

      messages: {
        email: {
          required: errore_text.required,
          email: errore_text.email
        },
        tel: {
          required: errore_text.required,
        }
      },

      submitHandler: function(form) {
        form.submit();
      }
    });
  }

}


function valide_form_order(id_form){
  console.log("crash");
  if ($(id_form).length > 0) {

    var lang_site;
    var errore_text = {};

    lang_site = location_leng();
    switch (lang_site) {
      case 'uk':
        errore_text.required = 'Поле обов\'язково для заповнення';
        errore_text.email = 'Поле має містити email';
        break;
      case 'ru':
        errore_text.required = 'Поле обязательно для заполнения';
        errore_text.email = 'Поле должно содержать email';
        break;
      case 'en':
        errore_text.required = 'The field is required';
        errore_text.email = 'The field must contain an email';
        break;
      default:
        errore_text.required = 'Поле обов\'язково для заповнення.';
        errore_text.email = 'Поле має містити email.';
    }


    $(id_form).validate({
      // errorPlacement: function(event, validator) {
      //   $(validator).parents(append_error_box).append($(event));
      // },
     //
     errorPlacement: function(event, validator) {
       $(validator).parents('.form-group').append($(event));
     },
      rules: {
        username: {
          required: true,
        },
        email: {
          required: true,
          email: true,
        },

        phone: {
          required: true,

        },
        address: {
          required: true,

        }
      },

      messages: {
        username: {
          required: errore_text.required,

        },
        email: {
          required: errore_text.required,
            email: errore_text.email
        },
        phone: {
          required: errore_text.required,
        },
        address: {
          required: errore_text.required,
        }
      },
ignore: ".ignore",
      submitHandler: function(form) {
        form.submit();
      }
    });
  }
}
