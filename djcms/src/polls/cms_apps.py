from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
from .views import IndexView, DetailView
from django.urls import path



@apphook_pool.register  
class PollsApphook(CMSApp):
    app_name = "polls"
    name = _("Polls Application")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["polls.urls"]
        # return [
        #     path('', IndexView.as_view()),
        #     path('<pk>/', DetailView.as_view()),
        # ]
