from django.urls import path
from .views import *


app_name = "polls"


urlpatterns = [
    path('',  IndexView.as_view(), name='index'),
    path('<pk>/',  DetailView.as_view(), name='detail'),
    path('<pk>/results/',  ResultsView.as_view(), name='results'),
    path('<poll_id>/vote/',  vote, name='vote'),

]
