from django.apps import AppConfig


class PollsConfig(AppConfig):
    name = 'polls'
    verbose_name = "Polls"
    verbose_name_plural = "Polls"
