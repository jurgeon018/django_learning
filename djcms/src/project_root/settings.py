import os
from django.utils.translation import ugettext_lazy as _
DATA_DIR         = os.path.dirname(os.path.dirname(__file__))
BASE_DIR         = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY       = '3dq25cmuuirz77v6@_s_2v989pk1*s=(al0eesx!vceo!+h)ui'
DEBUG            = True
ALLOWED_HOSTS    = ['*']
ROOT_URLCONF     = 'project_root.urls'
WSGI_APPLICATION = 'project_root.wsgi.application'
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',},
]
LANGUAGE_CODE = 'en'
TIME_ZONE     = 'Europe/Uzhgorod'
USE_I18N      = True
USE_L10N      = True
USE_TZ        = True
STATIC_URL    = '/static/'
MEDIA_URL     = '/media/'
MEDIA_ROOT    = os.path.join(BASE_DIR, 'media')
STATIC_ROOT   = os.path.join(BASE_DIR, 'static_root')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
SITE_ID = 1
AUTH_USER_MODEL = "custom_user.CustomUser" 
LANGUAGES = (
    ('en', _('en')),
    ('ru', _('ru')),
    ('uk', _('uk')),
)
LOCATE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)
DATABASES = {
    'default': {
        'CONN_MAX_AGE': 0,
        'ENGINE': 'django.db.backends.sqlite3',
        'HOST': 'localhost',
        'NAME': 'db.sqlite3',
        'PASSWORD': '',
        'PORT': '',
        'USER': ''
    }
}
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'),],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.media',
                'django.template.context_processors.csrf',
                'django.template.context_processors.tz',
                'sekizai.context_processors.sekizai',
                'django.template.context_processors.static',
                'cms.context_processors.cms_settings',
                'aldryn_boilerplates.context_processors.boilerplate',           
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                'aldryn_boilerplates.template_loaders.AppDirectoriesLoader',
            ],
        },
    },
]


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',

    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    'cms.middleware.utils.ApphookReloadMiddleware',
]

INSTALLED_APPS = [
    'custom_user',

    'djangocms_admin_style',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',

    'cms',
    'menus',
    'sekizai',
    'treebeard',
    'filer',
    'easy_thumbnails',
    'mptt',
    'absolute',
    'captcha',
    'emailit',
    'parler',
    'sortedm2m',
    'taggit',

    'djangocms_bootstrap4',
    'djangocms_bootstrap4.contrib.bootstrap4_alerts',
    'djangocms_bootstrap4.contrib.bootstrap4_badge',
    'djangocms_bootstrap4.contrib.bootstrap4_card',
    'djangocms_bootstrap4.contrib.bootstrap4_carousel',
    'djangocms_bootstrap4.contrib.bootstrap4_collapse',
    'djangocms_bootstrap4.contrib.bootstrap4_content',
    'djangocms_bootstrap4.contrib.bootstrap4_grid',
    'djangocms_bootstrap4.contrib.bootstrap4_jumbotron',
    'djangocms_bootstrap4.contrib.bootstrap4_link',
    'djangocms_bootstrap4.contrib.bootstrap4_listgroup',
    'djangocms_bootstrap4.contrib.bootstrap4_media',
    'djangocms_bootstrap4.contrib.bootstrap4_picture',
    'djangocms_bootstrap4.contrib.bootstrap4_tabs',
    'djangocms_bootstrap4.contrib.bootstrap4_utilities',

    'djangocms_file',
    'djangocms_icon',
    'djangocms_link',
    'djangocms_picture',
    'djangocms_style',
    'djangocms_snippet',
    'djangocms_googlemap',
    'djangocms_video',
    'djangocms_column',
    'djangocms_text_ckeditor',
    
    # #Aldryn News Blog
    # 'aldryn_apphooks_config',
    # 'aldryn_boilerplates',
    # 'aldryn_categories',
    # 'aldryn_common',
    # 'aldryn_newsblog',
    # 'aldryn_people',
    # 'aldryn_translation_tools',
    # # 'aldryn_forms',
    # # 'aldryn_forms.contrib.email_notifications',
    # # 'aldryn_reversion',
    # # 'reversion',

    #Custom Apps
    'polls',

]

CMS_LANGUAGES = {
    1: [
        {
            'code': 'en',
            'name': _('en'),
            'redirect_on_fallback': True,
            'public': True,
            'hide_untranslated': False,
        },
        {
            'code': 'uk',
            'name': _('uk'),
            'redirect_on_fallback': True,
            'public': True,
            'hide_untranslated': False,
        },
        {
            'code': 'ru',
            'name': _('ru'),
            'redirect_on_fallback': True,
            'public': True,
            'hide_untranslated': False,
        },
        
    ],
    'default': {
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': False,
    },
}
CMS_TEMPLATES = (
    ('horeca.html', 'Horeca'),
    ('contact.html', 'Contact'),
)
CMS_PERMISSION = True
CMS_PLACEHOLDER_CONF = {}
THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters'
)
ALDRYN_BOILERPLATE_NAME='bootstrap3'
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'aldryn_boilerplates.staticfile_finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]