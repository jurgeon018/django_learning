from django.db import models
from django.conf import settings
from django.shortcuts import reverse
from django.contrib.auth import get_user_model
from django.utils.text import slugify
from time import time
User = get_user_model()


class Post(models.Model):
	title = models.CharField(max_length=120)
	content = models.TextField()
	slug = models.SlugField(max_length=100, unique=True, blank=True)
	timestamp = models.DateTimeField(auto_now_add=True)
	comments = models.ForeignKey('Comment', blank=True, null=True, related_name='commentz', on_delete=models.CASCADE)
	def save(self, *args, **kwargs):
		self.slug = f'{slugify(self.title, allow_unicode=True)}-{str(int(time()))}'
		super().save(*args, **kwargs)
	def __str__(self):
		return self.title
	def get_absolute_url(self):
		return reverse('post_detail', kwargs={'slug': self.slug})
	def get_post_title(self):
		return self.title


class Comment(models.Model):
	post = models.ForeignKey(Post, related_name='post', on_delete=models.CASCADE)
	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	comment_text = models.TextField()
	timestamp = models.DateTimeField(auto_now_add=True)
	def __str__(self):
		return "Comment by {user} to post {post}".format(user=self.author.username, post=self.post.title)
	def get_comment_text(self):
		return self.comment_text


from django.contrib import admin

admin.site.register(Post)
admin.site.register(Comment)
