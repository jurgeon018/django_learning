from django.urls import path
from django.shortcuts import render
from .models import *
from django.views import View
from django.shortcuts import get_object_or_404
from django import forms
from .models import Comment


class CommentForm(forms.ModelForm):
	class Meta:
		model = Comment
		fields = [
			'comment_text'
		]


def base_view(request):
	posts = Post.objects.all()
	return render(request, 'djchannels/base.html', {'posts': posts})


def post_detail(request, slug):
	post = get_object_or_404(Post, slug=slug)
	form = CommentForm(request.POST or None)
	comments = Comment.objects.filter(post=post)
	return render(request, 'djchannels/post_detail.html', {
		'post': post,
		'form': form,
		'comments': comments,
		'timestamp': post.timestamp
	})


urlpatterns = [
    path('', base_view, name='base_view'),
    path('', base_view, name='djchannels_index'),
    path('post/<slug>/', post_detail, name='post_detail')
]
