from django.db import models
from django.urls import reverse
from django.conf import settings

class Category(models.Model):
    name = models.CharField(max_length=140)
    slug = models.SlugField()
    def __str__(self):
        return f'Category({self.id}:{self.name}:{self.slug})'
    def get_absolute_url(self):
        return reverse('category-detail', kwargs={'slug':self.slug})

def generate_filename(instance, filename):
    filename = instance.slug + '.jpg'
    return f'{instance}/{filename}'

class ArticleManager(models.Manager):
    def all(self, *args, **kwargs):
        return super(ArticleManager, self).get_queryset().filter(pk__in=[3,5,7])

class Article(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    slug = models.SlugField(blank=True)
    image = models.ImageField(upload_to=generate_filename, blank=True)
    content = models.TextField(blank=True)
    likes = models.PositiveIntegerField(default=0)
    dislikes = models.PositiveIntegerField(default=0)
    objects = models.Manager()
    custom_manager = ArticleManager()
    def __str__(self):
        return f'Article({self.id}:{self.title}:{self.slug})'
    def get_absolute_url(self):
        return reverse('article-detail', kwargs={'slug':self.slug, 'category':self.category.slug})

class MyArticles(Article):
    class Meta:
        proxy = True


# class Comments(models.Model):
#     author = models.ForeignKey(settings.AUTH_USER_MODEL)
