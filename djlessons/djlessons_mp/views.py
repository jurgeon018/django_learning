from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from .models import *
from .mixins import *
from django.http import JsonResponse

class ArticleListView(ListView):
    model = Article
    template_name = 'djlessons_mp/index.html'
    def get_context_data(self, *args, **kwargs):
        context = super(ArticleListView, self).get_context_data(*args, **kwargs)
        context['articles'] = self.model.objects.all()
        return context

class CategoryListView(ListView):
    model = Category
    template_name = 'djlessons_mp/index.html'
    def get_context_data(self, *args, **kwargs):
        context = super(CategoryListView, self).get_context_data(*args, **kwargs)
        context['categories'] = self.model.objects.all()
        context['articles'] = Article.objects.all()[:4]
        return context

class CategoryDetailView(DetailView, CategoryListMixin):
    model = Category
    template_name = 'djlessons_mp/category_detail.html'
    def get_context_data(self, *args, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(*args, **kwargs)
        context['categories'] = self.model.objects.all()
        context['category'] = self.get_object()
        context['articles_from_category'] = self.get_object().article_set.all()
        return context

class ArticleDetailView(DetailView, CategoryListMixin):
    model = Category
    template_name = 'djlessons_mp/article_detail.html'
    def get_context_data(self, *args, **kwargs):
        context = super(ArticleDetailView, self).get_context_data(*args, **kwargs)
        context['categories'] = self.model.objects.all()
        context['article'] = self.get_object()
        return context

def dynamic_article_image(request):
    article_id = request.GET.get('article_id')
    article = Article.objects.get(id=article_id)
    data = {
        'article_image': artiale.image.url
    }
    return JsonResponse(data)
