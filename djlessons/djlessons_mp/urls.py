from django.urls import path
from django.views.generic import TemplateView
from .views import *


urlpatterns = [
    path('', CategoryListView.as_view(), name='djlessons_mp_index'),
    path('', CategoryListView.as_view(), name='base'),
    path('category/<slug>', CategoryDetailView.as_view(), name='category-detail'),
    path('<category>/<slug>/', ArticleDetailView.as_view(), name='article-detail'),
    path('show_article_image/', dynamic_article_image, name='article_image'),
]
